import { Map } from 'immutable';
import { handleActions, createAction } from 'redux-actions';

const SET_INPUT_KEYWORD = 'input/SET_INPUT_KEYWORD';
const SET_INPUT_FOODNAME = 'input/SET_INPUT_FOODNAME';

export const setInputKeyword = createAction(SET_INPUT_KEYWORD);
export const setInputFoodname = createAction(SET_INPUT_FOODNAME);

const initialState = Map({
    keyword: '',
    foodname: ''
});

export default handleActions({
    [SET_INPUT_KEYWORD]: (state, action) => {
        return state.set('keyword', action.payload);
    },
    [SET_INPUT_FOODNAME]: (state, action) => {
        return state.set('foodname', action.payload);
    }
}, initialState);