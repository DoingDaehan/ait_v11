import { Map, List } from 'immutable';
import { handleActions, createAction } from 'redux-actions';

const INSERT = 'foods/INSERT';
const TOGGLE = 'foods/TOGGLE';
const REMOVE = 'foods/REMOVE';

export const insert = createAction(INSERT);
export const toggle = createAction(TOGGLE);
export const remove = createAction(REMOVE);


// this.state = {
//     //response: false,
//     endpoint: "http://192.168.0.23:",
//     //color: 'white',
//     keyword: '',
//     foodname: '',                
//     data: [],            
//     selectedFile: null,
//     loaded: 0
// }

const initialState = Map({
    endpoint: "http://192.168.0.23:",
    keyword: '',
    foodname: '',
    data: []
});

export default handleActions({
    [INSERT]: (state, action) => {        
        const { keyword, foodname } = action.payload;
        console.log(action.payload);
        
        const item = Map({
            "attribute": 
            {
                "keyword": action.payload.keyword,
                "foodname": action.payload.foodname
            }
        });

        return state.update('data', data => data.push(item));
    },
    [TOGGLE]: (state, action) => {
        // const { payload: id } = action;

        // const index = state.findIndex(todo => todo.get('id') === id);

        // return state.updateIn([index, 'done'], done => !done);
        //     const {data} = this.state;
        // const index = data.findIndex(d => d._id === id);

        
        // if(index !== -1){
        
        //     let tDone = !data[index].attribute.done;

        //     fetch("/list/askconfirm", {
        //         method: 'POST',
        //         headers: {'Content-Type':'application/json'},
        //         body: JSON.stringify({
        //             "id": data[index]._id,
        //             "done": tDone                
        //         })
        //     })   
        //     .then(res => res.json())    
        //     .then(data => {
        //         console.log(data);
        //         let newState = update(this.state, {
        //             data: {
        //                 [index]: {
        //                     attribute: {$set: data.attribute}
        //                 }
        //             } 
        //         });
            
        //         this.setState(newState);
        //     });    
        // }
    },
    [REMOVE]: (state, action) => {
        const { 
            payload:id,
            payload:data 
        } = action;
        
        const index = data.findIndex(d => d._id === id);
       
        if(index !== -1){          

            fetch("/api/list/remove", {
                method: 'POST',
                headers: {'Content-Type':'application/json'},
                body: JSON.stringify({
                    "id": data[index]._id,
                    "status": data[index].attribute.state
                })
            })              
            .then(data => {      
                console.log(data);                
                state.delete(index);                
            });    
        }

        return state;
    }
}, initialState);