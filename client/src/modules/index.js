import input from './input';
import foods from './foods';
import auth from './auth';
import { combineReducers } from 'redux';

export default combineReducers({
    input,
    foods,
    auth
});