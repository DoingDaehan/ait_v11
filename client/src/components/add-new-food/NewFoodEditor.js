import React, {Component} from 'react';
import { Card, CardBody, Form, FormInput, Button } from "shards-react";

import PropTypes from 'prop-types';

import "react-quill/dist/quill.snow.css";
import "../../assets/quill.css";

class NewFoodList extends Component {
  // shouldComponentUpdate(nextProps, nextState){
  //   return this.props.todos !== nextProps.todos;
  // }

  constructor(props){
    super(props);
    // this.state = {
    //   foodname:"",
    //   keyword:"",
    //   foodList:[]
    // }
  }
  
  static propTypes = {
    onInsert: PropTypes.func
  }

  static defaultProps = {
      onInsert: () => { console.error('onInsert not defined!')}
  }

  handlekeyPress = (e) => {
    if(e.key === 'Enter') {
        this.props.onInsert();
    }
  }
 
  handleSubmit = (e) => {
    e.preventDefault();
    
    this.onInsert();
  }

  render() {
    const {onInsert, onChange, keyword, foodname} = this.props;

    return (
      <Card small className="mb-3">
        <CardBody>
          {/* <Form className="add-new-food" onSubmit={this.handleSubmit}> */}
          <Form className="add-new-food">            
            <FormInput 
                  name="foodname" 
                  size="lg" className="mb-3" placeholder="Your Food Name" 
                  onChange={onChange} 
                  foodname={foodname}/>
            <FormInput 
                  name="keyword" 
                  size="lg" className="mb-3" placeholder="Your Directory Name " 
                  onChange={onChange} 
                  keyword={keyword}
                  onKeyPress={this.handlekeyPress} />        
            <Button onClick={onInsert}>Add New Food</Button>
          </Form>
        </CardBody>
      </Card>
    )
  };
}

export default NewFoodList;