import React, { Component } from 'react'
import Files from "react-butterfiles";
import {Progress} from 'reactstrap';
import { Card, CardBody, Form,  Button } from "shards-react";
import NewFoodInput from './NewFoodInput';

export default class NewFoodInputTemplate extends Component {
    constructor(props) {
        super(props);

        this.state = {
            errors: ""
        }
    }

    render() {
        const {
            keyword, 
            foodname, 
            onCrawlingStart, 
            onInsertFood,
            onChange,
            loaded,
            onDragDrop
        } = this.props;

        return (
            <Files
                multiple={true} 
                maxSize="2mb"
                multipleMaxSize="10mb"
                multipleMaxCount={3}
                // accept={["application/pdf","image/jpg","image/jpeg"]}
                accept={".txt"}
                onSuccess={(files) => { onDragDrop(files)}}
                onError={errors => this.setState({ errors })}
            >
                {({ browseFiles, getDropZoneProps }) => (
                    <div>
                        <label>아래에 크롤링 리스트 파일을 Drag & Drop 하세요</label>
                        <div
                            {...getDropZoneProps({
                                // style: {
                                //     width: 600,
                                //     minHeight: 200,
                                //     border: "2px lightgray dashed"
                                // }
                            })}
                        >
            
                            <Card small className="mb-3">
                                <CardBody>                                        
                                    <Form className="add-new-food">                                    
                                        <NewFoodInput 
                                            name="keyword"                             
                                            onChange={onChange} 
                                            cPlaceholder="Food Search Keyword"
                                            value={keyword}
                                            />                                                
                                        <NewFoodInput 
                                            name="foodname"                            
                                            onChange={onChange}
                                            cPlaceholder="Food Directory"
                                            value={foodname} 
                                            onInsert={onInsertFood} />
                                        <Button onClick={onInsertFood}>음식 추가 : 크롤링 시작</Button>
                                        <Button onClick={onCrawlingStart}>크롤링 시작</Button>
                                    </Form>
                                    <Progress max="100" color="success" value={loaded} >{Math.round(loaded,2) }%</Progress>
                                    {/* <div>
                                        Dragging not convenient? Click{" "}
                                        <button onClick={browseFiles}>here</button> to select files.
                                    </div> */}
                                </CardBody>
                            </Card>
                            {/* <ol>
                                {this.state.files.map(file => (
                                    console.log(file.src)                                            
                                ))}
                                
                            </ol> */}
                        </div>
                        
                    </div>
                )}
            </Files>
        )
    }
}
