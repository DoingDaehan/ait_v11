import React, {Component} from 'react';
import PropTypes from 'prop-types';
import NewFood from './NewFood';

class NewFoodList extends Component {

    
    shouldComponentUpdate(nextProps, nextState){
      return this.props.data !== nextProps.data;
    }

    static propTypes = {
        onToggle: PropTypes.func,
    }

    static defaultProps = {
        data: [],
        onToggle: () => { console.error('onToggle not defined!!')}        
    }

    render() {
        let index = 1;
        const {data, onToggle, onRemove, onFailHandler, type} = this.props;
        let list;
        console.log(data.length)
        if(data.length !== undefined && data.length !== 0){

        
          list = data.map(
              d => (<NewFood 
                        type={type}
                        data={d} 
                        key={d._id}
                        done={d.attribute.done}
                        index={index++} 
                        onRemove={() => onRemove(d._id)}
                        onToggle={() => onToggle(d._id)}
                        onFailHandler={() => onFailHandler(d._id)}
                        />
                    )
          );
        }

        return (                        
            <table className="table mb-0">
                <thead className="bg-light">
                  <tr>
                    <th scope="col" className="border-0">
                    
                    </th>
                    <th scope="col" className="border-0">
                      #
                    </th>
                    <th scope="col" className="border-0">
                      폴더이름
                    </th>
                    <th scope="col" className="border-0">
                      검색어
                    </th>
                    <th scope="col" className="border-0">
                      현재 작업 상태
                    </th>                    
                    <th scope="col" className="border-0">
                      전체 진행률
                    </th>
                    <th scope="col" className="border-0">
                      음식DB
                    </th>
                  </tr>
                </thead>
                <tbody>
                  
                     {list}                 
                  
                </tbody>
              </table>
        );
    }
}

NewFoodList.propTypes = {
  data: PropTypes.array
}

export default NewFoodList;