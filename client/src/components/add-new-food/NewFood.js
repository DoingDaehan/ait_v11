import React, { Component } from 'react';
import { Link } from "react-router-dom";
import ProgressBar from 'react-bootstrap/ProgressBar'
import ProcessStatusManager from "../../utils/ProcessStatusManager";
import { Button } from "shards-react";


const job = ProcessStatusManager;

class NewFood extends Component {    

    // DO NOT BE ENABLE !!!! for socket.io update status
    // shouldComponentUpdate(nextProps, nextState){
    //     return this.props.data.attribute.state !== nextProps.data.attribute.state;
    // }
   
    NewFoodButton =  (props)=> {
        const { status, onRemove, onFailHandler } = props;
        
        // console.log("NewFoodButton :::", status);
        if(status === "학습 실패") {
            return <Button outline theme="danger" size="sm" 
                onClick={
                    (e) => {
                        onFailHandler();
                        e.stopPropagation();
                        }
                }
                >되돌리기</Button>;            
        } else if(status === "크롤링 실패"){
            return <Button outline theme="danger" size="sm" 
                onClick={(e) => {
                onRemove();
                e.stopPropagation();
            }}>지우기</Button>;                        
        } else {
            return <Button outline theme="danger" size="sm" 
                    onClick={(e) => {
                    onRemove();
                    e.stopPropagation();
                }}>지우기</Button>;                        
        }  
    }
    
    render(){        
        const {done, onToggle, onRemove, onFailHandler, type} = this.props;
        const {attribute, source, _id} = this.props.data;
        
        //console.log(job.properties[attribute.state]);
        let statusObj;        
        let now;
        let status;
        let mStatus = [];                
        let checkConfirm = null;        
        let statusCheck = [];
        
        const style = {
            normal: {
                backgroundColor: '#FFFFFF'
            },
            warning: {
                backgroundColor: '#E86711'
            },
            error: {
                backgroundColor: '#F16C6E'
            }
        };
        
        if(job.properties[attribute.state] !== undefined){
            statusObj = job.properties[attribute.state];        
            now = statusObj.value;
            status = statusObj.name;
            mStatus = [];
            
            mStatus.push(job.properties[job.IDLE]);
            mStatus.push(job.properties[job.CROPPING_DONE]);
            mStatus.push(job.properties[job.CRAWLING_DONE]);
            mStatus.push(job.properties[job.CLASSIFYING_DONE]);
            mStatus.push(job.properties[job.LEARNING_DONE]);
            mStatus.push(job.properties[job.CROPPING]);
            mStatus.push(job.properties[job.READY_FOR_CONFIRM]);
            mStatus.push(job.properties[job.READY_FOR_TRAINING]);
    
            // console.log(attribute.state, status);
            checkConfirm = null;
            if(attribute.state === job.CROPPING_DONE || attribute.state === job.READY_FOR_TRAINING || attribute.state === job.READY_FOR_CONFIRM){
                checkConfirm = <td><input onClick={onToggle} type="checkbox" checked={done} readOnly/></td>;
            } else{
                checkConfirm = <td></td>;
            }
    
            statusCheck = mStatus.filter( (s) => {
                //console.log(s.name, status);
                return s.name === status
            });
            // console.log(statusCheck, statusCheck.length);
        }
        

        let thisStyle;
        if(status === "학습 실패" || status === "크롤링 실패"){
            thisStyle = style.error;
        } else {
            thisStyle = style.normal;
        }

        console.log(status, type);
        

        return (
            //<tr key={_id} style={{backgroundColor: done ? "#E2E2E2":"white"}}>
            <tr key={_id} style={thisStyle}>
                {checkConfirm}
                <td>{this.props.index}</td>
                <td>
                    {
                        (statusCheck.length) !== 0 ?
                        <Link
                            to={{
                                pathname: `/foodlist/${_id}`,
                                query:{
                                    status: attribute.state,
                                    type: type
                                }
                            }}>{attribute.name}                     
                        </Link>
                        :
                        <div>{attribute.name}</div>
                    }
                </td>
                <td>{attribute.keyword}</td>
                <td>                    
                    {status}
                </td>
                {/* <td>{attribute.is_activated ? "True":"False"}</td> */}
                <td>                    
                    <ProgressBar now={now} label={`${now}%`} />
                </td>
                <td>{source.filename}</td>
                <td>         
                    <this.NewFoodButton onRemove={onRemove} onFailHandler={onFailHandler} status={status}/>
                </td>              
            </tr>                        
            
        );
    }
}

export default NewFood;