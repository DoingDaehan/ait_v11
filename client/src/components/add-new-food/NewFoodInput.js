import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { FormInput } from "shards-react";

import "react-quill/dist/quill.snow.css";
import "../../assets/quill.css";

class NewFoodList extends Component {

  static propTypes = {
    onInsert: PropTypes.func
  }

  static defaultProps = {
      onInsert: () => { console.error('onInsert not defined!')}
  }


  handlekeyPress = (e) => {    
    if(e.key === 'Enter') {
        this.props.onInsert();
    }
  }

  render() {
    const { name, onChange, cPlaceholder, value } = this.props;

    return (      
      <FormInput 
            name={name}
            size="lg" className="mb-3" placeholder={cPlaceholder}
            onChange={onChange} 
            onKeyPress={this.handlekeyPress}
            value={value}
            />        
    )
  };
}

export default NewFoodList;