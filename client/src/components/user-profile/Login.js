import React from "react";
import PropTypes from "prop-types";
import {
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormGroup,
  FormInput,
  FormSelect,
  FormTextarea,
  Button
} from "shards-react";
import { Link } from 'react-router-dom';

const Login = ({ onChange, onLogin, email, password, value }) => (
  <Card small className="mb-4">
    {/* <CardHeader className="border-bottom">
      <h6 className="m-0">{title}</h6>
    </CardHeader> */}
    <ListGroup flush>
      <ListGroupItem className="p-3">
        <Row>
          <Col>
            <Form>              
              <Row form>
                {/* Email */}
                <Col md="6" className="form-group">
                  <label htmlFor="feEmail">Email</label>
                  <FormInput
                    name="email"
                    type="email"
                    id="feEmail"
                    placeholder="Email Address"
                    value={value}
                    onChange={(e) => onChange(e)}
                    autoComplete="email"
                  />
                </Col>
                {/* Password */}
                <Col md="6" className="form-group">
                  <label htmlFor="fePassword">Password</label>
                  <FormInput
                    name="password"
                    type="password"
                    id="fePassword"
                    placeholder="Password"
                    value={value}
                    onChange={(e) => onChange(e)}
                    autoComplete="current-password"
                  />
                </Col>
              </Row>                  
              <Row form>
                <Col>
                  <Button onClick={onLogin} theme="accent" >로그인</Button>
                </Col>
              </Row>              
            </Form>
          </Col>
        </Row>        
      </ListGroupItem>
    </ListGroup>
    <div className="footer">
      <div className="card-content">
          <div className="right" >
          New Here? <Link to="/register">계정 생성</Link>
          </div>
      </div>
    </div>
  </Card>
  
);

Login.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string
};

// Login.defaultProps = {
//   title: "Account Details"
// };

export default Login;
