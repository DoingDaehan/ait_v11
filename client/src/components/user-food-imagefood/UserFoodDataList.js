import React, {Component} from 'react';
import PropTypes from 'prop-types';
import UserFood from './UserFood';

class UserFoodDataList extends Component {

    
    shouldComponentUpdate(nextProps, nextState){
      return this.props.data !== nextProps.data;
    }

    static propTypes = {
        onToggle: PropTypes.func,
    }

    static defaultProps = {
        data: [],
        onToggle: () => { console.error('onToggle not defined!!')}        
    }

    render() {
        let index = 1;
        const {data, onToggle, onRemove, onFailHandler, type, onClassifying, onDownloadingUserData} = this.props;
        let list;
        console.log(data.length)
        if(data.length !== undefined && data.length !== 0){
        
          list = data.map(
              d => (<UserFood                         
                        data={d} 
                        key={d._id}
                        done={d.attribute.done}
                        index={index++} 
                        onRemove={() => onRemove(d._id)}
                        onToggle={() => onToggle(d._id)}
                        onFailHandler={() => onFailHandler(d._id)}
                        onClassifying={() => onClassifying(d._id)}
                        onDownloadingUserData={() => onDownloadingUserData(d._id)}
                        />
                    )
          );
        }

        return (                        
            <table className="table mb-0">
                <thead className="bg-light">
                  <tr>
                    <th scope="col" className="border-0">
                    
                    </th>
                    <th scope="col" className="border-0">
                      #
                    </th>
                    <th scope="col" className="border-0">
                      사용자 입력일
                    </th>              
                    <th scope="col" className="border-0">
                      이미지 수
                    </th>
                    <th scope="col" className="border-0">
                      민감정보 수
                    </th>
                    <th scope="col" className="border-0">
                      그 외 (Food/None)
                    </th>                    
                    <th scope="col" className="border-0">
                      현재 작업 상태
                    </th>                    
                    <th scope="col" className="border-0">
                      추가된 날짜
                    </th>
                    <th scope="col" className="border-0">
                      완료된 날짜
                    </th>
                    <th scope="col" className="border-0">
                      
                    </th>
                    <th scope="col" className="border-0">
                      
                    </th>
                  </tr>
                </thead>
                <tbody>
                  
                     {list}                 
                  
                </tbody>
              </table>
        );
    }
}

UserFoodDataList.propTypes = {
  data: PropTypes.array
}

export default UserFoodDataList;