import React from 'react';
import {Link} from 'react-router-dom';
import '../../App.css';

class Header extends React.Component {
    render() {

        const loginButton = (
            <li>
                <Link to="/login">
                    <i className="material-icons">vpn_key</i>
                </Link>
            </li>
        );

        return (
            <div>
            <div>Header</div>
            <nav>
            <div className="nav-wrapper blue darken-1">
                <a className="brand-logo center">AIT</a>

                <ul>
                <li><a><i className="material-icons">search</i></a></li>
                </ul>

                <div className="right">
                <ul>
                    <li>
                    <a><i className="material-icons">{loginButton}</i></a>
                    </li>
                    <li>
                    <a><i className="material-icons">lock_open</i></a>
                    </li>
                </ul>
                </div>
            </div>
            </nav>
            </div>
        );
    }
}

export default Header;