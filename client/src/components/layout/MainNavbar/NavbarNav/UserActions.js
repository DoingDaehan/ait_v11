import React from "react";
import { Link } from "react-router-dom";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Collapse,
  NavItem,
  NavLink
} from "shards-react";
import { connect } from 'react-redux';
import Cookies from 'universal-cookie';
import { loginRequest } from '../../../../actions/authentication';
import { getStatusRequest, logoutRequest } from '../../../../actions/authentication';
const cookies = new Cookies();

class UserActions extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: false,
      loginData: {}
    };

    this.toggleUserActions = this.toggleUserActions.bind(this);
  }

  toggleUserActions() {
    this.setState({
      visible: !this.state.visible
    });
  }

  componentDidMount() {
    let loginData = cookies.get('key');
                
    // if loginData is undefined, do nothing
    if(typeof loginData === "undefined") return;

    // decode base64 & parse json
    loginData = JSON.parse(atob(loginData));
    
    this.setState({
      loginData: loginData
    }, () => {
      console.log(this.state.loginData)
    })
  }

  handleLogout = () => {
    this.props.logoutRequest(this.state.loginData.token).then(
        () => {
            // Materialize.toast('Good Bye!', 2000);
            console.log('Good Bye');

            // EMPTIES THE SESSION
            let loginData = {
                isLoggedIn: false,
                username: '',
                token: ''
            };

            // document.cookie = 'key=' + btoa(JSON.stringify(loginData));
            cookies.set('key', btoa(JSON.stringify(loginData)));
        }
    );
  }

  render() {
    
    return (
      <NavItem tag={Dropdown} caret toggle={this.toggleUserActions}>
        <DropdownToggle caret tag={NavLink} className="text-nowrap px-3">
          <img
            className="user-avatar rounded-circle mr-2"
            src={require("./../../../../images/avatars/0.jpg")}
            alt="User Avatar"
          />{" "}
          <span className="d-none d-md-inline-block">{this.state.loginData.username}</span>
        </DropdownToggle>
        <Collapse tag={DropdownMenu} right small open={this.state.visible}>
          <DropdownItem tag={Link} to="user-profile">
            <i className="material-icons">&#xE7FD;</i> Profile
          </DropdownItem>
          <DropdownItem tag={Link} to="edit-user-profile">
            <i className="material-icons">&#xE8B8;</i> Edit Profile
          </DropdownItem>
          <DropdownItem tag={Link} to="file-manager-list">
            <i className="material-icons">&#xE2C7;</i> Files
          </DropdownItem>
          <DropdownItem tag={Link} to="transaction-history">
            <i className="material-icons">&#xE896;</i> Transactions
          </DropdownItem>
          <DropdownItem divider />
          <DropdownItem tag={Link} to="/" className="text-danger" onClick={this.handleLogout}>
            <i className="material-icons text-danger" >&#xE879;</i> Logout
          </DropdownItem>
        </Collapse>
      </NavItem>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      status: state.authentication.login.status,
      token: state.authentication.login.token
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
      loginRequest: (id, pw) => { 
          return dispatch(loginRequest(id,pw)); 
      },
      getStatusRequest: () => {
          return dispatch(getStatusRequest());
      },
      logoutRequest: (token) => {
          return dispatch(logoutRequest(token));
      }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserActions);