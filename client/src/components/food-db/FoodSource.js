import React, { Component } from 'react';

class FoodSource extends Component {
    render(){
        const { index, file_name, count, national_code } = this.props.data;

        return (
            <tr>
                <td>{index}</td>
                <td>{national_code}</td>
                <td>{file_name}</td>
                <td>{count}</td>               
            </tr>            
        )
    }
}

export default FoodSource;