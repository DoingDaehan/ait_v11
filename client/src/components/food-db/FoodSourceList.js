import React, {Component} from 'react';
import FoodSource from './FoodSource';

class FoodSourceList extends Component {

    static defaultProps = {
        data: []
    }

    render() {
        const {data} = this.props;
        const list = data.map(
            d => (<FoodSource data={d} key={d.index} />)
        );

        return (                        
            <table className="table mb-0">
                <thead className="bg-light">
                  <tr>
                    <th scope="col" className="border-0">
                      #
                    </th>
                    <th scope="col" className="border-0">
                      National Code
                    </th>
                    <th scope="col" className="border-0">
                      File Name
                    </th>
                    <th scope="col" className="border-0">
                      Count
                    </th>                    
                  </tr>
                </thead>
                <tbody>
                  
                     {list}                 
                  
                </tbody>
              </table>
        );
    }
}

export default FoodSourceList;