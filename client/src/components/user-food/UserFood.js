import React, { Component } from 'react';
import { Link } from "react-router-dom";
import ProgressBar from 'react-bootstrap/ProgressBar'
import ProcessStatusManager from "../../utils/ProcessStatusManager";
import { Button } from "shards-react";
import Enum from 'enum';
const classifiedTypeEnum = new Enum({'FACE': 'face', 'FOOD': 'food', 'NONE_FOOD': 'nonefood'});

const job = ProcessStatusManager;

class UserFood extends Component {    
    
    // shouldComponentUpdate(nextProps, nextState){
    //     return this.props.data.attribute.state !== nextProps.data.attribute.state;
    // }
   
    NewFoodButton =  (props)=> {
        const { status, onRemove, onFailHandler } = props;
        
        // console.log("NewFoodButton :::", status);
        if(status === job.properties[job.LEARNING_FAIL].name) {
            return <Button outline theme="danger" size="sm" 
                onClick={
                    (e) => {
                        onFailHandler();
                        e.stopPropagation();
                        }
                }
                >되돌리기</Button>;            
        } else if(status === job.properties[job.CRAWLING_FAIL].name){
            return <Button outline theme="danger" size="sm" 
                onClick={(e) => {
                onRemove();
                e.stopPropagation();
            }}>지우기</Button>;                        
        } else {
            return <Button outline theme="danger" size="sm" 
                    onClick={(e) => {
                    onRemove();
                    e.stopPropagation();
                }}>지우기</Button>;                        
        }  
    }

    ClassifyingButton =  (props)=> {
        const { status, onRemove, onFailHandler, onClassifying, onDownloadingUserData } = props;
        
        // console.log("NewFoodButton :::", status);
        if(status === job.properties[job.LEARNING_FAIL].name) {
            return <Button outline theme="warn" size="sm" 
                onClick={
                    (e) => {
                        onFailHandler();
                        e.stopPropagation();
                        }
                }
                >되돌리기</Button>;            
        } else if(status === job.properties[job.CRAWLING_FAIL].name){
            return <Button outline theme="info" size="sm" 
                onClick={(e) => {
                    onClassifying();
                e.stopPropagation();
            }}>지우기</Button>;        
        } else if(status === job.properties[job.DOWNLOADING_FILE_LIST_DONE].name) {
            return <Button outline theme="info" size="sm" 
                onClick={(e) => {
                    onDownloadingUserData();
                    e.stopPropagation();
            }}>이미지 다운로드</Button>;                        
        } else if(status === job.properties[job.DOWNLOADING_IMAGES_DONE].name) {
            return <Button outline theme="info" size="sm" 
                onClick={(e) => {
                    onClassifying();
                    e.stopPropagation();
            }}>분류하기</Button>;                        
        } else {
            return <div></div>
        }  
    }
    
    render(){        
        const {done, onToggle, onRemove, onFailHandler, onClassifying, onDownloadingUserData} = this.props;
        const {attribute, sensitiveImages, _id, updateAt, createdAt} = this.props.data;
        
        //console.log(job.properties[attribute.state]);
        let statusObj;        
        let now;
        let status;
        let mStatus = [];                
        let checkConfirm = null;        
        let statusCheck = [];
        
        const style = {
            normal: {
                backgroundColor: '#FFFFFF'
            },
            warning: {
                backgroundColor: '#E86711'
            },
            error: {
                backgroundColor: '#F16C6E'
            }
        };
        
        if(job.properties[attribute.status] !== undefined){
            statusObj = job.properties[attribute.status];        
            now = statusObj.value;
            status = statusObj.name;
            mStatus = [];
            
            mStatus.push(job.properties[job.IDLE]);
            mStatus.push(job.properties[job.CROPPING_DONE]);
            mStatus.push(job.properties[job.CRAWLING_DONE]);
            mStatus.push(job.properties[job.CLASSIFYING_DONE]);
            mStatus.push(job.properties[job.LEARNING_DONE]);
            mStatus.push(job.properties[job.CROPPING]);
            mStatus.push(job.properties[job.READY_FOR_CONFIRM]);
            mStatus.push(job.properties[job.READY_FOR_TRAINING]);
            // mStatus.push(job.properties[job.DOWNLOADING_FILE_LIST_DONE]);
            mStatus.push(job.properties[job.DOWNLOADING_IMAGES_DONE]);
            
    
            // console.log(attribute.state, status);
            checkConfirm = null;
            if(attribute.status === job.CROPPING_DONE || attribute.status === job.READY_FOR_TRAINING || attribute.status === job.READY_FOR_CONFIRM){
                checkConfirm = <td><input onClick={onToggle} type="checkbox" checked={done} readOnly/></td>;
            } else{
                checkConfirm = <td></td>;
            }
    
            statusCheck = mStatus.filter( (s) => {
                //console.log(s.name, status);
                return s.name === status
            });
            // console.log(statusCheck, statusCheck.length);
        }
        

        let thisStyle;
        if(status === job.properties[job.LEARNING_FAIL].name || status === job.properties[job.CRAWLING_FAIL].name){
            thisStyle = style.error;
        } else {
            thisStyle = style.normal;
        }

        return (
            //<tr key={_id} style={{backgroundColor: done ? "#E2E2E2":"white"}}>
            <tr key={_id} style={thisStyle}>
                {checkConfirm}
                <td>{this.props.index}</td>
                <td>                    
                    <div>{attribute.selectedDate}</div>                    
                </td>
                <td>
                    {attribute.listCount}
                </td>
                <td>
                    {
                        (attribute.faceCount != 0) ?
                        <Link
                            to={{
                                pathname: `/foodlist/user/${_id}`,
                                query:{
                                    status: attribute.status,
                                    type: classifiedTypeEnum.FACE.value
                                }
                            }}>{attribute.faceCount}
                        </Link>
                        : <div>{attribute.faceCount}</div>
                    }
                </td>
                <td>
                    {
                        (attribute.foodCount != 0) ?
                        <Link
                            to={{
                                pathname: `/foodlist/user/${_id}`,
                                query:{
                                    status: attribute.status,
                                    type: classifiedTypeEnum.FOOD.value
                                }
                            }}>{attribute.foodCount}
                        </Link>
                        : <div>{attribute.foodCount}</div>
                    }/
                    {
                        (attribute.noneFoodCount != 0) ?
                        <Link
                            to={{
                                pathname: `/foodlist/user/${_id}`,
                                query:{
                                    status: attribute.status,
                                    type: classifiedTypeEnum.NONE_FOOD.value
                                }
                            }}>{attribute.noneFoodCount}
                        </Link>
                        : <div>{attribute.noneFoodCount}</div>
                    }
                </td>
                <td>                    
                    {status}
                </td>
                {/* <td>{attribute.is_activated ? "True":"False"}</td> */}
                <td>                    
                    {/* <ProgressBar now={now} label={`${now}%`} /> */}
                    { createdAt }
                </td>
                <td>
                {/* {source.filename} */}
                { attribute.finishedDate }
                </td>
                <td>         
                    <this.ClassifyingButton onDownloadingUserData={onDownloadingUserData} onClassifying={onClassifying} onFailHandler={onFailHandler} status={status}/>
                </td>     
                <td>         
                    <this.NewFoodButton onRemove={onRemove} onFailHandler={onFailHandler} status={status}/>
                </td>              
            </tr>                        
            
        );
    }
}

export default UserFood;