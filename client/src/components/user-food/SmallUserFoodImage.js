import React, { Component } from 'react'
import {    
    Row,    
    Card,
    CardBody,
    CardImg    
  } from "shards-react";
import PropTypes from 'prop-types';
import ButtonHotkey from "../foodlist/ButtonForSmall";

export default class SmallUserFoodImage extends Component {
    
    static propTypes = {
        onClick: PropTypes.func
    }

    static defaultProps = {
        onClick: () => { console.error('onClick not defined!')}
    }

    onImgLoad({target:img}) {
        //console.log(img.offsetHeight, img.offsetWidth)        
    }

    render() {
        
        const { post, onClick, currentIdx } = this.props;

        return (     
     
            <Card small className="card-post card-post--1">
                <CardImg 
                    // src={"/foodlist/exist/" + post.backgroundImage}
                    src={post.backgroundImage}
                    // src="/foodlist/exist/godeungeo_jorim/109eb14b13944b72b798cbc44b779486.jpg"
                    className="card-post__image"
                    //style={{ backgroundImage: `url(${post.backgroundImage})`}}                                        
                    style={{ maxHeight: "170px" }}                    
                    onLoad={this.onImgLoad}
                />    
                
                <CardBody>                   
                    <Row>
                        <div className="btn-group btn-group-toggle mb-3" data-toggle="buttons">                            
                            <ButtonHotkey currentIdx={currentIdx} filename={post.backgroundImage} post={post} onClick={onClick} value={"blind"} isActive={post.isSensitive === true ? "active":""} buttonColor={"btn-outline-success"}/>
                            <ButtonHotkey currentIdx={currentIdx} filename={post.backgroundImage} post={post} onClick={onClick} value={"none"} isActive={(post.isSensitive === false || post.isSensitive === undefined) ? "active":""} buttonColor={"btn-outline-secondary"}/>                       
                        </div>
                    </Row>
                    
                    <Row>
                    {/* <Button filename={post.backgroundImage}
                            onClick={(e) => {
                        this.handleRemove(e);                      
                    }}>삭제</Button> */}
                    </Row>

                    
                </CardBody>
            </Card>
            
        )
    }
}
