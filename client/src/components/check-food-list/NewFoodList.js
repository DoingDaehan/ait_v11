import React, {Component} from 'react';
import PropTypes from 'prop-types';
import NewFood from './NewFood';

class NewFoodList extends Component {

    
    shouldComponentUpdate(nextProps, nextState){
      return this.props.data !== nextProps.data;
    }

    static defaultProps = {
        data: [],        
    }

    render() {
        let index = 1;
        const {data, onCancelConfirm} = this.props;
        
        const list = data.map(
            d => (<NewFood 
                      data={d} 
                      key={d._id}
                      done={d.attribute.done}
                      index={index++} 
                      onCancelConfirm={() => onCancelConfirm(d._id)}                      
                      />
                  )
        );


        return (                        
            <table className="table mb-0">
                <thead className="bg-light">
                  <tr>
                    <th scope="col" className="border-0">
                    
                    </th>
                    <th scope="col" className="border-0">
                      #
                    </th>
                    <th scope="col" className="border-0">
                      음식이름
                    </th>
                    <th scope="col" className="border-0">
                      키워드
                    </th>
                    <th scope="col" className="border-0">
                      현재 작업 상태
                    </th>                    
                    <th scope="col" className="border-0">
                      전체 진행률
                    </th>
                    <th scope="col" className="border-0">
                      음식DB
                    </th>
                  </tr>
                </thead>
                <tbody>
                  
                     {list}                 
                  
                </tbody>
              </table>
        );
    }
}

NewFoodList.propTypes = {
  data: PropTypes.array
}

export default NewFoodList;