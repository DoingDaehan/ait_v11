import React, { Component } from 'react';
import { Link } from "react-router-dom";
import ProgressBar from 'react-bootstrap/ProgressBar'
import ProcessStatusManager from "../../utils/ProcessStatusManager";
import PropTypes from 'prop-types';
import { Button } from "shards-react";

const job = ProcessStatusManager;
class NewFood extends Component {    

    // DO NOT BE ENABLE !!!! for socket.io update status
    // shouldComponentUpdate(nextProps, nextState){
    //     return this.props.done !== nextProps.done;
    // }

    static propTypes = {
        onCancelConfirm: PropTypes.func,
    }

    static defaultProps = {        
        onCancelConfirm: () => { console.error('onCancelConfirm not defined!!')}        
    }

    render(){        
        const {onCancelConfirm} = this.props;
        const {attribute, source, _id} = this.props.data;

        
        let statusObj = job.properties[attribute.state];
        let now = statusObj["value"];
        let status = statusObj["name"];
        let mStatus = [];

        mStatus.push(job.properties[job.CROPPING_DONE]);
        mStatus.push(job.properties[job.READY_FOR_CONFIRM]);
        mStatus.push(job.properties[job.READY_FOR_TRAINING]);

        console.log(attribute.state, status);
        let checkConfirm = null;
        
        return (
            <tr key={_id}>
                {checkConfirm}
                <td>{this.props.index}</td>
                <td>
                    <Link 
                        to={{
                            pathname: `/foodlist/${_id}`,
                            query:{
                                state: attribute.state
                            }
                        }}>{attribute.name}                     
                    </Link>
                </td>
                <td>{attribute.keyword}</td>
                <td>                    
                    {status}
                </td>
                {/* <td>{attribute.is_activated ? "True":"False"}</td> */}
                <td>                    
                    <ProgressBar now={now} label={`${now}%`} />
                </td>
                <td>{source.filename}</td>
                <td>                
                    <Button outline theme="warning" size="sm" 
                        onClick={(e) => {
                        onCancelConfirm();
                        e.stopPropagation();
                    }}>되돌리기</Button>
                    
                </td>              
            </tr>                        
            
        );
    }
}

export default NewFood;