import React from 'react'
import {hotkeys} from 'react-keyboard-shortcuts'
import PropTypes from 'prop-types';
import {  
  Button
} from "shards-react";
 
class ButtonWithHotKeyNext extends React.PureComponent {
  static propTypes = {
    onClick: PropTypes.func.isRequired,
  }
 
  hot_keys = {
    'right': { // combo from mousetrap
        priority: 1,
        handler: (event) => this.props.onClick(),
    },    
  }
 
  render () {
      const { value, onClick } = this.props;
    return (
      <Button onClick={onClick}>{value}</Button>
    )
  }
}
 
export default hotkeys(ButtonWithHotKeyNext)