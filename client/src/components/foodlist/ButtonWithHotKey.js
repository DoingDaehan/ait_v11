import React from 'react'
import {hotkeys} from 'react-keyboard-shortcuts'
import PropTypes from 'prop-types';
 
class ButtonWithHotKey extends React.PureComponent {
  static propTypes = {
    hotkey: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    isActive: PropTypes.string.isRequired,
    buttonColor: PropTypes.string.isRequired,
    post: PropTypes.object.isRequired,
    value: PropTypes.string.isRequired
  }
 
  hot_keys = {
    
    [this.props.hotkey]: { // combo from mousetrap
        priority: 1,
        handler: () => this.props.onClick(this.props.value, this.props.filename, this.props.currentIdx)
    },    
  }
 
  render () {
      const { onClick, post, value, isActive, buttonColor } = this.props;

      //console.log('ButtonWithHotKey:::', post);
      return (
        <label className={`mb-2 btn btn-sm ${buttonColor} mr-1 ${isActive}`}>
          <input type="radio" name="options" id="option1" filename={post.backgroundImage} onClick={() => onClick(this.props.value, this.props.filename, this.props.currentIdx)}/> {value}
        </label>
      )
  }
}
 
export default hotkeys(ButtonWithHotKey)