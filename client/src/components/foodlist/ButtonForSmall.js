import React from 'react'
// import {hotkeys} from 'react-keyboard-shortcuts'
import PropTypes from 'prop-types';
 
class ButtonForSmall extends React.PureComponent {
  static propTypes = {
    // hotkey: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    isActive: PropTypes.string.isRequired,
    buttonColor: PropTypes.string.isRequired,
    post: PropTypes.object.isRequired,
    value: PropTypes.string.isRequired
  }
 
  // hot_keys = {
    
  //   [this.props.hotkey]: { // combo from mousetrap
  //       priority: 1,
  //       handler: () => this.props.onClick([this.props.value]),
  //   },    
  // }
 
  render () {
      const { onClick, value, isActive, buttonColor, filename, currentIdx } = this.props;

      //console.log('ButtonForSmall:::', post);
      return (
        <label className={`mb-2 btn btn-sm ${buttonColor} mr-1 ${isActive}`}>
          <input type="radio" name="options" id="option1" onClick={() => onClick(value, filename, currentIdx)}/> {value}
        </label>
      )
  }
}
 
export default ButtonForSmall;