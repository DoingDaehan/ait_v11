import React, { Component } from 'react'
import {    
    Row,   
    Col, 
    Card,    
    Button,
    CardHeader
  } from "shards-react";
import PropTypes from 'prop-types';
//import ProcessStatusManager from "../../utils/ProcessStatusManager";
import ButtonHotkey from "./ButtonWithHotKey";
import ReactCrop from 'react-image-crop';
import "react-image-crop/dist/ReactCrop.css";
import update from 'react-addons-update'
import axios from 'axios';

import Cookies from 'universal-cookie';

const cookies = new Cookies();


var drawing = false;
var drawingMode = "add";
//var pencilWidth = 25;
var pencilWidth = 16;

var fillColor = "rgba(255,255,255,1)";


export default class FoodImage extends Component {
    
    constructor(props){
        super(props);

        this.state = {
            dimensions: {},
            originalFileName: "",
            post: {},
            isSaved: false,
            src: null,
            crop: {
              unit: "%",
              width: 300,
              height:0
             
            }
          };
    }

    static propTypes = {
        onClick: PropTypes.func
    }

    static defaultProps = {
        onClick: () => { console.error('onClick not defined!')}
    }

    onSelectFile = e => {
        if (e.target.files && e.target.files.length > 0) {
          const reader = new FileReader();
          reader.addEventListener("load", () =>
            this.setState({ src: reader.result })
          );
          reader.readAsDataURL(e.target.files[0]);
        }
      };
    
    // If you setState the crop in here you should return false.
    onImageLoaded = image => {
        this.imageRef = image;

        console.log("FoodImage::: ", image.offsetHeight, image.offsetWidth)        
        this.setState({dimensions:{height:image.offsetHeight,
            width:image.offsetWidth}});
    };

    onCropComplete = crop => {
        this.makeClientCrop(crop);
    };

    onCropChange = (crop, percentCrop) => {
        // You could also use percentCrop:
        // this.setState({ crop: percentCrop });
        this.setState({ crop });
    };

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                "newFile.jpeg"
            );
            this.setState({ croppedImageUrl }, () => {
                this.onLoadImageEraserCanvas(this.state.post.backgroundImage);
            });
        }
    }


    
    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement("canvas");
        
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext("2d");
        
        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );


        return new Promise((resolve, reject) => {
            canvas.toBlob(blob => {
                if (!blob) {
                    //reject(new Error('Canvas is empty'));
                    console.error("Canvas is empty");
                    return;
                }
                blob.name = fileName;
                window.URL.revokeObjectURL(this.fileUrl);
                this.fileUrl = window.URL.createObjectURL(blob);
                resolve(this.fileUrl);
                }, "image/jpeg");
        });
    }

    dataURItoBlob = (dataURI) => 
    {
        var byteString = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
        var ab = new ArrayBuffer(byteString.length);
        var ia = new Uint8Array(ab);
        for (var i = 0; i < byteString.length; i++)
        {
            ia[i] = byteString.charCodeAt(i);
        }

        var bb = new Blob([ab], { "type": mimeString });
        return bb;
    }

    handleSaveImage = (e) => {
        
        let c = document.getElementById("a");
        var d=c.toDataURL(); // png // 동작 안함 릴리즈 에서
    
        //this.props.onLoadImage(this.props.currentIdx, "");

        this.setState({
            originalFileName: this.state.post.backgroundImage,
            post: update(
                this.state.post,
                {
                    backgroundImage: {$set: ""}
                }
            ),
            isSaved: true
        },
            () => {
               
                var imageId = this.state.post.imageId;
                var originalfilename = this.state.originalFileName;                
                var status = this.props.status;
        
                const data = new FormData();
                data.append('originalfilename', originalfilename);
                data.append('imageId', imageId);                        
                data.append('file', d);
                data.append('status', status);
                data.append('type', this.props.type); // append or normal
                
                console.log('file', d);
                axios.post("/api/saveimage", data, {
                    headers: {'x-access-token': this.state.loginData.token}
                })
                .then(res => { // then print response status
                    console.log(originalfilename, res);
                    this.props.onLoadImage(this.props.currentIdx, res.data.dest);                   
                });
                            
            }
        );
    }

    onLoadImageEraserCanvas = () => {
        var canvas = document.getElementById('a');
        console.log("CCCCCCCCCCCCCCCCCCC", canvas);
        if (canvas != null && canvas.getContext) {
            const {crop } = this.state;
          
            const scaleX = this.imageRef.naturalWidth / this.imageRef.width;
            const scaleY = this.imageRef.naturalHeight / this.imageRef.height;
            canvas.width = crop.width;
            canvas.height = crop.height;
            const ctx = canvas.getContext("2d");
            
            var FindPosition = function(oElement)
            {
                if(typeof( oElement.offsetParent ) != "undefined")
                {
                    for(var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent)
                    {
                        posX += oElement.offsetLeft;
                        posY += oElement.offsetTop;
                    }
                    return [ posX, posY ];
                }
                else
                {
                    return [ oElement.x, oElement.y ];
                }
            }

            var GetCoordinates = function(e)
            {
                var PosX = 0;
                var PosY = 0;
                var ImgPos;
                ImgPos = FindPosition(canvas);
                
                if (!e) var e = window.event;
                
                if (e.pageX || e.pageY)
                {
                    PosX = e.pageX;
                    PosY = e.pageY;
                }
                else if (e.clientX || e.clientY)
                {
                    PosX = e.clientX + document.body.scrollLeft
                        + document.documentElement.scrollLeft;
                    PosY = e.clientY + document.body.scrollTop
                        + document.documentElement.scrollTop;
                }
                PosX = PosX - ImgPos[0];
                PosY = PosY - ImgPos[1];

//                console.log(PosX, PosY);
                return {
                    x : PosX,
                    y : PosY
                }
                //   document.getElementById("x").innerHTML = PosX;
                //   document.getElementById("y").innerHTML = PosY;
            }


            canvas.onmousedown = function (e) {
                console.log('onmousedonwn')
                if (drawing) return false;
                console.log('onmousedonwn 2')
                drawing = true;
                //var mouse = getMouseCoordinate.call(this, e);
                var mouse = GetCoordinates.call(this, e);
                console.log(mouse.x, mouse.y)
                
                ctx.globalCompositeOperation = drawingMode === "add" ? "source-over" : "destination-out";
                ctx.fillStyle = drawingMode === "add" ? fillColor : "rgba(255,255,255,1)";
                
                ctx.beginPath();
                ctx.arc(mouse.x, mouse.y, pencilWidth, 0, 2*Math.PI);
                ctx.fill();
                ctx.closePath();
            };
            
            canvas.onmousemove = function (e) {            
                if (!drawing) return false;
            
                //var mouse = getMouseCoordinate.call(this, e);
                var mouse = GetCoordinates.call(this, e);
            
                ctx.beginPath();
                ctx.arc(mouse.x, mouse.y, pencilWidth, 0, 2*Math.PI);
                ctx.fill();
                ctx.closePath();
            };
            
            canvas.onmouseup = function (e) {
                if (!drawing) return false;
                drawing = false;
            };

            ctx.drawImage(
                this.imageRef,
                crop.x * scaleX,
                crop.y * scaleY,
                crop.width * scaleX,
                crop.height * scaleY,
                0,
                0,
                crop.width,
                crop.height
            );
        }
    }

    componentDidMount(){
        const {post} = this.props;
        console.log(post);

        // get loginData from cookie
        let loginData = cookies.get('key');
                
        // if loginData is undefined, do nothing
        if(typeof loginData === "undefined") return;

        // decode base64 & parse json
        loginData = JSON.parse(atob(loginData));
        console.log(loginData);

        this.setState({loginData: loginData});

        this.setState({
            post:post
        }, () =>{
            console.log(this.state);
            this.onLoadImageEraserCanvas();
        });

        
        
    }

    CroppedImage = ()=> {
        const { croppedImageUrl, isSaved } = this.state;

        const style = {
            image: {
                border: '1px solid #ccc',
                background: '#fefefe',
            },
            canvasM: {
                border: '1px solid'
            }
        };

       
        if(croppedImageUrl) {
            if(!isSaved){
                return (
                    <div>
                        {/* <Image alt="Crop" width={500} height={500} style={style.image} src={croppedImageUrl} /> */}
                        <Button src={croppedImageUrl} onClick={(e) => {this.handleSaveImage(e);}}>저장</Button>
                        <canvas id="a" width="500" height="500" style={style.canvasM} ></canvas>
                        {/* Erase: <input id="erase" type="checkbox"/>
                        <button id="toBase64" type="button">addToAOILayer</button><br/> */}
                        
                        {/* <canvas id="b" width="400" height="300" style={styles.canvasM}></canvas> */}
                    </div>
                );
            } else{
                return <div></div>
            }            
        } else{
            return <div></div>
        }            
      }
   
    render() {
        
        const { post, onClick, currentIdx } = this.props;
        const { crop, dimensions } = this.state;

        
        const style = {
            image: {
                flex: 1,
                alignSelf: 'stretch',
                width: 600,
                height: 600,
            },
            canvasM: {
                border: '1px solid'
            }
        };
        
        //console.log(post);
        return (     
     
            <Card small className="card-post card-post--1">
                <CardHeader>
                    <Row>
                        <div className="btn-group btn-group-toggle mb-3" data-toggle="buttons">
                            <ButtonHotkey currentIdx={currentIdx} filename={post.backgroundImage} post={post} onClick={onClick} hotkey={"1"} value={"good"} isActive={post.s1class === 1 ? "active":""} buttonColor={"btn-outline-success"}/>
                            <ButtonHotkey currentIdx={currentIdx} filename={post.backgroundImage} post={post} onClick={onClick} hotkey={"2"} value={"bad"}  isActive={post.s1class === -1 ? "active":""} buttonColor={"btn-outline-danger"}/>
                            <ButtonHotkey currentIdx={currentIdx} filename={post.backgroundImage} post={post} onClick={onClick} hotkey={"3"} value={"none"} isActive={post.s1class === 0 ? "active":""} buttonColor={"btn-outline-secondary"}/>                       
                        </div>
                    </Row>                    
                    <Row>
                    {/* <Button filename={post.backgroundImage}
                            onClick={(e) => {
                        this.handleRemove(e);                      
                    }}>삭제</Button> */}
                    </Row>      
                </CardHeader>          
                <Row>
                    <Col lg="6" md="6" sm="6" className="mb-4" key={1}>
                    {
                        post.backgroundImage && (
                        <ReactCrop
                        src={post.backgroundImage}
                        crop={crop}
                        onImageLoaded={this.onImageLoaded}
                        onComplete={this.onCropComplete}
                        onChange={this.onCropChange}
                        style={style.image}                        
                        >                        
                        </ReactCrop>
                        )
                    }                    
                    {dimensions.width} / {dimensions.height}
                    </Col>
                
                    <Col lg="6" md="6" sm="6" className="mb-4" key={2}>
                                
                        <this.CroppedImage/>                        
                        {/* <canvas id="a" width="500" height="500" style={styles.canvasM} ></canvas> */}
                

                    </Col>
                </Row>                                
            </Card>
            
        )
    }
}
