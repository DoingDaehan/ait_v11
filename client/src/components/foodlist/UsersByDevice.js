import React from "react";
import PropTypes from "prop-types";
import {
  Row,
  Col,
  FormSelect,
  Card,
  CardHeader,
  CardBody,
  CardFooter
} from "shards-react";

import Chart from "../../utils/chart";

class UsersByDevice extends React.Component {
  constructor(props) {
    super(props);

    this.canvasRef = React.createRef();
  }

  componentDidMount() {

    const { data } = this.props;
    
    console.log(data.good, data.bad, data.none, data.total)
    let mChartData = {
      datasets: [
        {
          hoverBorderColor: "#ffffff",
          data: [data.good, data.bad, data.none],
          backgroundColor: [
            "#17c671",
            "#c4183c",
            "#5a6169"
          ]
        }
      ],
      labels: ["Good", "Bad", "None"]
    }

    const chartConfig = {
      type: "pie",
      data: mChartData,
      options: {
        ...{
          legend: {
            position: "bottom",
            labels: {
              padding: 10,
              boxWidth: 10
            }
          },
          cutoutPercentage: 0,
          tooltips: {
            custom: false,
            mode: "index",
            position: "nearest"
          }
        },
        ...this.props.chartOptions
      }
    };

    new Chart(this.canvasRef.current, chartConfig);
  }

  render() {
    const { data } = this.props;
    console.log(data);
    return (
      <Card small className="h-100">
        <CardHeader className="border-bottom">
          <h6 className="m-0">전체 이미지:{data.total}</h6>
        </CardHeader>    
        <CardBody className="d-flex py-0">
          <canvas
            height="220"
            ref={this.canvasRef}
            className="blog-users-by-device m-auto"
          />
          {data.good}
        </CardBody>        
      </Card>
    );
  }
}

UsersByDevice.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string,
  /**
   * The chart config object.
   */
  chartConfig: PropTypes.object,
  /**
   * The Chart.js options.
   */
  chartOptions: PropTypes.object,
  /**
   * The chart data.
   */
  chartData: PropTypes.object
};

UsersByDevice.defaultProps = {
  title: "Users by device",
  chartData: {
    datasets: [
      {
        hoverBorderColor: "#ffffff",
        data: [200, 110, 300],
        backgroundColor: [
          "#17c671",
          "#c4183c",
          "#5a6169"
        ]
      }
    ],
    labels: ["Good", "Bad", "None"]
  }
};

export default UsersByDevice;
