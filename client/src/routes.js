// import React from "react";
// import { Redirect } from "react-router-dom";

// Layout Types
import { DefaultLayout } from "./layouts";

// Route Views
import BlogOverview from "./views/BlogOverview";
import UserProfileLite from "./views/UserProfileLite";
import AddNewPost from "./views/AddNewPost";
import AddUserFood from "./views/AddUserFood";
import ExistFoodList from "./views/ExistFood";
import UserFood from "./views/UserFood";
import UserFood_FoodImage from "./views/UserFood_FoodImage";
import Errors from "./views/Errors";
import ComponentsOverview from "./views/ComponentsOverview";
import Tables from "./views/Tables";
import BlogPosts from "./views/BlogPosts";
import FoodDBs from "./views/FoodDBs";
import AddNewFood from "./views/AddNewFood";
import CheckFoodList from "./views/CheckFoodList";
import FoodList from "./views/FoodList";
import UserFoodList from "./views/UserFoodList";
import UserFoodList_FoodImage from "./views/UserFoodList_FoodImage";
import RegisterContainer from "./containers/RegisterContainer";
import Login from "./containers/Login";
import PredictDisplay from "./views/PredictDisplay";

export default [
  {
    path: ["/"],
    exact: true,
    layout: DefaultLayout,
    // component: () => <Redirect to="/newfood" />
    //component: AddNewFood
    component: Login
  },
  {
    path: "/blog-overview",
    layout: DefaultLayout,
    component: BlogOverview
  },
  {
    path: "/user-profile-lite",
    layout: DefaultLayout,
    component: UserProfileLite
  },
  {
    path: "/add-new-post",
    layout: DefaultLayout,
    component: AddNewPost
  },
  {
    path: "/errors",
    layout: DefaultLayout,
    component: Errors
  },
  {
    path: "/components-overview",
    layout: DefaultLayout,
    component: ComponentsOverview
  },
  {
    path: "/tables",
    layout: DefaultLayout,
    component: Tables
  },
  {
    path: "/blog-posts",
    layout: DefaultLayout,
    component: BlogPosts
  },
  {
    path: "/fooddbs",
    layout: DefaultLayout,
    component: FoodDBs
  },
  {
    path: "/newfood",
    layout: DefaultLayout,
    component: AddNewFood
    //component: () => <Redirect to="/" />
  },
  {
    path: "/adduserfood",
    layout: DefaultLayout,
    component: AddUserFood
    //component: () => <Redirect to="/" />
  },
  {
    path: "/existfood",
    layout: DefaultLayout,
    component: ExistFoodList    
  },
  {
    path: "/userfood",
    layout: DefaultLayout,
    component: UserFood,
    exact: true 
  },
  {
    path: "/userfood/foodimage",
    layout: DefaultLayout,
    component: UserFood_FoodImage,
    exact: true    
  },
  {
    path: "/foodlist/:id",
    layout: DefaultLayout,
    component: FoodList,
    exact: true,
  },
  {
    path: "/foodlist/user/:id",
    layout: DefaultLayout,
    component: UserFoodList,
    exact: true,
  },
  {
    path: "/foodlist/user/foodimage/:id",
    layout: DefaultLayout,
    component: UserFoodList_FoodImage,
    exact: true,
  },
  {
    path: "/checkfoodlist",
    layout: DefaultLayout,
    component: CheckFoodList
  },
  {
    path: "/login",
    layout: DefaultLayout,
    component: Login
  },
  {
    path: "/register",
    layout: DefaultLayout,
    component: RegisterContainer
  },
  {
    path: "/predict/display",
    layout: DefaultLayout,
    component: PredictDisplay
  },
];
