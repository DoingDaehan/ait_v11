import React, {Component} from "react";
import { Container, Row, Col, Card, CardBody, Form,  Button } from "shards-react";

import NewFoodInput from '../components/add-new-food/NewFoodInput';
import PageTitle from "../components/common/PageTitle";
import NewFoodList from "../components/add-new-food/NewFoodList"
import axios from 'axios';
import update from 'react-addons-update';
import Cookies from "universal-cookie";

const cookies = new Cookies();
class PredictDisplay extends Component {

    constructor(props) {
        super(props);
        this.state = {         
            keyword: '',
            foodname: '',                            
            keywordList: [],
            data: [],            
            selectedFile: null,
            loaded: 0,
            loginData: {}
        }
    }   

    input = null;
   
    handleStatus(data) {
        this.setState({
           data:data 
        });
    }

    handleInsertFood = () => {
        let keyword = this.state.keyword;
        //let foodname = this.state.foodname;
        let keywordList = this.state.keywordList;
        let data = null;

        console.log(this.state);
        
        if(keywordList.length !== 0){
            data = {
                "keyword": keywordList
            }
        } else{
            data = {
                "keyword": keyword                
            }
        }
                
        this.setState({
            foodname: "",
            keyword: "",    
            keywordList: []
        }, () => {
            console.log(this.state);
        });
        
        axios({
            method: 'post',
            url: '/api/registeritem',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': this.state.loginData.token
            },
            data: data
        })
        .then(res => res.json())
        .then(data => {          
            console.log('BEFORE:::', this.state)
            this.setState({                
                data:data
            })
            console.log('AFTER:::', this.state)
        },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                }) 
            }
        );   
          
      
    }


    componentDidMount() {  
      
        let loginData = cookies.get('key');
        console.log(loginData);        
        loginData = JSON.parse(atob(loginData));
        console.log(loginData);

        this.setState({loginData:loginData});

        if(loginData.isLoggedIn !== true){
            this.props.history.push('/login')
        }

        let mHeaders = new Headers();
        mHeaders.append('x-access-token', loginData.token);
        let options = {
            method: 'GET',
            headers: mHeaders
        }


        fetch('/api/datas', options)
        .then(res => res.json())
        .then(data => {
            console.log(data);
          this.setState({data:data})
          //console.log(data)
          //console.log(this.state.data)
        },
        (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            }
        );     
        
     

    }

    handleToggle = (id) => {
        const {data} = this.state;
        const index = data.findIndex(d => d._id === id);

        
        if(index !== -1){
        
            let tDone = !data[index].attribute.done;

            fetch("/list/askconfirm", {
                method: 'POST',
                headers: {
                    'Content-Type':'application/json',
                    'x-access-token': this.state.loginData.token
                },
                body: JSON.stringify({
                    "id": data[index]._id,
                    "done": tDone                
                })
            })   
            .then(res => res.json())    
            .then(data => {
                console.log(data);
                let newState = update(this.state, {
                    data: {
                        [index]: {
                            attribute: {$set: data.attribute}
                        }
                    } 
                });
            
                this.setState(newState);
            });    
        }
        
    }

    handleRemove = (id) => {
        const {data} = this.state;
        const index = data.findIndex(d => d._id === id);
       
        if(index !== -1){                                

            fetch("/api/list/remove", {
                method: 'POST',
                headers: {
                    'Content-Type':'application/json',
                    'x-access-token': this.state.loginData.token
                },
                body: JSON.stringify({
                    "id": data[index]._id,
                    "status": data[index].attribute.state
                })
            })              
            .then(data => {      
                console.log(data);
                this.setState({
                    data: update(
                        this.state.data,{
                            $splice: [[index, 1]]
                        }
                        
                    )
                });
            });    
        }       
    }

    handleCopyList = () => {
        const { data } = this.state;

        let foodList = data.filter((food) => {
            return food.attribute.done = true;
        });
        let list = foodList.map((food) => {
            return food._id;
        });

        //console.log(foodList, list);
        
        fetch("/api/confirm", {
            method: 'POST',
            headers: {
                'Content-Type':'application/json',
                'x-access-token': this.state.loginData.token
            },
            body: JSON.stringify({
                "idList": list        
            })
        })              
        .then(data => {      
            console.log(data);         
        });    
    }

    
    handleFailStatus = (id) => {

        const {data} = this.state;
        const index = data.findIndex(d => d._id === id);

        axios({
            method: 'post',
            url: '/api/cancel',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': this.state.loginData.token
            },
            data: {
                id: id
            }
        })
        .then((res) => {            
            let newState = update(this.state, {
                    data: {
                        [index]: {
                            attribute: {
                                state: {$set: res.data.attribute.state}
                            }
                        }
                    } 
                });
            
            this.setState(newState, () => {
                console.log(this.state.data[index]);
            });
           
        });
        
    }

    handleChange = (e) => {    
        
        this.setState({            
            [e.target.name]: e.target.value
        }, () => {
            console.log(this.state.keyword);
        });
        
        //this.send();
    }

    onChangeHandler=event=>{
        this.setState({
          selectedFile: event.target.files[0],
          loaded: 0,
        })

        console.log(event.target.files[0]);
    }

    onClickHandler = () => {
        const data = new FormData() 
        data.append('file', this.state.selectedFile)

        axios.post("/api/addcrawlingkeywords", data, { // receive two parameter endpoint url ,form data 
        })
        .then(res => { // then print response status
            console.log(res.statusText)
        })     
    }

    render() {
        const{ data, keyword } = this.state;
 
        const {
            handleInsertFood,
            handleChange,            
            handleToggle,
            handleRemove,     
            handleFailStatus,       
        } = this;

        return (       
            

            <Container fluid className="main-content-container px-4 pb-4">
                
                {/* Page Header */}
                <Row noGutters className="page-header py-4">
                <PageTitle sm="4" title="새 음식 추가" subtitle="음식 추가" className="text-sm-left" />
                </Row>
               
                <Row>
                {/* Editor */}
                <Col lg="9" md="12">  

               
                    
                <Card small className="mb-3">
                    <CardBody>                                        
                        <Form className="add-new-food"
                                onPaste={this.handleClipboard}
                                > 
                            <NewFoodInput 
                                name="keyword"                             
                                onChange={handleChange} 
                                cPlaceholder="음식 이미지 검색어"                                                    
                                value={keyword}                                           
                                />                                                
                        
                            <Button onClick={handleInsertFood}>음식 추가 : 크롤링 시작</Button>
                            
                        </Form>
                
                    </CardBody>
                </Card>                                  
                 
               
                </Col>
               
                </Row>

                

                {/* Default Light Table */}
                <Row>
                    <Col>
                    <Card small className="mb-4">                      
                        <CardBody className="p-0 pb-3">
                        <NewFoodList                             
                            data={data} 
                            onToggle={handleToggle} 
                            onRemove={handleRemove}
                            onFailHandler={handleFailStatus}    
                        />
                        </CardBody>
                    </Card>
                    </Col>
                </Row>
            </Container>
        )
    };
};

export default PredictDisplay;
