import React, {Component} from "react";
import { Container, Row, Col, Card, CardHeader, CardBody } from "shards-react";
//import socketIOClient from "socket.io-client";

import PageTitle from "../components/common/PageTitle";
import NewFoodList from "../components/check-food-list/NewFoodList"

import update from 'react-addons-update';
import SidebarCategories from "../components/check-food-list/SidebarCategories";

import ProcessStatusManager from "../utils/ProcessStatusManager";
const job = ProcessStatusManager.JobEnum;


// var PORT;
// if (process.env.NODE_ENV === "production") {    
//   console.log("Production Mode ::: ");

//   if(process.env.NODE_ENV_PRODUCTION == "production1"){
//     PORT = process.env.PORT || 5002;  
//   } else if(process.env.NODE_ENV_PRODUCTION == "production2") {
//     PORT = process.env.PORT || 5000;
//   }
  
// } else if (process.env.NODE_ENV == 'development') {
//   PORT = process.env.PORT || 4200;
// }

// var socket;

//const socket = socketIOClient(endpoint);

class CheckFoodList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            //response: false,
            endpoint: "http://192.168.0.23:",
            //color: 'white',
            keyword: '',
            foodname: '',                
            data: []
        }

        //socket = socketIOClient.connect(this.state.endpoint + PORT + '/users');
    }   
        
    // sending sockets
    send = () => {
        //const socket = socketIOClient(this.state.endpoint);
        //socket.emit("reqMsg", {comment: this.state.keyword});
    }

    handleStatus(data) {
        this.setState({
           data:data 
        });
    }

    handleInsertFood = () => {
        let foodname = this.state.foodname;
        let keyword = this.state.keyword;

        this.setState({
            foodname: "",
            keyword: ""
        });

        fetch('/registeritem', {    
        //fetch('/api/v1/data/register', {
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "foodname": foodname,
                "keyword": keyword
            })
        })
        .then(res => res.json())
        .then(data => {          
            console.log('BEFORE:::', this.state)
            this.setState({                
                data:data
            })
            console.log('AFTER:::', this.state)
        },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                }) 
            }
        );     
    }

    getData = foodItems => {
//        console.log(foodItems);
        let index = this.state.data.findIndex((e) => {
            if(e._id == foodItems._id){
                return e;
            }
        });
        
        if(index !== -1){
            // let newState = update(this.state, {
            //     data: {
            //         [index]: {
            //             attribute: {$set: foodItems.attribute}
            //         }
            //     } 
            // });
        
            // this.setState(newState);
            this.setState({
                data: update(
                    this.state.data,
                    {
                        [index]: {
                            attribute: {$set: foodItems.attribute}
                        }
                    }
                )
            });
            console.log(">>>> getData::: ",this.state)
        }
        
    };

    componentDidMount() {  

        const { endpoint } = this.state;
        
        // socket.emit('joinRoom', {userId: 'myroom'});

        // socket.on('recMsg', function (data) {
        //     console.log("from Server:: ",data.comment)
        //     //$('#chat').append(data.comment);
        // });
      
        // socket.on('status', this.getData);
      
        fetch("/api/getfoodlist", {
            method: 'POST',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify({
                "status": job.READY_FOR_CONFIRM | job.READY_FOR_TRAINING                
            })
        })   
        .then(res => res.json())
        .then(data => {          
            this.setState({data:data})
         
            console.log(this.state.data)        
         
        });      

        console.log('componentDidMount');
    }

    handleToggle = (id) => {
        const {data} = this.state;
        const index = data.findIndex(d => d._id === id);

        
        if(index !== -1){
        
            let tDone = !data[index].attribute.done;

            fetch("/list/askconfirm", {
                method: 'POST',
                headers: {'Content-Type':'application/json'},
                body: JSON.stringify({
                    "id": data[index]._id,
                    "done": tDone                
                })
            })   
            .then(res => res.json())    
            .then(data => {
                console.log(data);
                let newState = update(this.state, {
                    data: {
                        [index]: {
                            attribute: {$set: data.attribute}
                        }
                    } 
                });
            
                this.setState(newState);
            });    
        }
        
    }

    handleRemove = (id) => {
        const {data} = this.state;
        const index = data.findIndex(d => d._id === id);
       
        if(index !== -1){                                

            fetch("/list/remove", {
                method: 'POST',
                headers: {'Content-Type':'application/json'},
                body: JSON.stringify({
                    "id": data[index]._id,
                    "status": data[index].attribute.state
                })
            })              
            .then(data => {      
                console.log(data);
                this.setState({
                    data: update(
                        this.state.data,{
                            $splice: [[index, 1]]
                        }
                        
                    )
                });
            });    
        }       
    }

    handleCancelConfirm = (id) => {
        console.log("handleCancelConfirmed");

        const {data} = this.state;
        const index = data.findIndex(d => d._id === id);
       
        if(index !== -1){                                

            fetch("/api/cancelconfirmed", {
                method: 'POST',
                headers: {'Content-Type':'application/json'},
                body: JSON.stringify({
                    "id": data[index]._id,
                    "status": data[index].attribute.state
                })
            })              
            .then(data => {      
                console.log(data);
                this.setState({
                    data: update(
                        this.state.data,{
                            $splice: [[index, 1]]
                        }
                        
                    )
                });
            });    
        }       
    }

    componentDidUpdate() {        
        
        console.log('componentDidUpdate');
    }

    componentWillUnmount() {
        //socket.off("status");
    }

    handleCreate = (data) => {
        console.log(data)
    }

    handleChange = (e) => {    
        
        this.setState({
            [e.target.name]: e.target.value
        });
        
        this.send();
    }

    render() {
        const{ data, keyword, foodname } = this.state;
        //console.log('>>> ', data);

        const {
            handleCancelConfirm
        } = this;


        return (       
            

            <Container fluid className="main-content-container px-4 pb-4">
                
                {/* Page Header */}
                <Row noGutters className="page-header py-4">
                <PageTitle sm="4" title="학습 데이터 확인" subtitle="Blog Posts" className="text-sm-left" />
                </Row>

                <Row>               
                {/* Sidebar Widgets */}
                <Col lg="3" md="12">                          
                    <SidebarCategories />
                </Col>
                </Row>

                {/* Default Light Table */}
                <Row>
                    <Col>
                    <Card small className="mb-4">
                        <CardHeader className="border-bottom">
                        <h6 className="m-0">Food DB Sources <div>{this.state.isLoaded}</div></h6>                            
                        </CardHeader>
                        <CardBody className="p-0 pb-3">
                        <NewFoodList data={data} onCancelConfirm={handleCancelConfirm}/>                        
                        </CardBody>
                    </Card>
                    </Col>
                </Row>
            </Container>
        )
    };
};

export default CheckFoodList;
