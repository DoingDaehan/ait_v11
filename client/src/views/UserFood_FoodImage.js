import React, {Component} from "react";
import { Container, Row, Col, Card, CardBody, Button } from "shards-react";

//import NewFoodInput from '../components/add-new-food/NewFoodInput';
import PageTitle from "../components/common/PageTitle";
import UserDataFoodList from "../components/user-food-imagefood/UserFoodDataList"
import axios from 'axios';
import update from 'react-addons-update';
import SidebarCategories from "../components/add-new-food/SidebarCategories";
import RangeDatePicker from "../components/common/RangeDatePicker";
import Moment from 'moment';
import Cookies from "universal-cookie";
const cookies = new Cookies();

class UserFood_FoodImage extends Component {

    constructor(props) {
        super(props);
        this.state = {                        
            data: [],            
            loginData: {},
            startDate: '',
            endDate: ''
        }
    }   

    input = null;
    
    handleStatus(data) {
        this.setState({
           data:data 
        });
    }

    handleRefresh = (type) => {
        axios({
            method: 'GET',
            url: '/api/exist/refresh',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': this.state.loginData.token
            },
            params: {
                type: type
            }        
        })        
        .then(data => {          
            console.log(data);
            //console.log(this.state.data);
            // this.setState({
            //     data:data.data
            // }, ()=>{
            //     console.log('!!', this.state.data);
            // })
            // this.setState({
            //     data:data
            // })
        });

    }

    handleInsertFood = () => {
        let keyword = this.state.keyword;
        //let foodname = this.state.foodname;
        let keywordList = this.state.keywordList;
        let data = null;

        console.log(this.state);
        // console.log(this.input);
        // console.log(this.input.props);
        
        // this.setState({
        //     keyword: 
        // })
        
        if(keywordList.length !== 0){
            data = {
                "keyword": keywordList
            }
        } else{
            data = {
                "keyword": keyword                
            }
        }
                
        this.setState({
            foodname: "",
            keyword: "",    
            keywordList: []
        }, () => {
            console.log(this.state);
        });
        
        axios({
            method: 'post',
            url: '/registeritem',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': this.state.loginData.token
            },
            data: data
        })
        .then(res => res.json())
        .then(data => {          
            console.log('BEFORE:::', this.state)
            this.setState({                
                data:data
            })
            console.log('AFTER:::', this.state)
        },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                }) 
            }
        );   
          
      
    }



    componentDidMount() {  

        let loginData = cookies.get('key');

        loginData = JSON.parse(atob(loginData));

        this.setState({loginData:loginData});


        axios({
            method: 'GET',
            url: '/api/v2/userdata/getlist/all',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': loginData.token
            }        
        })        
        .then(data => {                      
            console.log(data, data.data);
            if(data.data === null){
                this.setState({
                    data:[]
                }, () => {
                    console.log(this.state.data);
                });            
            } else {
                this.setState({
                    data:data.data
                }, () => {
                    console.log(this.state.data);
                });            
            }
            
        });
        
    }

    handleToggle = (id) => {
        const {data} = this.state;
        const index = data.findIndex(d => d._id === id);

        
        if(index !== -1){
        
            let tDone = !data[index].attribute.done;

            fetch("/list/askconfirm", {
                method: 'POST',
                headers: {
                    'Content-Type':'application/json',
                    'x-access-token': this.state.loginData.token
                },
                body: JSON.stringify({
                    "id": data[index]._id,
                    "done": tDone                
                })
            })   
            .then(res => res.json())    
            .then(data => {
                console.log(data);
                let newState = update(this.state, {
                    data: {
                        [index]: {
                            attribute: {$set: data.attribute}
                        }
                    } 
                });
            
                this.setState(newState);
            });    
        }
        
    }

    handleRemove = (id) => {
        const {data} = this.state;
        const index = data.findIndex(d => d._id === id);
       
        if(index !== -1){                                

            fetch("/api/list/remove", {
                method: 'POST',
                headers: {
                    'Content-Type':'application/json',
                    'x-access-token': this.state.loginData.token
                },
                body: JSON.stringify({
                    "id": data[index]._id,
                    "status": data[index].attribute.state
                })
            })              
            .then(data => {      
                console.log(data);
                this.setState({
                    data: update(
                        this.state.data,{
                            $splice: [[index, 1]]
                        }
                        
                    )
                });
            });    
        }       
    }

    handleCopyList = () => {
        const { data } = this.state;

        let foodList = data.filter((food) => {
            return food.attribute.done = true;
        });
        let list = foodList.map((food) => {
            return food._id;
        });

        //console.log(foodList, list);
        
        fetch("/api/confirm", {
            method: 'POST',
            headers: {
                'Content-Type':'application/json',
                'x-access-token': this.state.loginData.token
            },
            body: JSON.stringify({
                "idList": list        
            })
        })              
        .then(data => {      
            console.log(data);
            // this.setState({
            //     data: update(
            //         this.state.data,{
            //             $splice: [[index, 1]]
            //         }
                    
            //     )
            // });
        });    
    }

    
    handleFailStatus = (id) => {

        const {data} = this.state;
        const index = data.findIndex(d => d._id === id);

        axios({
            method: 'post',
            url: '/api/cancel',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': this.state.loginData.token
            },
            data: {
                id: id
            }
        })
        .then((res) => {            
            let newState = update(this.state, {
                    data: {
                        [index]: {
                            attribute: {
                                state: {$set: res.data.attribute.state}
                            }
                        }
                    } 
                });
            
            this.setState(newState, () => {
                console.log(this.state.data[index]);
            });
           
        });
        
    }

    componentDidUpdate() {        
        
        console.log('componentDidUpdate');
    }

    componentWillUnmount() {
        //socket.off("status");
    }

    handleCreate = (data) => {
        console.log(data)
    }

    handleChange = (e) => {    
        
        this.setState({            
            [e.target.name]: e.target.value
        }, () => {
            console.log(this.state.keyword);
        });
        
        //this.send();
    }

    handleDragDrop = (files) => {
        console.log(files[0]);
        this.setState({
            selectedFile: files[0].src.file,
            loaded: 0,
        },
        () => {
            const data = new FormData() 
            data.append('file', files[0].src.file);
    
            axios.post("/api/addcrawlingkeywords", data, {
                headers: {'x-access-token': this.state.loginData.token}
            },
            {
                onUploadProgress: ProgressEvent => {
                    this.setState({
                        loaded: (ProgressEvent.loaded / ProgressEvent.total*100),
                    })
                }
            })
            .then(res => { // then print response status
                console.log(res);
                this.setState({
                    selectedFile: res.data.filename.filename
                }, () => {
                    console.log(this.state);
                });
            })
        })
                
        
    }

    handleCrawlingStart = () => {
        axios.get("/api/crawlingstart", {
            params: {filename: this.state.selectedFile}
        },
        {
            headers: {'x-access-token': this.state.loginData.token}     
        })
        .then(res => { // then print response status
            console.log(res.statusText)
        })
    }

    onChangeHandler=event=>{
        this.setState({
          selectedFile: event.target.files[0],
          loaded: 0,
        })

        console.log(event.target.files[0]);
    }

    handleInsertUserDataList = (startDate, endDate) => {
        let s = Moment(startDate).format('YYYY-MM-DD');
        let e = Moment(endDate).format('YYYY-MM-DD');
        console.log("on insert handler", s, e);

        this.setState({
            startDate: s,
            endDate: e
        }, () =>{
            console.log(this.state.startDate, this.state.endDate);

            axios({
                method: 'GET',              
                url: '/api/v2/userdata/get/list/new/',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'x-access-token': this.state.loginData.token
                },
                params: {
                    startdate: this.state.startDate,
                    enddate: this.state.endDate,
                    type: "foodimage"
                }        
            })        
            .then(data => {                      
                
                this.setState({
                    data:data.data
                }, () => {
                    console.log(this.state.data);
                });            
            });
        });

    }

    handleClassifying = (id) => {
        console.log('handle classifying', id);

        axios({
            method: 'GET',
            url: '/api/v2/userdata/predict/',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': this.state.loginData.token
            },
            params: {
                id: id                
            }        
        })        
        .then(data => {                      
            
            this.setState({
                data:data.data
            }, () => {
                console.log(this.state.data);
            });            
        });
    }

    handleDownloadUserData = (id) => {
        console.log("handleDownloadUserData", id);

        axios({
            method: 'GET',
            url: '/api/v2/userdata/download/id',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': this.state.loginData.token
            },
            params: {
                id: id
            }        
        })        
        .then(data => {                      
            
            this.setState({
                data:data.data
            }, () => {
                console.log(this.state.data);
            });            
        });
    }

    onClickHandler = () => {
        const data = new FormData() 
        data.append('file', this.state.selectedFile)

        axios.post("/api/addcrawlingkeywords", data, {
            headers: {'x-access-token': this.state.loginData.token}
        })
        .then(res => { // then print response status
            console.log(res.statusText)
        })     
    }

    handleClipboard= (event) => {
        var clipboardData = window.clipboardData || event.clipboardData || event.originalEvent && event.originalEvent.clipboardData;

        var pastedText = clipboardData.getData("Text") || clipboardData.getData("text/plain");
        console.log(pastedText)
        if (!pastedText && pastedText.length) {
            return;
        }

       
        var rows = pastedText.replace(/"((?:[^"]*(?:\r\n|\n\r|\n|\r))+[^"]+)"/mg, function (match, p1) {
            // This function runs for each cell with multi lined text.
            return p1
                // Replace any double double-quotes with a single
                // double-quote
                .replace(/""/g, '"')
                // Replace all new lines with spaces.
                .replace(/\r\n|\n\r|\n|\r/g, ' ')
            ;
        })
        // Split each line into rows
        .split(/\r\n|\n\r|\n|\r/g);

        this.setState({
            keywordList:rows,            
        }, () => {
            console.log(this.state);
        });
    }

    render() {
        const{ data, keyword } = this.state;
 
        const {
            handleRefresh,
            handleChange,            
            handleToggle,
            handleRemove,     
            handleFailStatus,
            handleInsertUserDataList,
            handleClassifying,
            handleDownloadUserData
        } = this;

        return (       
            

            <Container fluid className="main-content-container px-4 pb-4">
                
                {/* Page Header */}
                <Row noGutters className="page-header py-4">
                <PageTitle sm="4" title="민감정보 필터링 FoodImage" subtitle="음식 추가" className="text-sm-left" />
                </Row>
               
                <Row>
                {/* Editor */}
                <Col lg="9" md="12">  
              
                    {/* <Button onClick={() => handleRefresh("append")}>기존 음식 가져오기</Button> */}
                    <RangeDatePicker handleInsert={handleInsertUserDataList}/>
                </Col>
                {/* Sidebar Widgets */}
                <Col lg="3" md="12">        
                    {/* <SidebarActions /> */}
                    <SidebarCategories />
                </Col>
                </Row>

                

                {/* Default Light Table */}
                <Row>
                    <Col>
                    <Card small className="mb-4">                      
                        <CardBody className="p-0 pb-3">
                        <UserDataFoodList                             
                            data={data} 
                            onToggle={handleToggle} 
                            onRemove={handleRemove}
                            onFailHandler={handleFailStatus}    
                            onClassifying={handleClassifying}
                            onDownloadingUserData={handleDownloadUserData}
                        />
                        </CardBody>
                    </Card>
                    </Col>
                </Row>
            </Container>
        )
    };
};

export default UserFood_FoodImage;
