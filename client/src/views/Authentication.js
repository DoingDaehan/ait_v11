import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import Login from '../components/user-profile/Login';
import Register from '../components/user-profile/Register';

import { connect } from 'react-redux';
import { bindActionCreators} from 'redux';
import * as authActions from '../modules/auth';
import '../App.css';


class Authentication extends Component {

    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: ""
        };


    }

    static propTypes = {
        mode: PropTypes.bool,
        onLogin: PropTypes.func,
        onRegister: PropTypes.func
    };
    
    static defaultProps = {
        mode: true,
        onLogin: (id, pw) => { console.error("login function not defined"); },
        onRegister: (id, pw) => { console.error("register function not defined"); }
    };

    
    handleChange = (e) => {
                
        // this.setState({
        //     [e.target.name]: e.target.value
        // });

        // console.log(this.state);        

        const { value } = e.target;
        const { AuthActions } = this.props;
        console.log(AuthActions, value);
        AuthActions.setInput(value);
    }

    handleLogin = () => {
        let id = this.state.username;
        let pw = this.state.password;
        
        console.log("handleLogin");
        this.props.onLogin(id, pw).then(
            (success) => {
                if(!success) {
                    this.setState({
                        password: ''
                    });
                }
            }
        );
    }


    render(){        
        // const loginView = (          
        //     <Login/>
        // );

        // const registerView = (           
        //     <Register/>
        // );
        
        return (
            <div className="container auth">
                <Link className="logo" to="/">AIT</Link>
                <div className="card">
                    <div className="header blue white-text center">
                        <div className="card-content">{this.props.mode ? "LOGIN" : "REGISTER"}</div>
                    </div>
                    {this.props.mode ? <Login onLogin={this.handleLogin} onChange={this.handleChange} email={this.state.email} password={this.state.password}/> : <Register onChange={this.handleChange}/> }
                </div>
            </div>
        );
    }
}

//export default Authentication;

export default connect(
    (state) => ({
        value: state.input.get('value')
    }),
    (dispatch) => ({
        AuthActions: bindActionCreators(authActions, dispatch)        
    })
)(Authentication);