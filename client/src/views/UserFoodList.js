/* eslint jsx-a11y/anchor-is-valid: 0 */
import update from 'react-addons-update';
import React from "react";
import {
  Container,
  Row,
  Col,  
  Button
} from "shards-react";
import { Link } from "react-router-dom";
import axios from 'axios';
import PropTypes from 'prop-types';
import FoodImage from '../components/user-food/UserFoodImage';
import SmallUserFoodImage from '../components/user-food/SmallUserFoodImage';
import PageTitle from "../components/common/PageTitle";
import ProcessStatusManager from "../utils/ProcessStatusManager";
import ProgressBar from 'react-bootstrap/ProgressBar';
import ButtonHotKeyNext from "../components/foodlist/ButtonWithHotKeyNext";
import ButtonHotKeyPrev from "../components/foodlist/ButtonWithHotKeyPrev";
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
import Cookies from 'universal-cookie';
import $ from 'jquery';
const job = ProcessStatusManager;
const cookies = new Cookies();

window.$ = $;

class UserFoodList extends React.Component {
//const FoodList = ( {history} ) => {
  constructor(props) {
    super(props);

    this.state = {
      // First list of posts.
      foodId: "",
      PostsListOne: [
        
      ],
      status: "",
      currentIdx: 0,
      imageStat: {
        good: 0,
        bad: 0,
        none: 0,
        total: 0
      },
      type: '',
      loginData: {},
      loadingState: false
    };    
  }

  static propTypes = {
    /**
     * The small stats dataset.
     */
    smallStats: PropTypes.array
  };
  
  static defaultProps = {
    smallStats: [
      {
        label: "Good",
        value: "2,390",
        percentage: "4.7%",
        increase: true,
        chartLabels: [null, null, null, null, null, null, null],
        attrs: { md: "6", sm: "6" },
        datasets: [
          {
            label: "Today",
            fill: "start",
            borderWidth: 1.5,
            backgroundColor: "rgba(0, 184, 216, 0.1)",
            borderColor: "rgb(0, 184, 216)",
            data: [1, 2, 1, 3, 5, 4, 7]
          }
        ]
      },
      {
        label: "Bad",
        value: "182",
        percentage: "12.4",
        increase: true,
        chartLabels: [null, null, null, null, null, null, null],
        attrs: { md: "6", sm: "6" },
        datasets: [
          {
            label: "Today",
            fill: "start",
            borderWidth: 1.5,
            backgroundColor: "rgba(23,198,113,0.1)",
            borderColor: "rgb(23,198,113)",
            data: [1, 2, 3, 3, 3, 4, 4]
          }
        ]
      },
      {
        label: "None",
        value: "8,147",
        percentage: "3.8%",
        increase: false,
        decrease: true,
        chartLabels: [null, null, null, null, null, null, null],
        attrs: { md: "4", sm: "6" },
        datasets: [
          {
            label: "Today",
            fill: "start",
            borderWidth: 1.5,
            backgroundColor: "rgba(255,180,0,0.1)",
            borderColor: "rgb(255,180,0)",
            data: [2, 3, 3, 3, 4, 3, 3]
          }
        ]
      }
    
    ]
  };
  
  goBack = () => {
    this.props.history.goBack();
  }

  objToQueryString = (obj) => {
    const keyValuePairs = [];
    for (const key in obj) {
      keyValuePairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]));
    }
    return keyValuePairs.join('&');
  }

  componentDidUpdate(prevProps) {
    this.unlisten = this.props.history.listen((location, action) => {
      console.log("on route change");
    });
  }

  parseData = (mState, data, callback) => {
    let id = 0;
    let objList = [];
    
    mState = Number(mState);

    console.log(mState);
    
    if(mState === job.CLASSIFYING_DONE){

      data.map((v) => {
        let obj = {
          id: id++,
          imageId: v._id,
          backgroundImage: v.filename,
          s1class: v.s1class,
          s2class: v.s2class,
          type: v.type,
          isSensitive: v.isSensitive
        }
        console.log('>>>>>', obj);

        objList.push(obj);
      });
    
      // if(data.goodImages !== undefined && data.goodImages !== null){
      //   data.goodImages.map((v) => {

      //     let obj = {
      //       id: id++,
      //       imageId: v._id,
      //       backgroundImage: v.filename,
      //       s1class: v.s1class,
      //       s2class: v.s2class
      //     }
  
      //     objList.push(obj);
      //   });
      // }

      // if(data.badImages !== undefined && data.badImages !== null){
      //   data.badImages.map((v) => {

      //     let obj = {
      //       id: id++,
      //       imageId: v._id,
      //       backgroundImage: v.filename,
      //       s1class: v.s1class,
      //       s2class: v.s2class
      //     }
  
      //     objList.push(obj);
      //   });
      // }
    } 
    else {
      console.log(data);
      // if(data.images !== undefined && data.images !== null){
      //   data.images.map((v) => {

      //     let obj = {
      //       id: id++,
      //       imageId: v._id,
      //       backgroundImage: v.filename,
      //       s1class: v.s1class,
      //       s2class: v.s2class
      //     }
  
      //     objList.push(obj);
      //   });
      // }
      
    }

    callback(objList);
  }

  componentDidMount() {
    // this.setState({
    //   status: this.props.location.query.status
    // })
    //console.log('>>>>>>> location: ', this.props.location.query.status)   
    //console.log('>>>>>>> match: ', this.props.match)  
    
    $(window).scroll(() => {
      // WHEN HEIGHT UNDER SCROLLBOTTOM IS LESS THEN 250
      if ($(document).height() - $(window).height() - $(window).scrollTop() < 3) {
          if(!this.state.loadingState){
              //console.log("LOAD NOW", this.state.PostsListOne[this.state.PostsListOne.length - 1].imageId);

              if(this.state.PostsListOne !== undefined && this.state.PostsListOne.length !== 0){
                console.log("LOAD NOW", this.state.PostsListOne[this.state.PostsListOne.length - 1].imageId);

                axios.get(
                  '/api/v2/userdata/get/list/',
                  {   
                    params: {
                      id: this.props.match.params.id,
                      imageId: this.state.PostsListOne[this.state.PostsListOne.length - 1].imageId,                    
                      type: mType
                    }                 
                  },
                  {
                    headers: {
                      'x-access-token': loginData.token          
                    }
                  }      
                  )
                  .then(data => data.data)
                  .then(data => { // then print response status

                    
                    this.parseData(mState, data, (objList) => {
                          
                      if(data !== undefined && data !== null) {
                        this.setState({
                          foodId: this.props.match.params.id,
                          PostsListOne: this.state.PostsListOne.concat(objList),
                          status: mState          
                        }, () => {
                          console.log(">",this.state)
                        });
                      }
                    });      
                });
                this.setState({
                    loadingState: true
                });
              }
              
              
          }
      } else {
          if(this.state.loadingState){
              this.setState({
                  loadingState: false
              });
          }
      }
    });

    let loginData = cookies.get('key');
    console.log(loginData);        
    loginData = JSON.parse(atob(loginData));
    console.log(loginData);

    this.setState({loginData:loginData});
    
    console.log("componentDidMount::::", this.props.location.query);
    if(this.props.location.query === undefined){
        //this.goBack();
        this.props.history.push('/newfood');
        return;
    }

    let mState = this.props.location.query.status;
    let mType = this.props.location.query.type;
    const queryString = this.objToQueryString({
      state: mState,
      type: mType
    });
    
    console.log(this.props.match.params.id, mState, mType);
    
    axios.get(
      //'/api/v2/userdata/getlist/' + mType,
      '/api/v2/userdata/get/list/',
      {   
        params: {
          id: this.props.match.params.id,
          state: mState,
          type: mType
        }                 
      },
      {
        headers: {
          'x-access-token': loginData.token          
        }
      }      
      )
      .then(data => data.data)
      .then(data => { // then print response status
        console.log(data);
        
        this.parseData(mState, data, (objList) => {
          if(data !== undefined && data !== null) {
            this.setState({
              foodId: this.props.match.params.id,
              PostsListOne: this.state.PostsListOne.concat(objList),
              status: mState              
            }, () => {
              console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",this.state)
            });
          }
        });      
    });
   
  }

  handleRefreshImage = (idx, dest) => {
    console.log("handleRefreshImage", dest);
    this.setState({
      PostsListOne: update(
        this.state.PostsListOne,
        {
          [idx]:{
            backgroundImage: {$set: ""}
          }
        }
      )      
    })

    console.log(this.state, idx);
    this.setState({
      PostsListOne: update(
        this.state.PostsListOne,
        {
          [idx]:{
            backgroundImage: {$set: dest}
          }
        }
      )      
    })

    // this.setState({
    //     state: this.state
    //   })

    

  }

  handleClick = (value, filename, selectedIdx) => {    
    
    var url = "/api/v2/userdata/update/image/sensitive"
    let v = String(value);
 
    let prevS1class = this.state.PostsListOne[selectedIdx].isSensitive;
       
    let isSensitive = 0;
    
    if(v === "blind"){
      isSensitive = 1;      
    } else if(v === "none"){
      isSensitive = 0;      
    }

    this.setState({
      PostsListOne: update(
        this.state.PostsListOne,
        {
          [selectedIdx]:{
            isSensitive: {$set: isSensitive}
          }
        }
      ),
      currentIdx: selectedIdx      
    })

    console.log(this.state);

    axios.get(url, {
      params: {
        "id": this.state.foodId,
        "imageId": this.state.PostsListOne[selectedIdx].imageId,
        "type": this.state.PostsListOne[selectedIdx].type,
        "isSensitive": isSensitive,
        "file": filename,
        "status": this.state.status,
        "prevStatus": prevS1class
      }
    }, 
    {
      header: {
          'x-access-token': this.state.loginData.token
      }
    })
    .then(res => { // then print response status
      console.log(res)
      // this.setState({      
      //   PostsListOne: update(
      //     this.state.PostsListOne,
      //     {
      //       [this.state.currentIdx]:{
      //         backgroundImage: {$set: res.filename}
      //       }
      //     }
      //   )
      // }, () => {
      //   console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",this.state)
      // })
      
    });

    // fetch(url, {
    //   method: 'POST',
    //   headers: {
    //           'Content-Type':'application/json',
    //           'x-access-token': this.state.loginData.token
    //   },
    //   body: JSON.stringify({
    //     "id": this.state.foodId,
    //     "file": filename,
    //     "status": this.state.status,
    //     "prevStatus": prevS1class
    //   })
    // })
    // .then(res => res.json())
    // .then(data => {      
    
    //   this.setState({        
    //     // imageStat: {
    //     //   good: data.goodImages.length,
    //     //   bad: data.badImages.length,
    //     //   none: (data.images.length - data.goodImages.length - data.badImages.length) < 0 ? 0 : (data.images.length - data.goodImages.length - data.badImages.length),
    //     //   total: ((data.images.length - data.goodImages.length - data.badImages.length)) < 0 ? data.goodImages.length + data.badImages.length : data.images.length
    //     // },
    //     PostsListOne: update(
    //       this.state.PostsListOne,
    //       {
    //         [this.state.currentIdx]:{
    //           backgroundImage: {$set: data.filename}
    //         }
    //       }
    //     )
    //   }, () => {
    //     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",this.state)
    //   })
    // });
    
  }

  handleClickTraining = () => {
    console.log("Handle Clicking Training!!!");
    //fetch("/starttrainig", {
    fetch("/api/v2/starttraining", {
      method: 'POST',
      headers: {
        'Content-Type':'application/json',
        'x-access-token': this.state.loginData.token
      },
      body: JSON.stringify({
        "trainid": this.state.foodId        
      })
    })
    .then(res => res.json())
    .then(data => {      
      console.log(data);
    },
    (error) => {
          console.log(error)
        }
    );    
  }

  handleClickCropping = () => {
    
    fetch("/startcropping", {
      method: 'POST',
      headers: {
        'Content-Type':'application/json',
        'x-access-token': this.state.loginData.token
      },
      body: JSON.stringify({
        "trainid": this.state.foodId        
      })
    })
    .then(res => res.json())
    .then(data => {      
      console.log(data);
    },
    (error) => {
          console.log(error)
        }
    );    
  }

  handleRemove = (e) => {
    let filename = e.target.attributes.getNamedItem('filename').value;
    let t = filename.split('/');
    filename = t[t.length - 1];
    
    console.log(this.state.foodId, 'filename::: ', filename)
    
  

      fetch("/api/removefile", {
          method: 'POST',
          headers: {
            'Content-Type':'application/json',
            'x-access-token': this.state.loginData.token
          },
          body: JSON.stringify({
              "id": this.state.foodId,
              "path": filename
          })
      })              
      .then(data => {      
          console.log(data);
          // this.setState({
          //     data: update(
          //         this.state.data,{
          //             $splice: [[index, 1]]
          //         }
                  
          //     )
          // });
      });    
    
  }

  LargeFoodImage =  (props)=> {
    const { PostsListOne, status, currentIdx, onLoadImage } = props;
    const { type } = this.state;
    let largeFoodImage = null;
    //console.log("large food iamge :::", PostsListOne.find(p=> p.id === currentIdx));
    if(PostsListOne.length === 0) {
      return <div></div>;
    } else {
      largeFoodImage = PostsListOne.find(p => p.id === currentIdx);
      if(largeFoodImage){
        return <FoodImage onLoadImage={onLoadImage} post={largeFoodImage} currentIdx={currentIdx} onClick={this.handleClick} status={status} type={type}/>
      } else{
        return <div></div>
      } 
    }  
  }

  handleClickNext = () => {
    
    this.setState({
      currentIdx: this.state.currentIdx + 1
    });
  }

  handleClickPrev = () => {
    
    if(this.state.currentIdx !== 0){
      this.setState({
        currentIdx: this.state.currentIdx - 1
      });
    }
    
  }

  handleClickStatus = (e) => {
    console.log("handleClickStatus::", e.target.value);

    axios.post("/data/all", {
      "id":this.state.foodId,
      "s1class":e.target.value,
      "status": this.state.status
    }, 
    {
      headers: {'x-access-token': this.state.loginData.token}
    })
    .then(res => { // then print response status
        console.log(res)
    });

    this.props.history.push('/newfood');

  }

  handleComplete = () => {
    const { foodId, status, type } = this.state;

    // let foodList = data.filter((food) => {
    //     return food.attribute.done == true;
    // });
    let list = [];
    list.push(foodId);

    //console.log(foodList, list);
    
    //fetch("/api/confirm", {
    fetch("/api/v2/userdata/sensitive/confirm", {
        method: 'POST',
        headers: {
          'Content-Type':'application/json',
          'x-access-token': this.state.loginData.token
        },
        body: JSON.stringify({
            "idList": list,
            "status": status,
            "type": type
        })
    })              
    .then(data => {      
        console.log(data);
        // this.setState({
        //     data: update(
        //         this.state.data,{
        //             $splice: [[index, 1]]
        //         }
                
        //     )
        // });
    });    

    if(type === "append"){
      this.props.history.push('/existfood');
    } else {
      this.props.history.push('/newfood');
    }
    
  }

  

  

  submit = () => {
    const options = {
      title: '완료',
      message: '이미지 분류가 끝났습니까?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => this.handleComplete()
        },
        {
          label: 'No',
          // onClick: () => alert('Click No')
        }
      ],
      childrenElement: () => <div />,
      customUI: ({ title, message, onClose }) => 
          <div className='custom-ui'>
          <h1>민감정보가 포함된 이미지 분류가 끝났습니까?</h1>
          <p>완료하면 이 음식 이미지들은 이동 됩니다.</p>
          <Row>
            <Col>
            <Button onClick={onClose}>아니오</Button>
            </Col>
            <Col>
            <Button onClick={() => {
                this.handleComplete()
                onClose()
            }}>네</Button>
            </Col>
          </Row>
    </div>,
      willUnmount: () => {}
    }

    confirmAlert(options)
  };

  render() {
    
    const { PostsListOne, status, currentIdx } = this.state;    
    const {good, bad, none} = this.state.imageStat;
    //let query = null;

    // if(this.props.location.query !== undefined){
    //   query = queryString.parse(this.location.status)
    // }
    //const { status } = this.props.location.query;
    // console.log("render::::", this.props.location.query);
    // if(status != undefined){
    //   console.log("!!!!!!!!!!!!!!!!!", status);
    // } else{
    //   status = 0;
    // }
    //let status = 0;
    
    if(this.props.location.query !== undefined){
      return (
        <Container fluid className="main-content-container px-4">
          {/* Page Header */}       
          <Row noGutters className="page-header py-4">
            <PageTitle title="음식 이미지 분류" subtitle="음식 분류" className="text-sm-left mb-3" />
          </Row>
          <Row>
            {/* <PageTitle sm="4" title="Blog Posts" subtitle="Components" className="text-sm-left" /> */}
            <Col>
            <Button className="mb-2 mr-1">
              <Link className="text-white text-center rounded p-3 " to='/newfood/' onClick={() => this.handleClickTraining()}>학습 시작</Link>
            </Button>
            <Button className="mb-2 mr-1">            
                <Link className="text-white text-center rounded p-3 " to='/newfood/' onClick={() => this.handleClickCropping()}>사진 자르기</Link>
            </Button>
            </Col>          
            <Col>

                <Button className="bg-success text-white text-center rounded p-3" onClick={this.submit}>완료</Button>  
                {/* <Link className="bg-success text-white text-center rounded p-3 " to='/newfood/' onClick={() => this.handleComplete()}>완료</Link> */}
                
            </Col>
            
          </Row>
          
          <Row>
            <Col lg="12" md="12" sm="12">
            <ProgressBar>
              <ProgressBar striped variant="success" now={good} key={1} label={`${good}`}/>
              <ProgressBar variant="danger" now={bad} key={2} label={`${bad}`}/>
              <ProgressBar striped variant="info" now={none} key={3} label={`${none}`}/>          </ProgressBar>
            </Col>
          </Row>
  
          {/* First Row of Posts */}
          <Row>
              <Col lg="12" md="12" sm="12" className="mb-4" key={currentIdx}>
                <this.LargeFoodImage onLoadImage={this.handleRefreshImage} PostsListOne={PostsListOne} status={status} currentIdx={currentIdx} />
                <ButtonHotKeyPrev onClick={this.handleClickPrev} value={"이전"} />
                <ButtonHotKeyNext onClick={this.handleClickNext} value={"다음"} />
              </Col>
              {/* <Col lg="3" md="3" sm="3" className="mb-4">
              {
                
                  <UsersByDevice data={imageStat} />
                
              }
                
            </Col> */}
          </Row>
          {/* <Row>
            <Button onClick={(e) => this.handleClickStatus(e)} value={"good"} >All Good</Button>
            <Button onClick={(e) => this.handleClickStatus(e)} value={"bad"} > All Bad</Button>
            <Button onClick={(e) => this.handleClickStatus(e)} value={"none"} > Clear</Button>

          </Row> */}
          <Row>
            {
              PostsListOne.map(
                (post, idx) => (            
                  <Col lg="3" md="6" sm="12" className="mb-4" key={idx}>
                    <SmallUserFoodImage currentIdx={idx} post={post} onClick={this.handleClick}/>              
                  </Col>
                )
              )
            }
          </Row>          
        </Container>
      );
    } else{
      return (
        <div></div>
      );
    }
    
    
  }
}

export default UserFoodList;
