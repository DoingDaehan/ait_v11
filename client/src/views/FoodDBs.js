import React, { Component } from "react";
import { Container, Row, Col, Card, CardHeader, CardBody } from "shards-react";

import PageTitle from "../components/common/PageTitle";
import FoodSourceList from "../components/food-db/FoodSourceList";

class FoodDBs extends Component {

    constructor(props) {
      super(props);
      this.state = {
        error: null,
        isLoaded: false,
        foodDBs: []
      }
    }

    componentDidMount() {        
        fetch('/api/foodcheck')
            .then(res => res.json())
            .then(data => {          
              this.setState({foodDBs:data})
              console.log(this.state.foodDBs)
            },
            (error) => {
              this.setState({
                isLoaded: true,
                error
              }) 
            }
            );            
    }

    render(){
      const{ foodDBs } = this.state;
      return (
        <Container fluid className="main-content-container px-4">
      {/* Page Header */}
      <Row noGutters className="page-header py-4">
        <PageTitle sm="4" title="Add New Post" subtitle="Blog Posts" className="text-sm-left" />
      </Row>

      {/* Default Light Table */}
      <Row>
        <Col>
          <Card small className="mb-4">
            <CardHeader className="border-bottom">
              <h6 className="m-0">Food DB Sources </h6>                            
            </CardHeader>
            <CardBody className="p-0 pb-3">                            
              <FoodSourceList data={foodDBs.sources}/>               
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
    );
    }
    
}

export default FoodDBs;
