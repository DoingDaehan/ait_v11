import React, {Component} from "react";
import { Container, Row, Col, Card, CardBody, Form,  Button } from "shards-react";

import NewFoodInput from '../components/add-new-food/NewFoodInput';
import PageTitle from "../components/common/PageTitle";
import NewFoodList from "../components/add-new-food/NewFoodList"
import axios from 'axios';
import update from 'react-addons-update';
import SidebarCategories from "../components/add-new-food/SidebarCategories";
import Files from "react-butterfiles";
import Cookies from "universal-cookie";
// import ProcessStatusManager from "../utils/ProcessStatusManager";
// const job = ProcessStatusManager.JobEnum;


// var PORT;
// if (process.env.NODE_ENV === "production") {    
//   console.log("Production Mode ::: ");

//   if(process.env.NODE_ENV_PRODUCTION == "production1"){
//     PORT = process.env.PORT || 5002;  
//   } else if(process.env.NODE_ENV_PRODUCTION == "production2") {
//     PORT = process.env.PORT || 5000;
//   }
  
// } else if (process.env.NODE_ENV == 'development') {
//   PORT = process.env.PORT || 4200;
// }

// var socket = null;

//const socket = socketIOClient(endpoint);

const cookies = new Cookies();
class AddUserFood extends Component {

    constructor(props) {
        super(props);
        this.state = {
            //response: false,
            endpoint: "http://192.168.0.23:",
            //color: 'white',
            keyword: '',
            foodname: '',                            
            keywordList: [],
            data: [],            
            selectedFile: null,
            loaded: 0,
            loginData: {}
        }

        //socket = socketIOClient.connect(this.state.endpoint + PORT + '/users');
    }   

    input = null;
        
    // sending sockets
    send = () => {
        //const socket = socketIOClient(this.state.endpoint);
        //socket.emit("reqMsg", {comment: this.state.keyword});
    }

    handleStatus(data) {
        this.setState({
           data:data 
        });
    }

    handleInsertFood = () => {
        let keyword = this.state.keyword;
        //let foodname = this.state.foodname;
        let keywordList = this.state.keywordList;
        let data = null;

        console.log(this.state);
        
        if(keywordList.length !== 0){
            data = {
                "keyword": keywordList
            }
        } else{
            data = {
                "keyword": keyword                
            }
        }
                
        this.setState({
            foodname: "",
            keyword: "",    
            keywordList: []
        }, () => {
            console.log(this.state);
        });
        
        axios({
            method: 'post',
            url: '/api/registeritem',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': this.state.loginData.token
            },
            data: data
        })
        .then(res => res.json())
        .then(data => {          
            console.log('BEFORE:::', this.state)
            this.setState({                
                data:data
            })
            console.log('AFTER:::', this.state)
        },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                }) 
            }
        );   
          
      
    }


    componentDidMount() {  
      
        let loginData = cookies.get('key');
        console.log(loginData);        
        loginData = JSON.parse(atob(loginData));
        console.log(loginData);

        this.setState({loginData:loginData});

        if(loginData.isLoggedIn !== true){
            this.props.history.push('/login')
        }

        let mHeaders = new Headers();
        mHeaders.append('x-access-token', loginData.token);
        let options = {
            method: 'GET',
            headers: mHeaders
        }


        fetch('/api/datas', options)
        .then(res => res.json())
        .then(data => {
            console.log(data);
          this.setState({data:data})
          //console.log(data)
          //console.log(this.state.data)
        },
        (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            }
        );     
        
     

    }

    handleToggle = (id) => {
        const {data} = this.state;
        const index = data.findIndex(d => d._id === id);

        
        if(index !== -1){
        
            let tDone = !data[index].attribute.done;

            fetch("/list/askconfirm", {
                method: 'POST',
                headers: {
                    'Content-Type':'application/json',
                    'x-access-token': this.state.loginData.token
                },
                body: JSON.stringify({
                    "id": data[index]._id,
                    "done": tDone                
                })
            })   
            .then(res => res.json())    
            .then(data => {
                console.log(data);
                let newState = update(this.state, {
                    data: {
                        [index]: {
                            attribute: {$set: data.attribute}
                        }
                    } 
                });
            
                this.setState(newState);
            });    
        }
        
    }

    handleRemove = (id) => {
        const {data} = this.state;
        const index = data.findIndex(d => d._id === id);
       
        if(index !== -1){                                

            fetch("/api/list/remove", {
                method: 'POST',
                headers: {
                    'Content-Type':'application/json',
                    'x-access-token': this.state.loginData.token
                },
                body: JSON.stringify({
                    "id": data[index]._id,
                    "status": data[index].attribute.state
                })
            })              
            .then(data => {      
                console.log(data);
                this.setState({
                    data: update(
                        this.state.data,{
                            $splice: [[index, 1]]
                        }
                        
                    )
                });
            });    
        }       
    }

    handleCopyList = () => {
        const { data } = this.state;

        let foodList = data.filter((food) => {
            return food.attribute.done = true;
        });
        let list = foodList.map((food) => {
            return food._id;
        });

        //console.log(foodList, list);
        
        fetch("/api/confirm", {
            method: 'POST',
            headers: {
                'Content-Type':'application/json',
                'x-access-token': this.state.loginData.token
            },
            body: JSON.stringify({
                "idList": list        
            })
        })              
        .then(data => {      
            console.log(data);
            // this.setState({
            //     data: update(
            //         this.state.data,{
            //             $splice: [[index, 1]]
            //         }
                    
            //     )
            // });
        });    
    }

    
    handleFailStatus = (id) => {

        const {data} = this.state;
        const index = data.findIndex(d => d._id === id);

        axios({
            method: 'post',
            url: '/api/cancel',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': this.state.loginData.token
            },
            data: {
                id: id
            }
        })
        .then((res) => {            
            let newState = update(this.state, {
                    data: {
                        [index]: {
                            attribute: {
                                state: {$set: res.data.attribute.state}
                            }
                        }
                    } 
                });
            
            this.setState(newState, () => {
                console.log(this.state.data[index]);
            });
           
        });
        
    }

    componentDidUpdate() {        
        
        //console.log('componentDidUpdate');
    }

    componentWillUnmount() {
        //socket.off("status");
    }

    handleCreate = (data) => {
        console.log(data)
    }

    handleChange = (e) => {    
        
        this.setState({            
            [e.target.name]: e.target.value
        }, () => {
            console.log(this.state.keyword);
        });
        
        //this.send();
    }

    // readTextFile = (file) => {         
    //     var reader = new FileReader();
    //     console.log(file);
    //     // reader.onload = function () {
    //     //     console.log(reader.result);
    //     // };
    
    //     // reader.readAsText(file, /* optional */ "euc-kr");
    // }


    handleDragDrop = (files) => {
        console.log(files[0]);
        this.setState({
            selectedFile: files[0].src.file,
            loaded: 0,
        },
        () => {
            const data = new FormData() 
            data.append('file', files[0].src.file);
    
            axios.post("/api/addcrawlingkeywords", data, {
                onUploadProgress: ProgressEvent => {
                    this.setState({
                        loaded: (ProgressEvent.loaded / ProgressEvent.total*100),
                    })
                }
            })
            .then(res => { // then print response status
                console.log(res);
                this.setState({
                    selectedFile: res.data.filename.filename
                }, () => {
                    console.log(this.state);
                });
            })
        })
                
        
    }

    handleCrawlingStart = () => {
        axios.get("/api/crawlingstart", {
            params: {filename: this.state.selectedFile}
        },
        {
            header: {
                'x-access-token': this.state.loginData.token
            }
        }
        )
        .then(res => { // then print response status
            console.log(res.statusText)
        })
    }

    onChangeHandler=event=>{
        this.setState({
          selectedFile: event.target.files[0],
          loaded: 0,
        })

        console.log(event.target.files[0]);
    }

    onClickHandler = () => {
        const data = new FormData() 
        data.append('file', this.state.selectedFile)

        axios.post("/api/addcrawlingkeywords", data, { // receive two parameter endpoint url ,form data 
        })
        .then(res => { // then print response status
            console.log(res.statusText)
        })     
    }

    handleClipboard= (event) => {
        var clipboardData = window.clipboardData || event.clipboardData || event.originalEvent && event.originalEvent.clipboardData;

        var pastedText = clipboardData.getData("Text") || clipboardData.getData("text/plain");
        console.log(pastedText)
        if (!pastedText && pastedText.length) {
            return;
        }

        // Parse the pasted text from Excel into rows.
        // Pasted text is usually separated by a new line for each row,
        // but a single cell can contain multiple lines, which is what
        // we pars out in the first `replace`.
        //
        // We find all text within double-quotes ('"') which has new
        // lines, and put the text within the quotes into capture
        // groups. For each match, we replace its contents again, by
        // removing the new lines with spaces.
        //
        // Then lastly, once we've joined all the multi line cells, we
        // split the entire pasted content on new lines, which gives
        // us an array of each row.
        //
        // Since Windows usually uses weird line-endings, we need to
        // ensure we check for each of the different possible
        // line-endings in every regexp.
        //
        // It also handles cells which contains quotes. There appears
        // to be two ways this is handled. In Google Docs, quotes within
        // cells are always doubled up when pasting, so " becomes "".
        // In Libre Office, the quotes are not normal quotes, some
        // other character is used, so we don't need to handle it any
        // differently.
        var rows = pastedText.replace(/"((?:[^"]*(?:\r\n|\n\r|\n|\r))+[^"]+)"/mg, function (match, p1) {
            // This function runs for each cell with multi lined text.
            return p1
                // Replace any double double-quotes with a single
                // double-quote
                .replace(/""/g, '"')
                // Replace all new lines with spaces.
                .replace(/\r\n|\n\r|\n|\r/g, ' ')
            ;
        })
        // Split each line into rows
        .split(/\r\n|\n\r|\n|\r/g);

        this.setState({
            keywordList:rows,            
        }, () => {
            console.log(this.state);
        });
    }

    render() {
        const{ data, keyword } = this.state;
 
        const {
            handleInsertFood,
            handleChange,            
            handleToggle,
            handleRemove,     
            handleFailStatus,       
        } = this;

        return (       
            

            <Container fluid className="main-content-container px-4 pb-4">
                
                {/* Page Header */}
                <Row noGutters className="page-header py-4">
                <PageTitle sm="4" title="새 음식 추가" subtitle="음식 추가" className="text-sm-left" />
                </Row>
               
                <Row>
                {/* Editor */}
                <Col lg="9" md="12">  

                <Files
                        multiple={true} 
                        maxSize="2mb"
                        multipleMaxSize="10mb"
                        multipleMaxCount={3}
                        // accept={["application/pdf","image/jpg","image/jpeg"]}
                        accept={".txt"}
                        onSuccess={(files) => { this.handleDragDrop(files)}}
                        onError={errors => this.setState({ errors })}
                    >
                        {({ browseFiles, getDropZoneProps }) => (
                            <div>
                                <label>아래에 크롤링 리스트 파일을 Drag & Drop 하세요</label>
                                <div
                                    {...getDropZoneProps({})}
                                >
                    
                                    <Card small className="mb-3">
                                        <CardBody>                                        
                                            <Form className="add-new-food"
                                                  onPaste={this.handleClipboard}
                                                  > 
                                                <NewFoodInput 
                                                    name="keyword"                             
                                                    onChange={handleChange} 
                                                    cPlaceholder="음식 이미지 검색어"                                                    
                                                    value={keyword}                                           
                                                    />                                                
                                           
                                                <Button onClick={handleInsertFood}>음식 추가 : 크롤링 시작</Button>
                                              
                                            </Form>
                                    
                                        </CardBody>
                                    </Card>                                  
                                </div>
                                
                            </div>
                        )}
                    </Files>
                </Col>
                {/* Sidebar Widgets */}
                <Col lg="3" md="12">        
                    {/* <SidebarActions /> */}
                    <SidebarCategories />
                </Col>
                </Row>

                

                {/* Default Light Table */}
                <Row>
                    <Col>
                    <Card small className="mb-4">                      
                        <CardBody className="p-0 pb-3">
                        <NewFoodList                             
                            data={data} 
                            onToggle={handleToggle} 
                            onRemove={handleRemove}
                            onFailHandler={handleFailStatus}    
                        />
                        </CardBody>
                    </Card>
                    </Col>
                </Row>
            </Container>
        )
    };
};

export default AddUserFood;
