import {
    AUTH_LOGIN,
    AUTH_LOGIN_SUCCESS,
    AUTH_LOGIN_FAILURE,
    AUTH_REGISTER,
    AUTH_REGISTER_SUCCESS,
    AUTH_REGISTER_FAILURE,
    AUTH_GET_STATUS,
    AUTH_GET_STATUS_SUCCESS,
    AUTH_GET_STATUS_FAILURE,
    AUTH_LOGOUT
} from './ActionTypes';

import axios from 'axios';

/*============================================================================
    authentication
==============================================================================*/

/* LOGIN */
export function loginRequest(username, password) {
    return (dispatch) => {
        // Inform Login API is starting
        dispatch(login());

        // API REQUEST
        return axios.post('/api/v2/auth/login', { username, password })
        .then((response) => {
            // SUCCEED
            if(response.data.success === false){
                throw new Error("Login Fail");
            } else{
                dispatch(loginSuccess(username, response.data.data));
            }
            
        }).catch((error) => {
            // FAILED
            console.log(error);
            dispatch(loginFailure());
        });
    };
}

export function login() {
    return {
        type: AUTH_LOGIN
    };
}

export function loginSuccess(username, token) {
    return {
        type: AUTH_LOGIN_SUCCESS,
        username,
        token
    };
}

export function loginFailure() {
    return {
        type: AUTH_LOGIN_FAILURE
    };
}


/* REGISTER */
export function registerRequest(username, password) {
    return (dispatch) => {
        // To be implemented..
    };
}

export function register() {
    return {
        type: AUTH_REGISTER
    };
}

export function registerSuccess() {
    return {
        type: AUTH_REGISTER_SUCCESS,
    };
}

export function registerFailure(error) {
    return {
        type: AUTH_REGISTER_FAILURE,
        error
    };
}

/* GET STATUS */
export function getStatusRequest() {
    return (dispatch) => {
        // inform Get Status API is starting
        dispatch(getStatus());
        
        return axios.get('/api/v2/auth/me')
        .then((response) => {
            console.log('!!!!:::::',response.data.info.success);
            dispatch(getStatusSuccess(response.data.info.username, response.data.info.success));
        }).catch((error) => {
            dispatch(getStatusFailure());
        });
    };
}

export function getStatus() {
    return {
        type: AUTH_GET_STATUS
    };
}

export function getStatusSuccess(username, valid) {
    return {
        type: AUTH_GET_STATUS_SUCCESS,
        username,
        valid
    };
}

export function getStatusFailure() {
    return {
        type: AUTH_GET_STATUS_FAILURE
    };
}

/* Logout */
export function logoutRequest(token) {
    console.log('!!!!!!!!', token)
    return (dispatch) => {
        return axios.get('/api/v2/auth/logout', 
        {
            headers: {
              'x-access-token': token          
            }
        })
        .then((response) => {            
            dispatch(logout());
        });
    };
}

export function logout() {
    return {
        type: AUTH_LOGOUT
    };
}