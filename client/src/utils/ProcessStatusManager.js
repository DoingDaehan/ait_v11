


const JobEnum = {
    IDLE: 0,
    CRAWLING: 100,
    CRAWLING_DONE: 101,
    CRAWLING_FAIL: 109,
    LEARNING: 200,
    LEARNING_DONE: 201,
    WAITING_LEARNING: 202,
    LEARNING_FAIL: 209,
    CROPPING: 300,
    CROPPING_DONE: 301,
    CROPPING_FAIL: 309,
    PREDICTION: 400,
    PREDICTION_DONE: 401,
    PREDICTION_FAIL: 409,
    CLASSIFYING: 500,
    CLASSIFYING_DONE: 501,
    CLASSIFIYING_FAIL: 509,
    READY_FOR_CONFIRM: 600,
    READY_FOR_TRAINING: 700,
    DOWNLOADING_IMAGES: 801,
    DOWNLOADING_IMAGES_DONE: 802,
    DOWNLOADING_FILE_LIST: 811,
    DOWNLOADING_FILE_LIST_DONE: 812,
    ERROR: 999,
    properties: {
        0: {name: "준비", value: 0},
        100: {name: "크롤링", value: 10},
        101: {name: "크롤링 완료", value: 20},
        109: {name: "크롤링 실패", value: -1},
        200: {name: "학습 중", value: 30},
        201: {name: "학습 완료", value: 40},
        202: {name: "학습 대기 중", value: 40},        
        209: {name: "학습 실패", value: -1},        
        400: {name: "예측 중", value: 45},
        401: {name: "예측 완료", value: 50},
        409: {name: "예측 실패", value: -1},
        500: {name: "분류 중", value: 55},
        501: {name: "분류 완료", value: 60},
        509: {name: "분류 실패", value: -1},
        300: {name: "이미지 자르기 중", value: 70},
        301: {name: "이미지 자르기 완료", value: 80},
        309: {name: "이미지 자르기 실패", value: -1},
        600: {name: "확인 필요", value: 90},
        700: {name: "모델 학습 준비 완료", value: 100},
        801: {name: "사용자 이미지 다운로드 중", value: 0},
        802: {name: "사용자 이미지 다운로드 완료", value: 0},
        811: {name: "사용자 이미지 목록 수집 중", value: 0},
        812: {name: "사용자 이미지 목록 수집 완료", value: 0},
        999: {name: "ERROR", value: -1},
    }    
};

 
export default JobEnum;

// function getStatusDesc(job) {
//     // console.log("JOB:: ", job);
//     // console.log(this.JobEnum.properties[job]);
    
//     return this.JobEnum.properties[job];
// }

// function getStatus(job, status) {
//     console.log("JOB:: ", job, "Status:: ", status);
//     console.log(this.JobEnum.properties[job] + this.JobStatusEnum.properties[status]);
//     return this.JobEnum.properties[job] + this.JobStatusEnum.properties[status];
// }



//export default ProcessStatusManager;

