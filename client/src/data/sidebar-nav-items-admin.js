export default function() {
  return [
    // {
    //   title: "Blog Dashboard",
    //   to: "/blog-overview",
    //   htmlBefore: '<i class="material-icons">edit</i>',
    //   htmlAfter: ""
    // },
    // {
    //   title: "Blog Posts",
    //   htmlBefore: '<i class="material-icons">vertical_split</i>',
    //   to: "/blog-posts",
    // },
    // {
    //   title: "Add New Post",
    //   htmlBefore: '<i class="material-icons">note_add</i>',
    //   to: "/add-new-post",
    // },
    // {
    //   title: "Forms & Components",
    //   htmlBefore: '<i class="material-icons">view_module</i>',
    //   to: "/components-overview",
    // },
    // {
    //   title: "Tables",
    //   htmlBefore: '<i class="material-icons">table_chart</i>',
    //   to: "/tables",
    // },
    // {
    //   title: "User Profile",
    //   htmlBefore: '<i class="material-icons">person</i>',
    //   to: "/user-profile-lite",
    // },
    // {
    //   title: "Errors",
    //   htmlBefore: '<i class="material-icons">error</i>',
    //   to: "/errors",
    // },
    // {
    //   title: "FoodDBs",
    //   //htmlBefore: '<i class="material-icons">table_chart</i>',
    //   to: "/fooddbs",
    // },
    {
      title: "새 음식 추가",
      //htmlBefore: '<i class="material-icons">table_chart</i>',
      to: "/newfood",
    },
    {
      title: "기존 음식 수정",
      //htmlBefore: '<i class="material-icons">table_chart</i>',
      to: "/existfood",
    },
    {
      exact: true,
      title: "사용자 데이터 확인",
      //htmlBefore: '<i class="material-icons">table_chart</i>',
      to: "/userfood",
    },
    {
      exact: true,
      title: "사용자 데이터 확인(FoodImage)",
      //htmlBefore: '<i class="material-icons">table_chart</i>',
      to: "/userfood/foodimage",
    },
    {
      title: "사용자 데이터 추가",
      //htmlBefore: '<i class="material-icons">table_chart</i>',
      to: "/adduserfood",
    },
    {
      title: "Predict Display",
      //htmlBefore: '<i class="material-icons">table_chart</i>',
      to: "/predict/display",
    },
    // {
    //   title: "학습 데이터 확인",
    //   //htmlBefore: '<i class="material-icons">table_chart</i>',
    //   to: "/checkfoodlist",
    // }
  ];
}
