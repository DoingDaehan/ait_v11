import React, { Component } from 'react';
import NewFoodInputTemplate from "../components/add-new-food/NewFoodInputTemplate";

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as inputActions from '../modules/input';
import * as foodsActions from '../modules/foods';

import axios from 'axios';
import Cookies from 'universal-cookie';

const cookies = new Cookies();
class FoodInputContainer extends Component {

    constructor(props){
        super(props);

        this.state = {
            loginData: {}
        }
    }

    handleInsertFood = () => {
        // let foodname = this.state.foodname;
        // let keyword = this.state.keyword;
        const { InputActions, FoodsActions, keyword, foodname } = this.props;

        // this.setState({
        //     foodname: "",
        //     keyword: ""
        // });

        console.log(this.state.loginData);

        fetch('/registeritem', {    
        //fetch('/api/v1/data/register', {
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'x-access-token': this.state.loginData.token
            },
            body: JSON.stringify({
                "foodname": foodname,
                "keyword": keyword
            })
        })
        .then(res => res.json())
        .then(data => {          
            console.log('BEFORE:::', this.state)
            this.setState({                
                data:data
            })
            console.log('AFTER:::', this.state)
        },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                }) 
            }
        );     
        

        console.log("handleInsert Button", keyword, foodname);
        FoodsActions.insert({"foodname":foodname, "keyword":keyword});

        InputActions.setInputKeyword("");
        InputActions.setInputFoodname("");
        
    }
    
    handleChange = (e) => {    

        const { value } = e.target;
        const { InputActions } = this.props;

        if(e.target.name === "keyword"){
            InputActions.setInputKeyword(value);
        } else if(e.target.name === "foodname"){
            InputActions.setInputFoodname(value);
        }
        
    }

    handleDragDrop = (files) => {
        console.log(files[0]);
        this.setState({
            selectedFile: files[0].src.file,
            loaded: 0,
        },
       () => {
            const data = new FormData() 
            data.append('file', this.state.selectedFile);
    
            axios.post("/api/addcrawlingkeywords", data, {
                onUploadProgress: ProgressEvent => {
                    this.setState({
                        loaded: (ProgressEvent.loaded / ProgressEvent.total*100),
                    })
                }
            })
            .then(res => { // then print response status
                console.log(res.statusText)
            })
        })
    }

    handleCrawlingStart = () => {
        axios.get("/api/crawlingstart", {
            params: {filename: "1561698306054-food_list (1) - 복사본.txt"}            
        },
        {
            headers: {'x-access-token': this.state.loginData.token}
        })
        .then(res => { // then print response status
            console.log(res.statusText)
        })
    }

    componentDidMount() {
        // get loginData from cookie
        let loginData = cookies.get('key');
                
        // if loginData is undefined, do nothing
        if(typeof loginData === "undefined") return;

        // decode base64 & parse json
        loginData = JSON.parse(atob(loginData));
        console.log(loginData);

        this.setState({loginData: loginData});
    }

    render(){
        const { keyword, foodname, loaded } = this.props;
        return (
            <NewFoodInputTemplate 
                    keyword={keyword} 
                    foodname={foodname} 
                    onCrawlingStart={this.handleCrawlingStart} 
                    onInsertFood={this.handleInsertFood} 
                    onChange={this.handleChange} 
                    loaded={loaded} 
                    onDragDrop={this.handleDragDrop}/>         
        )
    }
}

export default connect(
    (state) => ({
        keyword: state.input.get('keyword'),
        foodname: state.input.get('foodname')
    }),   
    (dispatch) => ({
        InputActions: bindActionCreators(inputActions, dispatch),
        FoodsActions: bindActionCreators(foodsActions, dispatch)
    })
)(FoodInputContainer);