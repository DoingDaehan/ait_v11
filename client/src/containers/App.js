import React from 'react';
import { Header } from '../components/layout';
import { connect } from 'react-redux';
import { getStatusRequest, logoutRequest } from '../actions/authentication';
import { BrowserRouter as Router, Route, Redirect, Link } from "react-router-dom";

import withTracker from "../withTracker";
import routes from "../routes";

import "bootstrap/dist/css/bootstrap.min.css";
import "../shards-dashboard/styles/shards-dashboards.1.1.0.min.css";
import Cookies from 'universal-cookie';

const cookies = new Cookies();
class App extends React.Component {
    constructor(props) {
        super(props);        
    }

    handleLogout = () => {
        this.props.logoutRequest().then(
            () => {
                // Materialize.toast('Good Bye!', 2000);
                console.log('Good Bye');

                // EMPTIES THE SESSION
                let loginData = {
                    isLoggedIn: false,
                    username: ''
                };

                //document.cookie = 'key=' + btoa(JSON.stringify(loginData));
                cookies.set('key', btoa(JSON.stringify(loginData)));
            }
        );
    }

    componentDidMount() {
        // get cookie by name
        function getCookie(name) {
            //var value = "; " + document.cookie;
            // var value = "; " + cookies.get('key');
            // var parts = value.split("; " + name + "=");
            // if (parts.length === 2) return parts.pop().split(";").shift();
            return cookies.get('key');
        }

        // get loginData from cookie
        let loginData = getCookie('key');

        // if loginData is undefined, do nothing
        if(typeof loginData === "undefined") return;

        // decode base64 & parse json
        loginData = JSON.parse(atob(loginData));
        
        // if not logged in, do nothing
        if(!loginData.isLoggedIn) return;

        // page refreshed & has a session in cookie,
        // check whether this cookie is valid or not
        console.log("!!!@!#!@#@!#!@");
        this.props.getStatusRequest().then(
            () => {
                console.log('dfdfdfdfd', this.props.status);
                // if session is not valid
                if(!this.props.status.valid) {
                    // logout the session
                    loginData = {
                        isLoggedIn: false,
                        username: ''
                    };

                    //document.cookie='key=' + btoa(JSON.stringify(loginData));
                    cookies.set('key', btoa(JSON.stringify(loginData)));

                    // and notify
                    // let $toastContent = $('<span style="color: #FFB4BA">Your session is expired, please log in again</span>');
                    // Materialize.toast($toastContent, 4000);
                    console.log('Your session is expired, please log in again');

                }
            }
        );
    }

    render() {
        /* Check whether current route is login or register using regex */
        let re = /(login|register)/;
        //let isAuth = re.test(this.props.location.pathname);
        let isAuth = 0;
        console.log(this.props.status.isLoggedIn);
 
        return (
            <div>
                
                <Router basename={process.env.REACT_APP_BASENAME || ""}>
                    <div>
                    {routes.map((route, index) => {
                        return (
                        <Route
                            key={index}
                            path={route.path}
                            exact={route.exact}
                            component={withTracker(props => {
                            return (
                                <route.layout {...props}>
                                <route.component {...props} />
                                </route.layout>
                            );
                            })}
                        />
                        );
                    })}
                    </div>
                </Router>
            </div>
            
        
        );
        
        // return (
        //     <div>
        //         {/* {isAuth ? undefined : <Header isLoggedIn={this.props.status.isLoggedIn}
        //                                         onLogout={this.handleLogout}/>}
        //         { this.props.children } */}
        //         hello
        //     </div>
        // );
        
    }
}

const mapStateToProps = (state) => {
    return {
        status: state.authentication.status
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getStatusRequest: () => {
            return dispatch(getStatusRequest());
        },
        logoutRequest: () => {
            return dispatch(logoutRequest());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);