import React, { Component } from 'react';
import NewFoodList from "../components/add-new-food/NewFoodList";

import { connect } from 'react-redux';
import { bindActionCreatros } from 'redux';

import * as foodsActions from '../modules/foods';

class NewFoodListContainer extends Component {

    handleToggle = (id) => {
        const {data} = this.state;
        const index = data.findIndex(d => d._id === id);

        
        if(index !== -1){
        
            let tDone = !data[index].attribute.done;

            fetch("/list/askconfirm", {
                method: 'POST',
                headers: {'Content-Type':'application/json'},
                body: JSON.stringify({
                    "id": data[index]._id,
                    "done": tDone                
                })
            })   
            .then(res => res.json())    
            .then(data => {
                console.log(data);
                let newState = update(this.state, {
                    data: {
                        [index]: {
                            attribute: {$set: data.attribute}
                        }
                    } 
                });
            
                this.setState(newState);
            });    
        }
        
    }

    handleRemove = (id) => {
        // const {data} = this.state;
        // const index = data.findIndex(d => d._id === id);
       
        // if(index !== -1){                                

        //     fetch("/list/remove", {
        //         method: 'POST',
        //         headers: {'Content-Type':'application/json'},
        //         body: JSON.stringify({
        //             "id": data[index]._id,
        //             "status": data[index].attribute.state
        //         })
        //     })              
        //     .then(data => {      
        //         console.log(data);
        //         this.setState({
        //             data: update(
        //                 this.state.data,{
        //                     $splice: [[index, 1]]
        //                 }
                        
        //             )
        //         });
        //     });    
        // }       
        const { FoodsActions } = this.props;
        FoodsActions.remove(id);
    }

    render(){
        const { data } = this.props;
        return (
            <NewFoodList 
                data={data} 
                onToggle={this.handleToggle} 
                onRemove={this.handleRemove}/>
        );
    }
}

export default connect(
    (state) => ({
        data: state.data
    }),
    (dispatch) => ({
        NewFoodListActions: bindActionCreatros(foodsActions, dispatch)
    })
)(NewFoodListContainer)