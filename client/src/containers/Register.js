import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as authActions from '../modules/auth';
import Authentication from '../views/Authentication';

class RegisterContainer extends Component {
    render() {
        const { onChange } = this.props;
        return (
            <div>
                <Authentication mode={true} onChange={onChange}/>
            </div>
        )
    }
}

export default RegisterContainer;