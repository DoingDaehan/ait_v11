import React from 'react';
import { Authentication } from '../components/auth';
import { connect } from 'react-redux';
import { loginRequest } from '../actions/authentication';
import { ToastContainer, toast } from 'react-toastify';
import { getStatusRequest, logoutRequest } from '../actions/authentication';
import Cookies from 'universal-cookie';

import 'react-toastify/dist/ReactToastify.css';
const cookies = new Cookies();
class Login extends React.Component {
     
    
    notify = (str) => toast(str);
    
    handleLogin = (id, pw) => {
        return this.props.loginRequest(id, pw).then(
            () => {
                                
                if(this.props.status === "SUCCESS") {
                    // create session data
                    let loginData = {
                        isLoggedIn: true,
                        username: id,
                        token:this.props.token              
                    };

                    console.log('::::', JSON.stringify(loginData));
                    
                    //document.cookie = 'key=' + btoa(JSON.stringify(loginData));
                    cookies.set('key', btoa(JSON.stringify(loginData)));
                    //this.notify(loginData.username + ' 로그인 성공');
                    this.props.history.push('/newfood');
                    return true;
                } else {                    
                    this.notify("아이디와 비밀번호를 확인 해 주세요.");                    
                    return false;
                }
            }
        );
    }

    handleLogout = () => {
        this.props.logoutRequest().then(
            () => {
                // Materialize.toast('Good Bye!', 2000);
                console.log('Good Bye');

                // EMPTIES THE SESSION
                let loginData = {
                    isLoggedIn: false,
                    username: '',
                    token: ''
                };

                // document.cookie = 'key=' + btoa(JSON.stringify(loginData));
                cookies.set('key', btoa(JSON.stringify(loginData)));
            }
        );
    }

    componentDidMount() {
        // setCookie('key', 'key-value', {
        //     //maxAge: 30 * 24 * 60 * 60,
        //     maxAge: 60 * 60,
        //     path: '/'
        // })
        // get cookie by name
        // function getCookie(name) {
        //     //var value = "; " + document.cookie;            
        //     // var value = "; " + cookies.get('key');
        //     // var parts = value.split("; " + name + "=");
        //     // if (parts.length === 2) return parts.pop().split(";").shift();
        //     //return Cookies.get(name);
        //     return getCookie(name)
        // }

        // get loginData from cookie
        let loginData = cookies.get('key');
                
        // if loginData is undefined, do nothing
        if(typeof loginData === "undefined") return;

        // decode base64 & parse json
        loginData = JSON.parse(atob(loginData));
        console.log(loginData);

        // if not logged in, do nothing
        // if(loginData.isLoggedIn) {
        //     //return;
        //     this.props.history.push('/newfood')
        // } else 
        
        // page refreshed & has a session in cookie,
        // check whether this cookie is valid or not
        this.props.getStatusRequest().then(
            () => {
                console.log(this.props);
                // if session is not valid
                //if(!this.props.status.valid) {
                if(!loginData.isLoggedIn) {
                    // logout the session
                    loginData = {
                        isLoggedIn: false,
                        username: '',
                        token: ''
                    };

                    //document.cookie='key=' + btoa(JSON.stringify(loginData));
                    cookies.set('key', btoa(JSON.stringify(loginData)));

                    // and notify
                    // let $toastContent = $('<span style="color: #FFB4BA">Your session is expired, please log in again</span>');
                    // Materialize.toast($toastContent, 4000);
                    console.log('Your session is expired, please log in again');

                }
                else {
                    this.props.history.push('/newfood');
                }
            }
        );        
    }
    
    render() {
        //const { name } = this.state;

        return (
            <div>
                <Authentication mode={true} 
                    onLogin={this.handleLogin}/>                
                <div>
                    <button onClick={this.notify}>Notify !</button>
                    <ToastContainer />
                </div>
                {/* <div>                    
                    {this.state.name && <h1>Hello {this.state.name}!</h1>}
                </div> */}
            </div>            
        );
    }
}

const mapStateToProps = (state) => {
    return {
        status: state.authentication.login.status,
        token: state.authentication.login.token
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        loginRequest: (id, pw) => { 
            return dispatch(loginRequest(id,pw)); 
        },
        getStatusRequest: () => {
            return dispatch(getStatusRequest());
        },
        logoutRequest: () => {
            return dispatch(logoutRequest(this.state.loginData.token));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);