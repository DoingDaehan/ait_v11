var mysql = require('mysql');
/*
* 기본 정보 설정
*/

var dbconfig = require('../configuration/dbconfig.json')
const connectionInfo = {
    host : dbconfig.host,
    user : dbconfig.user,
    port : dbconfig.port,
    password : dbconfig.password,
    database : dbconfig.database,
    connectionLimit:100,
    waitForConnections:true,
    multipleStatements : true
};

/*
* pool 에서 실행되는 모든 쿼리들의 sql 로그를 남김
*/
const poolEventSetup = () => {
  global.pool.on('connection', (poolConnection) => {
    poolConnection.on('enqueue', (sequence) => {
      if (sequence.constructor.name === 'Query') {
        console.log(sequence.sql);
      }
    });
  });
};

/*
* 일반 connection 접속 체크 후 pool 생성
*/
const initialization = (callback) => {
  const connectionCheck = mysql.createConnection(connectionInfo);
  connectionCheck.connect((err) => {
    connectionCheck.end();

    // connection 에러 발생
    if (err) return callback(err);

    // global 할당 ex) pool.query('select * from test', (err, results) => {});
    global.pool = mysql.createPool(connectionInfo);

    // 개발 환경 시 실행 query 로깅 활성화
    if (process.env.NODE_ENV !== 'production') poolEventSetup();

    callback(null);
  });
};

module.exports.init = initialization;