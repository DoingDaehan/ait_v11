var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {  
  res.render('index', { title: 'Express' });
});

router.get('/test', function(req, res, next){
  res.status(200).json("OK");  
});


router.get('/call', function(req, res, next){
  res.status(200).json(__basedir);
});

module.exports = router;
