
// var job = Object.freeze({
//     IDLE: 0,
//     CRAWLING: 100,
//     CRAWLING_DONE: 101,
//     LEARNING: 200,
//     LEARNING_DONE: 201,
//     WAITING_LEARNING: 202,
//     CROPPING: 300,
//     CROPPING_DONE: 301,
//     PREDICTION: 400,
//     PREDICTION_DONE: 401,
//     CLASSIFYING: 500,
//     CLASSIFYING_DONE: 501,
//     READY_FOR_CONFIRM: 600,
//     READY_FOR_TRAINING: 700,
//     ERROR: 999,
//     properties: {
//         0: {name: "IDLE", value: 0},
//         100: {name: "CRAWLING", value: 10},
//         101: {name: "CRAWLING DONE", value: 20},
//         200: {name: "LEARNING", value: 30},
//         201: {name: "LEARNING DONE", value: 40},
//         202: {name: "WAITING LEARNING", value: 40},        
//         400: {name: "PREDICTION", value: 45},
//         401: {name: "PREDICTION DONE", value: 50},
//         500: {name: "CLASSIFYING", value: 55},
//         501: {name: "CLASSIFYING DONE", value: 60},
//         300: {name: "CROPPING", value: 70},
//         301: {name: "CROPPING DONE", value: 80},
//         600: {name: "READY FOR CONFIRM", value: 90},
//         700: {name: "READY FOR TRAINING", value: 100},
//         999: {name: "ERROR", value: -1},
//     }    
// });

module.exports = function(){
    return {
        IDLE: function(){
            return {name: "IDLE", value: 0, code:0}
        },

        CRAWLING: function(){
            return {name: "CRAWLING", value: 10, code: 100}
        },

        CRAWLING_DONE: function(){
            return {name: "CRAWLING DONE", value: 20, code: 101}
        },

        CRAWLING_FAIL: function(){
            return {name: "CRAWLING FAIL", value: -1, code: 109}
        },

        LEARNING: function(){
            return {name: "LEARNING", value: 30, code: 200}
        },

        LEARNING_DONE: function(){
            return {name: "LEARNING DONE", value: 40, code: 201}
        },

        WAITING_LEARNING: function(){
            return {name: "WAITING LEARNING", value: 40, code:202}
        },

        LEARNING_FAIL: function(){
            return {name: "LEARNING FAIL", value: -1, code:209}
        },

        PREDICTION: function(){
            return {name: "PREDICTION", value: 45, code: 400}
        },

        PREDICTION_DONE: function(){
            return {name: "PREDICTION DONE", value: 50, code: 401}
        },

        PREDICTION_FAIL: function(){
            return {name: "PREDICTION FAIL", value: -1, code: 409}
        },

        CLASSIFYING: function(){
            return {name: "CLASSIFYING", value: 55, code: 500}
        },

        CLASSIFYING_DONE: function(){
            return {name: "CLASSIFYING DONE", value: 60, code: 501}
        },

        CLASSIFYING_FAIL: function(){
            return {name: "CLASSIFYING FAIL", value: -1, code: 509}
        },

        CROPPING: function(){
            return {name: "CROPPING", value: 70, code: 300}
        },

        CROPPING_DONE: function(){
            return {name: "CROPPING DONE", value: 80, code: 301}
        },

        CROPPING_FAIL: function(){
            return {name: "CROPPING FAIL", value: -1, code: 309}
        },

        READY_FOR_CONFIRM: function(){
            return {name: "READY FOR CONFIRM", value: 90, code: 600}
        },

        READY_FOR_TRAINING: function(){
            return {name: "READY FOR TRAINING", value: 100, code: 700}
        },

        ERROR: function(){
            return {name: "ERROR", value: -1, code: 999}
        },

        DOWNLOADING_IMAGES: function(){
            return {name: "DOWNLOADING IMAGES", value: 0, code:801}
        },

        DOWNLOADING_IMAGES_DONE: function(){
            return {name: "DOWNLOADING IMAGES DONE", value: 0, code:802}
        },

        DOWNLOADING_FILE_LIST: function(){
            return {name: "DOWNLOADING FILE LIST", value: 0, code:811}
        },

        DOWNLOADING_FILE_LIST_DONE: function(){
            return {name: "DOWNLOADING FILE LIST DONE", value: 0, code:812}
        },

    };
}
// module.exports = {
//     JobEnum() {
        
//         return job;
//     }
// }
// function getStatusDesc(job) {
//     // console.log("JOB:: ", job);
//     // console.log(this.JobEnum.properties[job]);
    
//     return this.JobEnum.properties[job];
// }

// function getStatus(job, status) {
//     console.log("JOB:: ", job, "Status:: ", status);
//     console.log(this.JobEnum.properties[job] + this.JobStatusEnum.properties[status]);
//     return this.JobEnum.properties[job] + this.JobStatusEnum.properties[status];
// }



//export default ProcessStatusManager;

