const Router = require('express').Router
module.exports = Router({mergeParams: true})
.get('/v1/users', async (req, res, next) => {
    try {
        const users = await req.db.User.find()
        console.log('1111 ', users);
        res.send(users);
    } catch (error) {
        next(error)
    }
})