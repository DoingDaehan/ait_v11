const routerUtil = require('../../../router_util');
const jobManager = require('../../../ProcessStatusManager');
const job = jobManager();
const Router = require('express').Router
module.exports = Router({mergeParams: true})
.post('/v1/data/register', async (req, res, next) => {
    try {                    
        // let data = await req.db.Food.find({"attribute.is_activated": true});
        // res.status(200).json(data);      
        var foodname = req.body.foodname;
        var keyword = req.body.keyword;

        console.log(foodname, keyword);

        var FoodModel = req.db.Food;
        console.log(FoodModel)
        var food = new FoodModel({
            attribute: {                
                name: foodname,
                keyword: keyword,
                state: job.CRAWLING().code    
            },
            source: {
                national_code: "KR",
                name: "KR",
                filename: "Manual"
            },
            image: {
                
            }    
        })
        // const food = new req.db.Food({            
        //     attribute: {                
        //         name: foodname,
        //         keyword: keyword,
        //         state: job.CRAWLING().code    
        //     },
        //     source: {
        //         national_code: "KR",
        //         name: "KR",
        //         filename: "Manual"
        //     },
        //     image: {
                
        //     }
        // });
        
        console.log(food)
        
        const t = await food.save();

        routerUtil.addCrawling(t._id, keyword);
        
        //const location = `${req.base}${req.originalUrl}/${user.id}`
        //res.setHeader('Location', location)
        res.status(201).redirect('datas');        
    } catch(error) {
        next(error)
    }
});


    