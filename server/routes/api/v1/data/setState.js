const routerUtil = require('../../../router_util');
const path_util = require('../../../path_util');
const fs = require('fs');
const Router = require('express').Router;
const ROOT_PATH = path_util.getDataPath() + '/';
module.exports = Router({mergeParams: true})
.post('/v1/data/setState/', async (req, res, next) => {
    try {                    
        var id = req.body.id;
        var fn = req.body.file;
        var filename = fn.split('/').pop();
        var keyword = "";


        const rawData = await req.db.Food.findOne({_id:id});
        let tData = JSON.stringify(rawData);
        let foodObj = JSON.parse(tData);

        keyword = foodObj.attribute.keyword;

        console.log(id, foodObj._id, filename)
        await req.db.Food.updateOne(
            {
                "_id":foodObj._id, 
                image: 
                {
                    $elemMatch: 
                    {
                        "filename":filename
                    }
                }
            }, 
            {
                $set: 
                {
                    "image.$.s1class": 1
                }
            }
        );

        
        var source = ROOT_PATH + fn;
        var dest = ROOT_PATH + keyword + '/train/good/' + filename;
               
        routerUtil.copyFile(source, dest, (err) => {
            if (err) throw err;
            console.log(source, ' was copied to ', dest);
        });

        var bad = ROOT_PATH + keyword + '/train/bad/' + filename;
        if(fs.existsSync(bad)) {
            console.log('del:'+bad);
            fs.unlink(bad, function() {});
        }
        
        console.log(id, fn, filename)
        
        let goodImages = [];
        let badImages = [];
        
        const obj = await req.db.Food.findOne({_id:id});        
        tObj = JSON.stringify(obj);
        foodObj = JSON.parse(tObj);

        foodObj.image.forEach( row => {                    
            row.filename = keyword+'/' + row.filename;
            let tFileName = row.filename;
            
            if(row.s1class == 1){ // Good                        
                //row.filename = row.filename.replace('/', '/classified/good/');
                let newRow = JSON.parse(JSON.stringify(row));
                goodImages.push(newRow);
                row.filename = tFileName;
            } else if(row.s1class == -1){ // Bad                        
                //row.filename = row.filename.replace('/', '/classified/bad/');
                let newRow = JSON.parse(JSON.stringify(row));
                badImages.push(newRow);
                row.filename = tFileName;
            } 
        });

        //console.log(foodObj.image, goodImages, badImages)
        res.status(200).json(
            {
                images: foodObj.image, 
                goodImages:goodImages, 
                badImages:badImages
            }
        );
    } catch(error) {
        next(error)
    }
})