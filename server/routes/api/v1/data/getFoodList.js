const Router = require('express').Router
module.exports = Router({mergeParams: true})
.get('/v1/foodlist/', async (req, res, next) => {
    try {                    
        let data = await req.db.Food.find({"attribute.is_activated": true});
        res.status(200).json(data);      
    } catch(error) {
        next(error)
    }
})