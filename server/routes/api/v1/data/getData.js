const path_util = require('../../../path_util');
const fs = require('fs');
const jobManager = require('../../../ProcessStatusManager');
const job = jobManager();
const Router = require('express').Router
module.exports = Router({mergeParams: true})
.get(['/v1/datas', '/v1/datas/:id'], async (req, res, next) => {
    try {
        var mState = Number(req.query.state);
        var id = req.params.id;
        console.log('>>>>>>>>>>>>>>>>> State:', mState, 'ID: ', id);


        if(id != null) {
            
            const rawData = await req.db.Food.findOne({_id:id});            
            let data = JSON.parse(JSON.stringify(rawData));
                        
            if(!data) return -1;
            
            if(data.length <= 0) {
                res.send('errors');
            }
        
            var keyword = data.attribute.keyword;
            var images = [];
            let goodImages = [];
            let badImages = [];
            let cropImages = [];
            
            let foodImages = data.image;  
            console.log(foodImages);
                    
            await foodImages.forEach( row => {                    
                row.filename = keyword+'/' + row.filename;
                let tFileName = row.filename;
                
                if(row.s1class == 1){ // Good                        
                    //row.filename = row.filename.replace('/', '/classified/good/');
                    let newRow = JSON.parse(JSON.stringify(row));
                    goodImages.push(newRow);
                    row.filename = tFileName;
                } else if(row.s1class == -1){ // Bad                        
                    //row.filename = row.filename.replace('/', '/classified/bad/');
                    let newRow = JSON.parse(JSON.stringify(row));
                    badImages.push(newRow);
                    row.filename = tFileName;
                } 
            });
            
            console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            console.log(goodImages);
            
            console.log(badImages);
                        
            console.log(mState);            
            if(mState === job.CLASSIFYING_DONE().code){
                
                await goodImages.forEach(item => {                        
                    item.filename = item.filename.replace('/', '/classified/good/');
                });

                await badImages.forEach(item => {
                    item.filename = item.filename.replace('/', '/classified/bad/');
                });
                
                console.log(badImages);

                res.status(200).json(
                    {
                        itemid: req.params.id, 
                        foodname: data.attribute.foodname, 
                        keyword: keyword, 
                        images: "", 
                        activation:data.attribute.is_activated, 
                        goodImages:goodImages, 
                        badImages:badImages
                    }
                );
            } else if((mState === job.CROPPING_DONE().code) ||
                    (mState === job.READY_FOR_CONFIRM().code) ||
                    (mState === job.READY_FOR_TRAINING().code)) {
                                
                //const croppedPath = '../ait_data/' + keyword + '/classified_cropped/good';
                const croppedPath = path_util.getDataPathWithKeywordCropped(keyword);
                const fs = require('fs');
                let cropped_images = [];

                if (fs.existsSync(croppedPath)) {
                    fs.readdirSync(croppedPath).forEach(file => {                    
                        cropped_images.push({filename: keyword + '/classified_cropped/good/' + file});
                    });
                }

                console.log(cropped_images);

                res.status(200).json(
                    {
                        itemid: req.params.id, 
                        foodname: data.attribute.foodname, 
                        keyword: keyword, 
                        images: foodImages, 
                        activation:data.attribute.is_activated, 
                        goodImages:goodImages, 
                        badImages:badImages, 
                        cropped_images:cropped_images
                    }
                );
            } else{                    
                res.status(200).json(
                    {
                        itemid: req.params.id, 
                        foodname: data.attribute.foodname, 
                        keyword: keyword, 
                        images: foodImages, 
                        activation:data.attribute.is_activated, 
                        goodImages:goodImages, 
                        badImages:badImages
                    }
                );
            }
                
                
          
        }else{
            let data = await req.db.Food.find({"attribute.is_activated": true});
            res.status(200).json(data);
        }
      
    } catch(error) {
        next(error)
    }
})