

const db = require('scripts/database_mongo');
const crawler = require('scripts/crawler');
const trainer = require('scripts/trainer');
const cropper = require('scripts/cropper');
const files = require('scripts/files')
const wt = require('worker-thread');
const fs = require('fs');
const jobEnum = require('routes/ProcessStatusManager.js');
const job = jobEnum();
const path_util = require('routes/path_util');
const Watchpuppy = require('watchpuppy');

var watchpuppy;

global.ch_crawl = wt.createChannel(worker_crawling, 1);
global.ch_train = wt.createChannel(worker_training, 1);
global.ch_cropper = wt.createChannel(worker_cropper, 1);

const rootpath = path_util.getDataPath() + '/';

var _getAllFilesFromFolder = function(dir) {
    
    var filesystem = require("fs");
    var results = [];

    filesystem.readdirSync(dir).forEach(function(file) {

        file = dir+'/'+file;
        var stat = filesystem.statSync(file);

        if (stat && stat.isDirectory()) {
            results = results.concat(_getAllFilesFromFolder(file))
        } else {
            console.log(file);
            if(file.indexOf('jpg') !== -1 ||
                file.indexOf('jpeg') !== -1 ||
                file.indexOf('png') !== -1 ||
                file.indexOf('JPG') !== -1 ||
                file.indexOf('JPEG') !== -1 ||
                file.indexOf('PNG') !== -1){
                    results.push(file);
            }
            
        }
    });

    return results;

};

function worker_cropper(params) {
    cmd = params[0]
    id = params[1]
    basePath = params[2]
    savePath = params[3]
    console.log('Start Cropping... ', cmd, id, basePath, savePath)

    return new Promise( r=> {
        //console.log('Start:'+id);
        //db.setState(id, 'Cropping...', savePath);
        db.setState(id, job.CROPPING().code);
        cropper.crop(basePath, savePath, function(){
            console.log('Done:' + savePath);
            //db.setState(id, 'Cropping Done');
            db.setState(id, job.CROPPING_DONE().code);
            console.log('Start prediction after cropping ')
            runPredict(id, savePath + 'good', basePath.replace('/classified', '/train'), savePath + 'good', "remove");
        });

    })
    .then(r => {
        console.log(r) // will not get executed
    })
    .catch(err => {
        console.error(err) // we will see what to do with it later
        return Promise.resolve(3)
    })
    .then(r => {
        console.log(r) // 3
    })
    .catch(err => {
        // in case in the previous block occurs another error
        console.error(err)
    });
}

function worker_training(params) {
    var cmd = params[0]
    var id = params[1]
    var pathRoot = params[2]
    var pathTrain = params[3]
    var pathClassify = params[4]

    console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', cmd);

    if( cmd.toString() == 'train_predict2') {
        return new Promise( r=> {
            console.log('Start:'+pathTrain);
            db.setState(id, job.LEARNING().code);

            trainer.train(pathTrain, pathClassify, function(ret) {
                
                console.log(">>>>>", ret);

                if(ret === -10 || ret === -11 || ret === -12){
                    db.setState(id, job.LEARNING_FAIL().code);
                } else if(ret === 1){
                    db.setState(id, job.LEARNING_DONE().code);
                    console.log('TRAIN Done:'+ pathTrain)
                                        
                    r([id, pathRoot, pathTrain, pathClassify]);
                }                
            })
        })
        .then(runPredic2)
        .then(() => {
            console.log('PREDICT Done:'+ pathTrain)
        })
        // .then(() => {            
        //     runPredic2(id, pathRoot, pathTrain, pathClassify)
        //     .then(function(){
        //         console.log('PREDICT Done:'+ pathTrain)
            
        //         //updateImageClass(id, pathClassify);
        //     });
        // })
        // .catch(err => {
        //     console.error(err) // we will see what to do with it later
        //     return Promise.resolve(3)
        // });        
    }

    if( cmd.toString() == 'train_predict') {
        return new Promise( r=> {
            console.log('Start:'+pathTrain);
            db.setState(id, job.LEARNING().code);

            trainer.train(pathTrain, pathClassify, function(ret) {
                
                console.log(">>>>>", ret);

                if(ret === -10 || ret === -11 || ret === -12){
                    db.setState(id, job.LEARNING_FAIL().code);
                } else if(ret === 1){
                    db.setState(id, job.LEARNING_DONE().code);
                    console.log('TRAIN Done:'+ pathTrain)
                                        
                    r()                    
                }                
            })
        })
        .then(r => {
            console.log('!!!!@#!@#!', r) // will not get executed
            runPredict(id, pathRoot, pathTrain, pathClassify)
            .then(function(){
                console.log('PREDICT Done:'+ pathTrain)
            
                updateImageClass(id, pathClassify);
            });
        })
        .catch(err => {
            console.error(err) // we will see what to do with it later
            return Promise.resolve(3)
        });        
    }


    if( cmd.toString() == 'train') {
        return new Promise( r=> {
            console.log('Start:'+pathTrain);
            db.setState(id, job.LEARNING().code);

            trainer.train(pathTrain, pathClassify, function() {
                //db.setState(id, 'training done')
                //console.log(">>>>>", ret);

                // if(ret === -10 || ret === -11){
                //     db.setState(id, job.LEARNING_FAIL().code);
                // } else{
                    db.setState(id, job.LEARNING_DONE().code);
                    console.log('TRAIN Done:'+ pathTrain)
                    
                    setTimeout(function () {
                        r()
                    }, 1000)
                //}
                
            })
        })
        .then(r => {
            console.log(r) 
        })
        .catch(err => {
            console.error(err) // we will see what to do with it later
            return Promise.resolve(3)
        });
    }

    if( cmd.toString() == 'predict') {        
        runPredict(id, pathRoot, pathTrain, pathClassify)
            .then(function(){
                console.log('PREDICT Done:'+ pathTrain)
            
                updateImageClass(id, pathClassify);
            });
    }

    if( cmd.toString() == 'predict_cropping') {        
        runPredict(id, pathRoot, pathTrain, pathClassify)
            .then(function(){
                console.log('Done:'+ pathTrain)          
                updateImageClass(id, pathClassify);
                runCropping(id);
            })
            .then(function(){
                // runCropping(id);
            })
            // .then(function(){
            //     console.log('?!!!!??????????????????????????????????after cropping?!!!!??????????????????????????????????')
            // });
    }

    if( cmd.toString() == 'simple_predict') {
        runPredict(id, pathRoot, pathTrain, pathClassify)
            .then(function(){                
                console.log('Simple Classifying Done:'+ pathTrain)
            });       
    }

    
}

function synctoDB(id, keyword) {
    var path = rootpath + keyword
    var images = []
    console.log('synctoDB:::', path)
    if(fs.existsSync(path)) {
        console.log('111');
        fs.readdir(path, (err, files) => {
            files.forEach( file => {
                //console.log(file)
                //images.push([id, file, 0, 0]);
                if(file.indexOf('classified') === -1 && file.indexOf('train') === -1){
                    images.push([file, 0, 0]);
                }
                
            });
            db.pushImagelist(id, images, function() {
                console.log(keyword+' : DB Sync');
            })
        });
    }
    
}

function createFolders(keyword) {
    var path = rootpath;

    console.log(path);
    console.log(rootpath);
    console.log(keyword);

    baseKeywordPath = path + keyword 

    console.log(':::::', baseKeywordPath);

    if (!fs.existsSync(baseKeywordPath)){
        fs.mkdirSync(baseKeywordPath);        
    }

    if (!fs.existsSync(baseKeywordPath+'/train')){
        fs.mkdirSync(baseKeywordPath+'/train');
    }
  
    if (!fs.existsSync(baseKeywordPath+'/train/good')){
        fs.mkdirSync(baseKeywordPath+'/train/good');
    }
  
    if (!fs.existsSync(baseKeywordPath+'/train/bad')){
        fs.mkdirSync(baseKeywordPath+'/train/bad');
    }
  
    if (!fs.existsSync(baseKeywordPath+'/classified')){
        fs.mkdirSync(baseKeywordPath+'/classified');
    }
  
    if (!fs.existsSync(baseKeywordPath+'/classified/good')){
        fs.mkdirSync(baseKeywordPath+'/classified/good');
    }
  
    if (!fs.existsSync(baseKeywordPath+'/classified/bad')){
        fs.mkdirSync(baseKeywordPath+'/classified/bad');
    }
    
}



function worker_crawling(params) {
    id = params[0]
    keyword = params[1]
    count = params[2]
    const options = {checkInterval: 10000, minPing: 1, stopOnError: true};
    const callback = (err) => {
        console.error(err);
        //process.exit(1);
        db.setState(id, job.CRAWLING_FAIL().code);
    };
    console.log('Start Crawling... ', id, keyword, count)
    
    // console.log('!!!!!!!!!', job.IDLE().code);
    // console.log('!!!!!!!!!', job);
    
    //createFolders(keyword);
    db.setState(id, job.CRAWLING().code);

    return new Promise( r=> {
        console.log('Start:'+keyword);
        watchpuppy = new Watchpuppy(options, callback);
        if (count != undefined){
            crawler.crawl(keyword, count, watchpuppy, function(ret) {

                console.log('1 >>>>>>>>', ret);

                if(ret === 1){
                    db.setState(id, job.CRAWLING_DONE().code);
                    
                    console.log('CRAWLING Done:'+ keyword)
                    
                    setTimeout(function () {
                        r()
                    }, 1000)    
                }
                // db.setState(id, job.CRAWLING_DONE().code);
                
                // console.log('CRAWLING Done: '+ keyword)
                // synctoDB(id, keyword)
                // createFolders(keyword);

                // setTimeout(function () {
                //     r()                    
                // }, 1000)
            })            
        } else {
            crawler.crawl(keyword, undefined, watchpuppy, function(ret) {
                                
                if(ret === 1){
                    db.setState(id, job.CRAWLING_DONE().code);
                    
                    console.log('CRAWLING Done:'+ keyword)
                    
                    setTimeout(function () {
                        r()
                    }, 1000)    
                }
                // db.setState(id, job.CRAWLING_DONE().code);
                // console.log('CRAWLING Done:'+ keyword)
                // synctoDB(id, keyword)
                // createFolders(keyword);
                // setTimeout(function () {
                //     r()
                // }, 1000)
            })
        }
            
        
    })
    .then(r => {
        console.log("After process Crawling...");
        //console.log(r) // will not get executed
        if(watchpuppy !== undefined && watchpuppy !== null){
            watchpuppy.stop();
        }
                
        createFolders(keyword);
    })
    .catch(err => {
        console.error(err) // we will see what to do with it later
        return Promise.resolve(3)
    })
    .then(r => {
        synctoDB(id, keyword);
    })
    .catch(err => {
        // in case in the previous block occurs another error
        console.error(err)
    });
}

function updateImageClass(id, pathClassify){
    let classifiedFileList = _getAllFilesFromFolder(pathClassify);
    //console.log(classifiedFileList);

    classifiedFileList.forEach(row => {
        let strList = row.split('/');        
        if(strList[7] == 'good'){                    
            db.getImageIdByFilename(id, strList[8], function(imageid, imageFileName) {
                db.updateImageS1Class(imageid, imageFileName, 1, function(result) {
                    console.log('update done (G)', imageFileName);
                }); 
            });
        } else{
            db.getImageIdByFilename(id, strList[8], function(imageid, imageFileName) {
                db.updateImageS1Class(imageid, imageFileName, -1, function(result) {
                    console.log('update done (B)', imageFileName);
                }); 
            });
        }
    });
    
}

function _copyFile(source, target, cb) {
    var cbCalled = false;
  
    var rd = fs.createReadStream(source);
    rd.on("error", function(err) {
      done(err);
    });
    var wr = fs.createWriteStream(target);
    wr.on("error", function(err) {
      done(err);
    });
    wr.on("close", function(ex) {
      done();
    });
    rd.pipe(wr);
  
    function done(err) {
      if (!cbCalled) {
        cb(err);
        cbCalled = true;
      }
    }
}

function _moveImageFile(source, dest){
    _copyFile(source, dest, (err) => {
        if (err) {
            console.error(err);
            throw err;            
        } else {
            console.log(source, ' was copied to ', dest);
                   
            let path = null;
            if(source.indexOf("bad") !== -1){
                path = source.replace("bad", "good");
            } else if(source.indexOf("good") !== -1){
                path = source.replace("good", "bad");
            } else {
                if(dest.indexOf("bad") !== -1){
                    path = dest.replace("bad", "good");
                } else if(dest.indexOf("good") !== -1){
                    path = dest.replace("good", "bad");
                }   
            }

            if(fs.existsSync(path)) {
                console.log('del:'+ path);
                fs.unlink(path, function() {});
            }            
        }
        
    }); 
}

function _updateImageS1Class(id, filename, s1class){
    db.getImageIdByFilename(id, filename, function(imageid, imageFileName) {
        let val = null;
        if(s1class === "GOOD"){
            val = 1;
        } else if(s1class === "BAD"){
            val = -1;
        } else {
            val = 0;
        }
        db.updateImageS1Class(imageid, imageFileName, val, function(result) {
            console.log('update done (', s1class, ')', result);
        });
    });
}

function _setImageS1Class(row, id, tFilename, pathClassify, s1class, status){

    const start = async () => {
        db.getImageIdByFilename(id, tFilename, function(imageid, imageFileName) {
            let val = null;
            if(s1class === "good"){
                val = 1;
            } else if(s1class === "bad"){
                val = -1;
            } else {
                val = 0;
            }
            db.updateImageS1Class(imageid, imageFileName, val, function(result) {
                console.log('update done (', s1class, ')', result);
            });
        });
    
        if(status === job.CLASSIFYING_DONE().code){                                
    
            var source = row;
            var dest;
                
            if(s1class === "bad"){
                dest = row.replace("bad", "good");
            } else if(s1class === "good"){
                dest = row.replace("good", "bad");
            }            
            
            console.log('updateAllImageClass ::::: 1 ', source, dest)        
    
        } else if(status === job.CRAWLING_DONE().code){               
            var source = pathClassify + '/' + tFilename;
            var dest;
                        
            dest = source.replace(tFilename, "train/" + s1class + "/" + tFilename);        
    
            console.log('updateAllImageClass ::::: 2 ', source, dest)
        }
    
        console.log('Copy: ', source, dest)
        try{
            _moveImageFile(source, dest);
        } catch(e){
            console.error(e);
        }
        
    }

    start();
    

}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }

function updateAllImageClass(id, pathClassify, s1class, status){
    let classifiedFileList = _getAllFilesFromFolder(pathClassify);
    console.log("updateAllImageClass ::: ", status, pathClassify,  classifiedFileList);
    
    const start = async () => {
        await asyncForEach(classifiedFileList, async (row) => {
            let tFilename = row.split('/').pop();
    
            if(s1class === "good" || s1class === "bad"){                  
                _setImageS1Class(row, id, tFilename, pathClassify, s1class, status);        
            } else if(s1class === "none"){
                db.getImageIdByFilename(id, tFilename, function(imageid, imageFileName) {
                    db.updateImageS1Class(imageid, imageFileName, 0, function(result) {
                        console.log('update done (N)', result);
                    });
                });
    
                if(status === job.CLASSIFYING_DONE().code){
                    console.log("All status changed::: classifying done");
    
                    // change status to crawling done (201)
                    db.setState(id, job.CRAWLING_DONE().code);               
                
                } else if(status === job.CRAWLING_DONE().code){
                    console.log("All status changed::: crawling done");
                }
    
                console.log(row);
                if(row.indexOf('classified') !== -1 ||
                row.indexOf('train') !== -1) {
                    if(fs.existsSync(row)) {
                        console.log('del:'+row);
                        fs.unlink(row, function() {});
                    }
                }
            }
        });

        console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    }
    
    start();
  
    
}

function runPredic2([id, pathRoot, pathTrain, pathClassify, opt]){
    
    console.log('RUN PREDICT 2::: Start:'+pathRoot, pathClassify);

    if(opt === undefined){
        db.setState(id, job.CLASSIFYING().code);
    }
    
    
    trainer.predict2(pathRoot, pathTrain, pathClassify, opt, function(ret) {

        if(opt === "remove"){
            console.log(":::::::::::::::::RUN PREDICT REMOVE");
            db.setState(id, job.CROPPING_DONE().code);
        } else{

            console.log('!!!!!', ret);

            if(ret.type === -1){ // error      
                if(ret.errCode === -10 || ret.errCode === -11){
                    db.setState(id, job.CLASSIFYING_FAIL().code);
                } 
            } else if(ret.type === 1){ // good or bad
                //console.log(ret.imageStatus, ret.filename);
                _updateImageS1Class(id, ret.filename, ret.imageStatus);
            } else if(ret.type === 2){
                if(ret.exitCode === 0){
                    db.setState(id, job.CLASSIFYING_DONE().code);
                }
            }


        }
        
            
      
            
    })
    
}

function runPredict(id, pathRoot, pathTrain, pathClassify, opt){
    return new Promise( r=> {
        console.log('RUN PREDICT::: Start:'+pathRoot, pathClassify);

        if(opt === undefined){
            db.setState(id, job.CLASSIFYING().code);
        }
        
        
        trainer.predict(pathRoot, pathTrain, pathClassify, opt, function(ret) {

            if(opt === "remove"){
                console.log(":::::::::::::::::RUN PREDICT REMOVE");
                db.setState(id, job.CROPPING_DONE().code);
            } else{

                if(ret === -10 || ret === -11){
                    db.setState(id, job.CLASSIFYING_FAIL().code);
                } else{
                    db.setState(id, job.CLASSIFYING_DONE().code);
                    console.log('Done:'+ pathTrain)
                    
                    setTimeout(function () {
                        r()
                    }, 1000)
                }
                
            }
            
            
      
            
        })
    });
}

function runCropping(id){
    console.log('Start Cropping... IN runCropping', id);
    db.getKeywordbyid(id, function(keyword) {
        //var pathRoot = base + 'ait_data/'+ keyword +'/'
        var pathRoot = rootpath + keyword +'/'        
        var pathTrain = pathRoot + 'train'
        var savePath = pathRoot +'classified_cropped/'
        var pathBase = pathRoot +'classified/'

        //console.log('pathclassify ', savePath);
        addCropper(id, pathBase, savePath, pathTrain)
        //db.setState(id, 'wait for training')
        //res.redirect('datas');
    })
}

module.exports = {

    addTrainingPredictV2(id, pathRoot, pathTrain, pathClassify) {
        // console.log('Add Training', pathTrain);
        // var cmd = 'train'
        // ch_train.add([cmd, id, pathRoot, pathTrain, pathClassify]);
        // addPredict('predict', id, pathRoot, pathTrain, pathClassify);
        console.log('Add Train to predict', pathTrain);
        var cmd = 'train_predict2'
        ch_train.add([cmd, id, pathRoot, pathTrain, pathClassify]);    
    },

    addTrainingPredict(id, pathRoot, pathTrain, pathClassify) {
        // console.log('Add Training', pathTrain);
        // var cmd = 'train'
        // ch_train.add([cmd, id, pathRoot, pathTrain, pathClassify]);
        // addPredict('predict', id, pathRoot, pathTrain, pathClassify);
        console.log('Add Train to predict', pathTrain);
        var cmd = 'train_predict'
        ch_train.add([cmd, id, pathRoot, pathTrain, pathClassify]);    
    },

    addTraining(id, pathRoot, pathTrain, pathClassify, postAction) {
        // console.log('Add Training', pathTrain);
        // var cmd = 'train'
        // ch_train.add([cmd, id, pathRoot, pathTrain, pathClassify]);
        // addPredict('predict', id, pathRoot, pathTrain, pathClassify);
        console.log('Add Training', pathTrain);
        var cmd = 'train'
        ch_train.add([cmd, id, pathRoot, pathTrain, pathClassify]);

        var postCmd = "predict";
        if(postAction != "predict"){
            console.log('!!!!!!!!!!!!!!! predcit_cropping will start');
            postCmd = "predict_cropping";
        }
        this.addPredict(postCmd, id, pathRoot, pathTrain, pathClassify);
    },
    
    addPredict(cmd, id, pathRoot, pathTrain, pathClassify, opt) {
        // console.log('Add Predict', pathTrain);
        // //var cmd = 'predict'
        // pathClassify = pathClassify.slice(0, -1);
        // ch_train.add([cmd, id, pathRoot, pathTrain, pathClassify]);
        console.log('Add Predict', pathTrain);
    
        ch_train.add([cmd, id, pathRoot, pathTrain, pathClassify, opt]);
    },
    
    addCropper(id, basePath, savePath, pathTrain) {
        // console.log('Add Cropping Path : ', savePath);
        // var cmd = 'crop'
        // //ch_cropper.add([cmd, id, basePath, savePath]);
        // addPredict('simple_predict', id, basePath, pathTrain, savePath);
        //console.log('Add Cropping Path : ', savePath);
        var cmd = 'crop'
        ch_cropper.add([cmd, id, basePath, savePath]);
        //addPredict('simple_predict', id, basePath, pathTrain, savePath);
    },

    addCrawling(id, keyword) {
        console.log('Add Crawling:',id)
        ch_crawl.add([id, keyword])
    },
    
    copyFile(source, target, cb) {
        var cbCalled = false;
      
        var rd = fs.createReadStream(source);
        rd.on("error", function(err) {
          done(err);
        });
        var wr = fs.createWriteStream(target);
        wr.on("error", function(err) {
          done(err);
        });
        wr.on("close", function(ex) {
          done();
        });
        rd.pipe(wr);
      
        function done(err) {
          if (!cbCalled) {
            cb(err);
            cbCalled = true;
          }
        }
    },
    
    updateImageStatusAll(id, path, s1class, status){
        updateAllImageClass(id, path, s1class, status);
    },
    
}