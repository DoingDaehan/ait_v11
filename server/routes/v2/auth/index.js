const express = require('express');
const auth = express.Router();

const authCtrl = require('./auth.ctrl');
const userCtrl = require('./user.ctrl');

auth.get('/', authCtrl.hello);

auth.post('/', userCtrl.create);

auth.post('/login', authCtrl.login);

auth.get('/check', authCtrl.check);

auth.post('/logout', authCtrl.logout);

module.exports = auth;