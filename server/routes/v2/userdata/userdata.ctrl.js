
const express  = require('express');
const router   = express.Router();
const path = require('path');
const util     = require('routes/util');
const path_util = require('routes/path_util');
const prechecker = require('scripts/prechecker');
const db = require('scripts/database_mongo');
const dbDACA = require('scripts/database_mysql');
const fs = require('fs');
const jobManager = require('routes/ProcessStatusManager');
const job = jobManager();
const wt = require('worker-thread');
var url = require("url");

const TRAINSET_PATH = '/FoodJobs/User_Data/trainset/';
const MODEL_PATH = '/FoodJobs/User_Data/model/';
const PREDICT_PATH_BASE = '/FoodJobs/User_Data/';
const CLASSIFIED_PATH_BASE = '/FoodJobs/User_Data/classified/';
const USER_DATA_LIST_PATH = '/FoodJobs/User_Data/list/';

const Enum = require('enum');
const classifiedTypeEnum = new Enum({'FACE': 'face', 'FOOD': 'food', 'NONE_FOOD': 'nonefood'});

global.ch_userdata_train = wt.createChannel(worker_training, 1);
global.ch_userdata_predict = wt.createChannel(worker_predict, 1);
global.ch_userdata_download = wt.createChannel(worker_download, 1);

function makeClassifiedDirs(classifiedPath){
    if (!fs.existsSync(classifiedPath)){
        fs.mkdirSync(classifiedPath);
    }

    if (fs.existsSync(classifiedPath)){
        if (!fs.existsSync(classifiedPath + '/face')){
            fs.mkdirSync(classifiedPath + '/face');
        }

        if (!fs.existsSync(classifiedPath + '/nonefood')){
            fs.mkdirSync(classifiedPath + '/nonefood');
        }

        if (!fs.existsSync(classifiedPath + '/food')){
            fs.mkdirSync(classifiedPath + '/food');
        }
    }
}

function makeDownloadDirs(path){    
    if (!fs.existsSync(path)){
        fs.mkdirSync(path);
    }    
}

function getImagesByClassifiedType(id, userdata, type){
    let classifiedPath = path.join(CLASSIFIED_PATH_BASE, userdata.attribute.selectedDate, '/', type, '/'); 
    let classifiedImages = [];
    if (fs.existsSync(classifiedPath)) {
        
        fs.readdirSync(classifiedPath).forEach(file => {                    
            let mFilename = path.join(userdata.attribute.selectedDate, '/', type, '/', file);
            classifiedImages.push({filename: mFilename});
            db.updateImageForUserDataList(id, file, type, 0, userdata.attribute.selectedDate, function(userData){
                //console.log(userData);
            });
        });
        console.log('ClassifiedImages::', type, classifiedImages);        
    }

    return classifiedImages;
}

function runDownload(id, imageList, selectedDate, callback){
    return new Promise( r => {   
        const downPath = '/FoodJobs/User_Data/';
        //let dirpath = downPath + selectedDate;

        console.log(selectedDate)
        let downloadPath = path.join(downPath, selectedDate);
        console.log('RUN DOWNLOAD::: Start:', id, selectedDate, downloadPath);
        
        makeDownloadDirs(downloadPath);

        callback(imageList);
        // let i = 0;
        // imageList.forEach(o => {
        //     i++;               
        //     //console.log(i, ret.length);
        //     db.addImageIntoUserDataList(id, o.img_path, function(id, userData){
        //         //console.log(id, userData);
        //         prechecker.downloadExactFile(o.img_path, selectedDate, function(r){
        //             //console.log(r);
        //             db.setDownloadTrueByFilename(id, o.img_path, function(ret){
        //                 //console.log('DOWNLOADED', o.img_path);
        //                 if(i === imageList.length){
        //                     db.setStatusForUserDataList(id, job.DOWNLOADING_IMAGES_DONE().code);
        //                 }
        //             });
        //         });
        //     });
        // });
        
    });
}

function runPredict(id, selectedDate){
    return new Promise( r => {
        let predictPath = path.join(PREDICT_PATH_BASE, selectedDate, '/');
        console.log(selectedDate)
        let classifiedPath = path.join(CLASSIFIED_PATH_BASE, selectedDate);
        console.log('RUN PREDICT::: Start:', id, selectedDate, classifiedPath);
        
        makeClassifiedDirs(classifiedPath);

        prechecker.predict(predictPath, MODEL_PATH, classifiedPath, function(ret) {

            if(ret === -10 || ret === -11){                
                db.setStatusForUserDataList(id, job.CLASSIFYING_FAIL().code, ()=>{});
            } else if(ret === 1){                
                db.setStatusForUserDataList(id, job.CLASSIFYING_DONE().code, ()=>{                                       
                    db.getUserDataById(id, function(userdata){
                        let c = getImagesByClassifiedType(id, userdata, classifiedTypeEnum.FACE.value);
                        
                        db.setCountForUserDataList(id, classifiedTypeEnum.FACE.value, c.length);
                
                        c = getImagesByClassifiedType(id, userdata, classifiedTypeEnum.FOOD.value);
                        
                        db.setCountForUserDataList(id, classifiedTypeEnum.FOOD.value, c.length);
                
                        c = getImagesByClassifiedType(id, userdata, classifiedTypeEnum.NONE_FOOD.value);
                        
                        db.setCountForUserDataList(id, classifiedTypeEnum.NONE_FOOD.value, c.length);
                    });                    
                });
                console.log('Done:'+ selectedDate)
                
                r(id);                
            } else {
                console.log('Classifying...', selectedDate);
                db.setStatusForUserDataList(id, job.CLASSIFYING().code, ()=>{});
            }
        });
        
    });
}

function worker_training(params) {
    var cmd = params[0]
    var id = params[1]
    var pathRoot = params[2]
    var pathTrain = params[3]
    var selectedDate = params[4]

    console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', cmd);

    if( cmd.toString() == 'train') {
        return new Promise( r=> {
            console.log('Start:'+pathTrain);            
            db.setStatusForUserDataList(id, job.LEARNING().code, ()=>{});

            prechecker.train(function(ret) {                                
                if(ret === 0){
                    console.log('Training... '+ pathTrain, ret);
                } else if(ret === 1){
                    console.log('Training Done! : '+ pathTrain, ret);
                    db.setStatusForUserDataList(id, job.LEARNING_DONE().code, ()=>{});
                }                
                
                setTimeout(function () {
                    r()
                }, 1000)                
            });
        })
        .then(r => {
            console.log(r) 
        })
        .catch(err => {
            console.error(err) // we will see what to do with it later
            return Promise.resolve(3)
        });
    }    
}

function postProcessForDownload(id, imgPath, selectedDate){
    //console.log(i, ret.length);
    db.addImageIntoUserDataList(id, imgPath, function(id, userData){
        //console.log(id, userData);
        prechecker.downloadExactFile(imgPath, selectedDate, function(r){
            //console.log(r);
            db.setDownloadTrueByFilename(id, imgPath, function(ret){
                //console.log('DOWNLOADED', o.img_path);                
            });
        });
    });
}

function worker_download(params) {
    var cmd = params[0]
    var id = params[1]    
    var imageList = params[2]
    var selectedDate = params[3]

    console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! worker_download', cmd);
    //console.log(id, imageList, selectedDate);

    if( cmd.toString() == 'download') {        
        runDownload(id, imageList, selectedDate, (imageList) => {

            let i = 0;
            imageList.forEach(o => {
                i++;               
                postProcessForDownload(id, o.img_path, selectedDate);
                // //console.log(i, ret.length);
                // db.addImageIntoUserDataList(id, o.img_path, function(id, userData){
                //     //console.log(id, userData);
                //     prechecker.downloadExactFile(o.img_path, selectedDate, function(r){
                //         //console.log(r);
                //         db.setDownloadTrueByFilename(id, o.img_path, function(ret){
                //             //console.log('DOWNLOADED', o.img_path);
                //             if(i === imageList.length){
                //                 db.setStatusForUserDataList(id, job.DOWNLOADING_IMAGES_DONE().code);
                //             }
                //         });
                //     });
                // });
                if(i === imageList.length){
                    db.setStatusForUserDataList(id, job.DOWNLOADING_IMAGES_DONE().code);
                }
            });
        })
        .then(function(ret){
            if(ret === 1){
                console.log('Download Done!:'+ selectedDate, ret)
                // db.setFinishedForUserDataList(id, function(userData){
                //     console.log(userData);
                // });                
            } 
            
        });
    } else if(cmd.toString() == 'download_foodimage'){
        runDownload(id, imageList, selectedDate, (imageList) => {

            let i = 0;
            imageList.forEach(o => {
                i++;               
                postProcessForDownload(id, o.full_image_file, selectedDate);
                if(i === imageList.length){
                    db.setStatusForUserDataList(id, job.DOWNLOADING_IMAGES_DONE().code);
                }
            });
        })
        .then(function(ret){
            if(ret === 1){
                console.log('Download Done!:'+ selectedDate, ret)
                // db.setFinishedForUserDataList(id, function(userData){
                //     console.log(userData);
                // });
            } 
            
        });
    } 
}

function worker_predict(params) {
    var cmd = params[0]
    var id = params[1]    
    var selectedDate = params[2]

    //console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', cmd);

    if( cmd.toString() == 'predict') {        
        runPredict(id, selectedDate)
            .then(function(ret){
                if(ret === 1){
                    console.log('Predict Done!:'+ selectedDate, ret)
                    db.setFinishedForUserDataList(id, function(userData){
                        console.log(userData);
                    });
                } else if(ret === 0){
                    console.log('Predicting...', selectedDate, ret)
                }
                
            
                //updateImageClass(id, pathClassify);
            });
    }

    
}

function countFileLines(filePath){
    return new Promise((resolve, reject) => {
    let lineCount = 0;
    //console.log(filePath)
    fs.createReadStream(filePath)
      .on("data", (buffer) => {
        let idx = -1;
        lineCount--; // Because the loop will run once for idx=-1
        do {
          idx = buffer.indexOf(10, idx+1);
          lineCount++;
        } while (idx !== -1);
      }).on("end", () => {
        resolve(lineCount);
      }).on("error", reject);
    });
};

router.get('/get/list/', function(req, res, next){
    let id = req.query.id;
    let imageId = req.query.imageId;
    let type = req.query.type;
    
    db.getImagesByIdForPagination(id, imageId, type, 0, function(userdata){
        //console.log(userdata);
        res.status(200).json(userdata);
    });
});


router.get('/hello', function(req, res, next){
    let id = req.query.id;

    db.getUserDataById(id, function(userdata){
        let c = getImagesByClassifiedType(id, userdata, classifiedTypeEnum.FACE.value);
        
        db.setCountForUserDataList(id, classifiedTypeEnum.FACE.value, c.length);

        c = getImagesByClassifiedType(id, userdata, classifiedTypeEnum.FOOD.value);
        
        db.setCountForUserDataList(id, classifiedTypeEnum.FOOD.value, c.length);

        c = getImagesByClassifiedType(id, userdata, classifiedTypeEnum.NONE_FOOD.value);
        
        db.setCountForUserDataList(id, classifiedTypeEnum.NONE_FOOD.value, c.length);
    });
    
    res.status(200).json("ok hello");
});

router.get('/train', function(req, res, next){
    let id = req.query.id;
    
    db.getUserDataById(id, function(userdata){
        console.log(userdata);
        
        let cmd = 'train'
        ch_userdata_train.add([cmd, id, TRAINSET_PATH, userdata.attribute.selectedDate]);    
    });
    
    db.getUserDataListAll(function(list){
        console.log(list)        
        res.json(list);        
    });
});

router.get('/predict', function(req, res, next){
    let id = req.query.id;
    
    db.getUserDataById(id, function(userdata){
        console.log(userdata);
        
        let cmd = 'predict'
        ch_userdata_predict.add([cmd, id, userdata.attribute.selectedDate]);    
    });

    db.getUserDataListAll(function(list){
        //console.log(list)        
        res.json(list);        
    });
    
});

router.get('/getlist/all', util.isLoggedin, function(req, res, next){
    db.getUserDataListAll(function(list){
        //console.log(list)        
        res.json(list);        
    });
    // res.status(200).json('ok all');
});

router.get('/getlist/:type', function(req, res, next){
    let id = req.query.id;
    let type = req.params.type;

    //console.log(id, type);

    db.getUserDataById(id, function(userdata){
        if(userdata !== null){
            let classifiedPath = path.join(CLASSIFIED_PATH_BASE, userdata.attribute.selectedDate, '/', type, '/'); 
            let classifiedImages = [];
            
            if (fs.existsSync(classifiedPath)) {
                fs.readdirSync(classifiedPath).forEach(file => {                    
                    classifiedImages.push({filename: path.join(userdata.attribute.selectedDate, '/', type, '/', file)});
                });
                //console.log('ClassifiedImages::', type, classifiedImages);
                if(classifiedImages.length !== 0){
                    res.status(200).json(classifiedImages);
                } else{
                    res.status(204).json([]);
                } 
            } else{
                res.status(204).json([]);    
            }        
           
        } else{
            res.status(204).json([]);
        }
    });


});

router.post('/sensitive/confirm', function(req, res, next){
    let list = req.body.idList;
    let status = req.body.status;
    let type = req.body.type;
    let action = req.body.action;

    console.log('confirm!!!!!!!!! sensitive');
    console.log(list, status, type, action);

    console.log('sensitive', list);
    db.getSensitiveImages(list[0], function(ret){
        ret.forEach((e) => {
            console.log(e);
            let filename = e.filename.split('/');
            console.log(filename[2]);

            if(action == "foodimage"){
                dbDACA.getFoodImageByFilename(filename[2], function(results){
                    console.log(results);
                    results.forEach((o) => {
                        dbDACA.updateFoodImageWithDownflagByImgPath(o.full_image_file, 1, function(r, err){
                            if(err){
                                console.log(err);
                            }                        
                        });;
                    })
                    
                });
            } else {
                dbDACA.getEatHistoryByFilename(filename[2], function(results){
                    console.log(results);
                    results.forEach((o) => {
                        dbDACA.updateEatHistoryWithDownflagByImgPath(o.img_path, 2, function(r, err){
                            if(err){
                                console.log(err);
                            }                        
                        });;
                    })
                    
                });
            }            
            
        });
        
        
    //    res.status(200).json(ret);
    });
    

    res.status(200).json([]);
});

router.post('/sensitive/imagefood/confirm', function(req, res, next){
    let list = req.body.idList;
    let status = req.body.status;
    let type = req.body.type;

    console.log('confirm!!!!!!!!! sensitive');
    console.log(list, status, type);

    console.log('sensitive', list);
    db.getSensitiveImages(list[0], function(ret){
        ret.forEach((e) => {
            console.log(e);
            let filename = e.filename.split('/');
            console.log(filename[2]);

            dbDACA.getEatHistoryByFilename(filename[2], function(results){
                console.log(results);
                results.forEach((o) => {
                    // dbDACA.updateEatHistoryWithDownflagByImgPath(o.img_path, 2, function(r, err){
                    //     if(err){
                    //         console.log(err);
                    //     }                        
                    // });;
                })
                
            });
            
        });
        
        
    //    res.status(200).json(ret);
    });
    

    res.status(200).json([]);
});

router.get('/get/list/new/', util.isLoggedin, function(req, res, next){
    let selectedDate = req.query.startdate;
    let selectedEndDate = req.query.enddate;
    let id;
    let type = req.query.type;
    
    console.log('!!!', selectedDate, selectedEndDate, type);
    //TODO: collecting data from start date to end date
    db.getUserDataIdByDate(selectedDate, function(userdata){
        if(userdata === null){
            if(type == "foodimage"){
                dbDACA.getFilenameFromFoodImageByDate(selectedDate, function(ret){                
                    db.addUserDataList(selectedDate, ret.length, function(_id){
                        id = _id;
                        
                        db.setStatusForUserDataList(id, job.DOWNLOADING_IMAGES().code, function(userdata){
                                                        
                        });

                        db.getUserDataListAll(function(list){
                            //console.log(list)        
                            res.status(200).json(list);        
                        });

                        let cmd = 'download_foodimage'
                        ch_userdata_download.add([cmd, id, ret, selectedDate]);    
                    });
                });
            } else {
                dbDACA.getFilenameFromEatHistoryByDate(selectedDate, function(ret){                
                    db.addUserDataList(selectedDate, ret.length, function(_id){
                        id = _id;
                        
                        db.setStatusForUserDataList(id, job.DOWNLOADING_IMAGES().code, function(userdata){
                                                       
                        });
    
                        db.getUserDataListAll(function(list){
                            //console.log(list)        
                            res.json(list);        
                        });
    
                        let cmd = 'download'                        
                        ch_userdata_download.add([cmd, id, ret, selectedDate]);    
                    });
                });
            }
            
        } else{
            res.status(204).json([]);
        }
    });    
});


router.get('/getlist', util.isLoggedin, function(req, res, next){
    let selectedDate = req.query.startdate;
    let selectedEndDate = req.query.enddate;
    let id;

    console.log(selectedDate, selectedEndDate);
    // TODO: collecting data from start date to end date
    db.getUserDataIdByDate(selectedDate, function(userdata){

        if(userdata === null){        
            db.addNewUserDataList(selectedDate, selectedDate + ".txt", 0, function(_id){
                id = _id;
                
                db.setStatusForUserDataList(id, job.DOWNLOADING_FILE_LIST().code, function(userdata){
                    db.getUserDataListAll(function(list){
                        //console.log(list)        
                        res.json(list);        
                    });   
                });
            });
        
            prechecker.getlist(selectedDate, function(ret){
                
                if(ret === 1){                    
                    const mFolder = path.join(USER_DATA_LIST_PATH, selectedDate);
                    let readFile;
                    let readPath;
        
                    //console.log(mFolder)
                    fs.readdirSync(mFolder).forEach(file => {
                        
                        readFile = file;
                    });
        
                    readPath = path.join(USER_DATA_LIST_PATH, selectedDate, '/', readFile);
        
        
                    countFileLines(readPath)
                    .then(function(lineCount){
                        //console.log(lineCount);
                        db.setListCountForUserDataList(id, lineCount, function(userData){
                            
                            db.setStatusForUserDataList(id, job.DOWNLOADING_FILE_LIST_DONE().code);
                        });                
                    });
                }
            });
        }
    });
    
    
    
});

router.get('/download/id', util.isLoggedin, function(req, res, next){
    let id = req.query.id;
    db.getUserDataById(id, function(userData){
        
        if(userData.attribute.status === job.DOWNLOADING_FILE_LIST_DONE().code){
            db.setStatusForUserDataList(userData._id, job.DOWNLOADING_IMAGES().code, function(userdata){
                db.getUserDataListAll(function(list){
                    console.log(list)        
                    res.json(list);        
                });                
            });
            prechecker.download(userData.attribute.selectedDate, function(ret){        
                if(ret === 1){
                    db.getUserDataIdByDate(userData.attribute.selectedDate, function(userData){                        
                        db.setStatusForUserDataList(userData._id, job.DOWNLOADING_IMAGES_DONE().code);
                    });
                }
            });
            
        } else {
            res.status(201).json([]);
        }
        
    });
});


router.get('/download', function(req, res, next){
    let selectedDate = req.query.date;
    db.getUserDataIdByDate(selectedDate, function(userData){
        
        if(userData.attribute.status === job.DOWNLOADING_FILE_LIST_DONE().code){
            db.setStatusForUserDataList(userData._id, job.DOWNLOADING_IMAGES().code);
            prechecker.download(selectedDate, function(ret){        
                if(ret === 1){
                    db.getUserDataIdByDate(selectedDate, function(userData){                        
                        db.setStatusForUserDataList(userData._id, job.DOWNLOADING_IMAGES_DONE().code);
                    });        
                }
            });            
            res.status(200).json("ok download");
        } else {
            res.status(201).json("not ok");
        }
        
    });
});

router.get('/get/eathistory', function(req, res, next){
    let filename = req.query.filename;
    dbDACA.getEatHistoryByFilename(filename, function(results){
        console.log(results);
    });
    res.status(200).json("get eat history all !!");
});

router.get('/update/eathistory', function(req, res, next){
    let id;
    let val = req.query.val;
    let filename = req.query.filename;

    dbDACA.getEatHistoryByFilename(filename, function(results){
        console.log(results);

        if(results.length != 0){
            id = results[0].id;
            console.log(':::::', id);
            
            // dbDACA.updateEatHistoryWithDownflag(id, val, function(ret){
            //     console.log(ret);
            // });    
        }
        

    });
    res.status(200).json("update/eathistory !!");
});

router.get('/update/image/sensitive', function(req, res, next){
    let id = req.query.id;
    let imageId = req.query.imageId;
    let isSensitive = req.query.isSensitive;
    let type = req.query.type;

    console.log(id, imageId, isSensitive);
    
    db.getUserDataById(id, function(userdata){
        
        
        let mImageObj = userdata.images.filter((o) => {
            //console.log(o)
            //console.log(o._id, imageId);
            return o._id == imageId;
        });
        //console.log('>>>>', mImageObj[0].filename);

        let mParseFilename = mImageObj[0].filename.split('/');
        console.log(mParseFilename[2]);
        db.setSensitiveByImageId(id, imageId, isSensitive, function(ret){
            //console.log(ret);
            //res.status(200).json([]);
            db.getImagesByIdForPagination(id, imageId, type, 0, function(userdata){
                console.log(userdata);
                res.status(200).json(userdata);
            });
        });
        // dbDACA.getEatHistoryByFilename(mParseFilename[2], function(result){
        //     console.log(result);
        // });
    });
});

router.put('/update/image/sensitive/', function(req, res, next){
    let id = req.query.id;
    
    console.log('sensitive', id);
    db.getSensitiveImages(id, function(ret){
        ret.forEach((e) => {
            console.log(e);
            let filename = e.filename.split('/');
            console.log(filename[2]);

            dbDACA.getEatHistoryByFilename(filename[2], function(results){
                console.log(results);
            });
            // dbDACA.updateEatHistoryWithDownflagByImgPath(filename[2], query, function(r, err){
            //     if(err){
            //         console.log(err);
            //     }
                
            // });
        });
        
        
        res.status(200).json(ret);
    });
    
});

router.get('/get/image/sensitive', function(req, res, next){
    let id = req.query.id;
    
    console.log('Sensitive Images:::', id);
    
    db.getUserDataById(id, function(userdata){
                
        let mImageObj = userdata.images.filter((o) => {
            return o.isSensitive === true;
        });
        console.log('>>>>', mImageObj);

        res.status(200).json([]);
        // let mParseFilename = mImageObj[0].filename.split('/');        
        
        // dbDACA.getEatHistoryByFilename(mParseFilename[2], function(result){
        //     console.log(result);
        // });
    });
});



router.get(['/exist/refresh', '/api/datas/exist/refresh/:type'], util.isLoggedin, async function(req, res) {
    let type = req.query.type;
    console.log('full image!!!! append!!!', type);

    if(type === "append"){
    
        const existPath = path_util.getExistAppendPath();

        existFoods = getExistFiles(existPath);

        console.log('exist_append', existFoods);

        existFoods.forEach(function(foodName){
            db.retrieveFoodName(foodName.filename, function(food){
                
                if(food === null || food === undefined){
                    let foodPath = existPath + foodName.filename;
                    setFood(foodPath, (images) => {
                        db.addExistKeyword(
                            foodName.filename, 
                            foodName.filename, 
                            "exist_append", 
                            "KR", 
                            images,
                            req.decoded.username,
                            function(f){
                            console.log(f);
                        });
                    });
                } else{                        
                    console.log(">>> ", food._id);
                    // db.retrieveFood(food._id, function(f){
                    //     //console.log(f);
                    //     data.push(f);
                    // });   
                }                
            });
        });

        res.status(200).json(existFoods);     
    };

    
});

router.get(['/exist', '/exist/:type'], util.isLoggedin, async function(req, res) {
    let type = req.query.type;
    console.log(type);

    if(type === "append"){
    
        const existPath = path_util.getExistAppendPath();

        console.log(existPath);
        existFoods = getExistFiles(existPath);

        db.retrieveFoodsBySource("exist_append", function(data){

            res.json(data);
        })
        
    
    };

    
});


module.exports = router;