var express = require('express');
var router = express.Router();
const db = require('../../scripts/database_mongo');
const routerUtil = require('../router_util');
const path_util = require('../path_util');
const fs = require('fs-extra');
const sizeOf = require('image-size');
const jobManager = require('../ProcessStatusManager');
const job = jobManager();
var util     = require('routes/util');

router.get('/', function(req, res){
	res.send('Hello');		
});

function copyFile(src, dest, callback) {

    try {
        routerUtil.copyFile(src, dest, (err) => {
            if (err) console.error(err);
            console.log(src, ' was copied to ', dest);
        });        
        //fs.copySync(src, dest, callback);

    } catch(err){
        console.error("ENOENT:::", err);
    }
        
}

function getSrcPath(keyword, filename){    
    const rootpath = path_util.getDataPath() + '/';
    return rootpath + keyword + '/' + filename;
}

function getTrainPath(keyword, type, filename){
    return path_util.getFoodTrainPath(keyword) + '/' + type + '/' + filename;
}

function getCompletePath(keyword, filename){
    return path_util.getCompleteBasePath(keyword) + filename;
}

function getExistCompletePath(keyword, filename){
    return path_util.getExistAppendCompleteBasePath(keyword) + filename;
}


router.post('/starttraining', util.isLoggedin, function(req, res) {
    var id = req.body.trainid;
    res.json("start training");

    let _preprocessTrain = function(id){
        return new Promise(function (resolve, reject){
            db.getDataByIndex(id, function(food, err){
                if(err) reject(Error("error"));
                resolve(food);
            });
        });
    }

    _preprocessTrain(id)
    .then(function(food){        
        let keyword = food.attribute.keyword;
        console.log(keyword);

        let goodImages = food.image.filter((img) => img.s1class === 1);
        //console.log('GOOD IMAGES ++++', goodImages);

        goodImages.forEach(function(img) {
            let srcPath = getSrcPath(keyword, img.filename);
            let destPath = getTrainPath(keyword, 'good', img.filename);
            
            //console.log(srcPath, destPath);
            copyFile(srcPath, destPath);
        });
        
        return food;
    })
    .then(function(food){
        let keyword = food.attribute.keyword;
        let badImages = food.image.filter((img) => img.s1class === -1);
        //console.log('BAD IMAGES ++++', badImages);
        
        badImages.forEach(function(img) {
            let srcPath = getSrcPath(keyword, img.filename);
            let destPath = getTrainPath(keyword, 'bad', img.filename);
            
            //console.log(srcPath, destPath);
            copyFile(srcPath, destPath);
        });     
        
        return keyword;
    })
    .then(function(keyword){
        
        const rootpath = path_util.getDataPath() + '/';
        var pathRoot = rootpath + keyword +'/'        
        var pathTrain = path_util.getFoodTrainPath(keyword);
        var pathClassify = pathRoot +'classified'
        
        console.log(pathRoot, pathTrain, pathClassify)
        routerUtil.addTrainingPredictV2(id, pathRoot, pathTrain, pathClassify)
        
        db.setState(id, job.WAITING_LEARNING().code);
        //res.redirect('datas');
    });  
});

router.post('/data/imgstat', util.isLoggedin, function(req, res) {
    var id = req.body.id;
    var fn = req.body.file;
    let status = req.body.status;
    let imgType = req.body.imgtype;
    let type = 0;
    var filename = fn.split('/').pop();
    var mKeyword = "";
    let s1class = req.body.prevStatus;

    if(imgType === 'good'){
        type = 1;
    } else if(imgType === 'bad'){
        type = -1;
    } else if(imgType === 'none'){
        type = 0;
    }

    db.getImageIdByFilename(id, filename, function(imageid) {        
        db.updateImageS1Class(imageid, filename, type, function(result) {
            console.log('update done ', imgType, imageid, filename, result.length, status);
        }); 
    });
    
    let goodImages = [];
    let badImages = [];
    
    db.getImagelist(id, function(results) {
        
        results.forEach( row => {                    
            row.filename = mKeyword+'/' + row.filename;
            let tFileName = row.filename;
            
            if(row.s1class == 1){ // Good                        
                //row.filename = row.filename.replace('/', '/classified/good/');
                let newRow = JSON.parse(JSON.stringify(row));
                goodImages.push(newRow);
                row.filename = tFileName;
            } else if(row.s1class == -1){ // Bad                        
                //row.filename = row.filename.replace('/', '/classified/bad/');
                let newRow = JSON.parse(JSON.stringify(row));
                badImages.push(newRow);
                row.filename = tFileName;
            } 
        });

        //console.log(results, goodImages, badImages)
        let tFilename = fn.replace('bad', 'good');
        res.status(200).json({images: results, goodImages:goodImages, badImages:badImages, filename:tFilename});
    });
});

router.post('/data/good', util.isLoggedin, function(req, res) {
    var id = req.body.id;
    var fn = req.body.file;
    let status = req.body.status;
    var filename = fn.split('/').pop();
    var mKeyword = "";
    let s1class = req.body.prevStatus;

    db.getImageIdByFilename(id, filename, function(imageid) {        
        db.updateImageS1Class(imageid, filename, 1, function(result) {
            console.log('update done (G)', imageid, filename, result.length, status);
        }); 
    });
    
    let goodImages = [];
    let badImages = [];
    
    db.getImagelist(id, function(results) {
        
        results.forEach( row => {                    
            row.filename = mKeyword+'/' + row.filename;
            let tFileName = row.filename;
            
            if(row.s1class == 1){ // Good                        
                //row.filename = row.filename.replace('/', '/classified/good/');
                let newRow = JSON.parse(JSON.stringify(row));
                goodImages.push(newRow);
                row.filename = tFileName;
            } else if(row.s1class == -1){ // Bad                        
                //row.filename = row.filename.replace('/', '/classified/bad/');
                let newRow = JSON.parse(JSON.stringify(row));
                badImages.push(newRow);
                row.filename = tFileName;
            } 
        });

        //console.log(results, goodImages, badImages)
        let tFilename = fn.replace('bad', 'good');
        res.status(200).json({images: results, goodImages:goodImages, badImages:badImages, filename:tFilename});
    });
    
});

router.post('/data/none', util.isLoggedin, function(req, res) {
    var id = req.body.id;
    var fn = req.body.file;
    var filename = fn.split('/').pop();
    let status = req.body.status;
    let mKeyword = "";
    let s1class = req.body.prevStatus;

    console.log(fn);
    db.getImageIdByFilename(id, filename, function(imageid) {
        db.updateImageS1Class(imageid, filename, 0, function(result) {            
            console.log('update done (N)', imageid, filename, status);
        }); 
    });
    
    let goodImages = [];
    let badImages = [];
    
    db.getImagelist(id, function(results) {
        
        results.forEach( row => {                    
            row.filename = mKeyword+'/' + row.filename;
            let tFileName = row.filename;
            
            if(row.s1class == 1){ // Good                        
                //row.filename = row.filename.replace('/', '/classified/good/');
                let newRow = JSON.parse(JSON.stringify(row));
                goodImages.push(newRow);
                row.filename = tFileName;
            } else if(row.s1class == -1){ // Bad                        
                //row.filename = row.filename.replace('/', '/classified/bad/');
                let newRow = JSON.parse(JSON.stringify(row));
                badImages.push(newRow);
                row.filename = tFileName;
            } 
        });

        let tFileName = fn;
        console.log(tFileName);
        res.status(200).json({images: results, goodImages:goodImages, badImages:badImages, filename:tFileName});
    });
});

router.post('/data/bad', util.isLoggedin, function(req, res) {
    var id = req.body.id;
    var fn = req.body.file;
    var filename = fn.split('/').pop();
    let mKeyword = "";
    let status = req.body.status;
    let s1class = req.body.prevStatus;

    db.getImageIdByFilename(id, filename, function(imageid) {
        db.updateImageS1Class(imageid, filename, -1, function(result) {
            console.log('update done (B)', filename, status);
        }); 
    });

    let goodImages = [];
    let badImages = [];
    
    db.getImagelist(id, function(results) {
        
        results.forEach( row => {                    
            row.filename = mKeyword+'/' + row.filename;
            let tFileName = row.filename;
            
            if(row.s1class == 1){ // Good                        
                //row.filename = row.filename.replace('/', '/classified/good/');
                let newRow = JSON.parse(JSON.stringify(row));
                goodImages.push(newRow);
                row.filename = tFileName;
            } else if(row.s1class == -1){ // Bad                        
                //row.filename = row.filename.replace('/', '/classified/bad/');
                let newRow = JSON.parse(JSON.stringify(row));
                badImages.push(newRow);
                row.filename = tFileName;
            } 
        });

        //console.log(results, goodImages, badImages)
        let tFilename = fn.replace('good', 'bad');
        res.status(200).json({images: results, goodImages:goodImages, badImages:badImages, filename:tFilename});
    });
});

router.post('/datas', util.isLoggedin, function(req, res){
//router.post('/datas', function(req, res){
    var mState = Number(req.body.state);
    var mId = req.body.id;
    let mType = req.body.type;
    console.log('>>>>>>>>>>>>>>>>>11 State:', mState, 'ID: ', mId, 'Type: ', mType, req.decoded);


    if(mId != null) {
        
        db.getDataByIndex(mId, function(data) {
            if(data.length <= 0) {
                res.send('errors');
            }
        
            var keyword = data.attribute.keyword;            
            let goodImages = [];
            let badImages = [];            
            var dimensions = null;

            db.getImagelist(mId, function(results) {
                console.log(results);
                results.forEach( row => {
                    let rawFilename = row.filename;                
                    row.filename = keyword +'/' + row.filename;
                    //console.log(row.filename, keyword, row.filename)
                    //let tFileName = row.filename;
                    let tFilePath;
                    if(mType === 'append'){
                        tFilePath = path_util.getExistAppendPath() + keyword + '/' + rawFilename;                        
                        row.filename = '/foodlist/exist/' + row.filename;
                    } else{
                        tFilePath = path_util.getDataPathWithKeyword(keyword) + '/' + rawFilename
                    }

                    //let tFilePath = path_util.getExistAppendPath() + keyword + '/' + rawFilename;
                    console.log(tFilePath);
                    try{
                        dimensions = sizeOf(tFilePath);
                        //console.log(tFilePath, dimensions.width, dimensions.height);
                        if(dimensions.height < 300 && dimensions.width < 300){
                            console.log('Need to remove:: ', tFilePath, dimensions.width, dimensions.height);
                            // file remove

                            // db update
                        }
                    } catch(e){
                        console.error('catch error::', e.stack);
                    } finally {
                        
                    }                  
                 
                });
                                
                //console.log(goodImages);
                goodImages = results.filter((img) => img.s1class === 1);
                
                //console.log(badImages);
                badImages = results.filter((img) => img.s1class === -1);

                let cropped_images = [];
                console.log(mState);
                res.status(200).json({itemid: req.params.id, foodname: data.attribute.foodname, keyword: keyword, images: results, activation:data.attribute.is_activated, goodImages:goodImages, badImages:badImages, cropped_images:cropped_images});
                
            });
            
        });
    }
})

router.post(['/confirm'], util.isLoggedin, function(req, res) {    
    let id = req.body.idList[0];
    let status = req.body.status;
    let type = req.body.type;

    console.log('>>>>::: ', id, status, type, req.decoded);
  
    let _preprocessComplete = function(id){
        return new Promise(function (resolve, reject){
            db.getDataByIndex(id, function(food, err){
                if(err) reject(Error("error"));
                resolve(food);
            });
        });
    }

    _preprocessComplete(id)
    .then(function(food){        
        let keyword = decodeURI(food.attribute.keyword);
        console.log("!!!!!>>>>", keyword)
        let goodImages = food.image.filter((img) => img.s1class === 1);
        
        goodImages.forEach(function(img) {
            let srcPath;

            let destPath;
            let destKeywordBase;

            if(type === 'append'){
                srcPath = path_util.getExistAppendPath() + keyword + '/' + img.filename;
                
                destPath = getExistCompletePath(keyword, img.filename);
                destKeywordBase = path_util.getExistAppendCompletePath() + keyword;
            } else {
                srcPath = getSrcPath(keyword, img.filename);       
                destPath = getCompletePath(keyword, img.filename);
                destKeywordBase = path_util.getCompleteBasePath(keyword);
            }
            
            console.log("11181818",destKeywordBase);
            if(!fs.existsSync(decodeURI(destKeywordBase))){
                fs.mkdirSync(decodeURI(destKeywordBase));
            }
            
            console.log(srcPath, destPath);
            copyFile(srcPath, destPath);
        });
        
        return keyword;
    })   
    .then((keyword)=> {        
        let path;
        if(type === 'append'){
            path = path_util.getExistAppendPath() + keyword;
        } else {
            path = path_util.getDataPathWithKeyword() + keyword;
        }

        console.log("Will be REMOVED:::", path);
        path_util.removeFolderAsync(path);            
        return keyword;
    })
    .then((keyword)=> { 
        console.log("COMPLETED::: ", id, keyword, req.decoded.username);
        db.setDeactive(id, () => {
            console.log("SET DEACTIVED ", id);

            db.setUser(id, req.decoded.username, () => {
                console.log("SET USERNAME");
            });
        });
    });

});


module.exports = router;