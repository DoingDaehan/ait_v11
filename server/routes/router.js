const express = require('express');
const os = require('os');
const router = express.Router();
const routerUtil = require('routes/router_util');
const db = require('scripts/database_mongo');
const path_util = require('routes/path_util');
const fs = require('fs')
var sizeOf = require('image-size');
var util     = require('routes/util');
// const pump = require('pump')
//const rootpath = path_util.getBasePath() + path_util.getDataDirName() + '/';
const rootpath = path_util.getDataPath() + '/';
const jobManager = require('routes/ProcessStatusManager');

const job = jobManager();
const path = require('path');
const files = require('lib/files')


//fire a ping whenever you know things are running fine, e.g. somewhere in your stream processing
//watchpuppy.ping();

//you can modify the weight of a ping by passing in a number
//watchpuppy.ping(5); //equivalent to firing watchpuppy.ping() five times

//you can also invalidate a ping by passing in a negative number
//watchpuppy.ping(-1); //doesn't do anything

// const cors = require('cors');

// // var corsOptions = {
// //   origin: "http://192.168.0.23",
// //   optionSuccessStatus: 200
// // }

// router.all('*', cors());


const multer = require('multer');
// 기타 express 코드

//const upload = multer({ dest: 'uploads/', limits: { fileSize: 5 * 1024 * 1024 } });
const upload = multer({
  storage: multer.diskStorage({
    destination: function (req, file, cb) {
        let dest = path_util.getDataPath() + '/' + req.body.fromPath;
        //cb(null, 'uploads/');
        console.log('!!!!.....',dest);
        cb(null, dest);
    },
    filename: function (req, file, cb) {
    //console.log(file, file.originalname, file.mimetype);
        console.log(req.body.fromPath, req.body.oriFilename);
        console.log(path_util.getDataPath());
        //let extension = file.mimetype.split('/')[1];

        cb(null, req.body.oriFilename);
        //cb(null, new Date().valueOf() + '.' +extension);
    //   let extension = path.extname(file.mimetype);
    //   let basename = path.basename(file.originalname, extension);
    //   console.log(extension," ::: " , basename);
    //   cb(null, basename + "-" + Date.now() + extension);
    }
  }),
});

const upload2 = multer({
    storage: multer.diskStorage({
      destination: function (req, file, cb) {
          let dest = path_util.getKeywordPath();
          //cb(null, 'uploads/');
          console.log('!!!!.....', dest);
          cb(null, dest);
      },
      filename: function (req, file, cb) {                    
        cb(null, file.originalname);
      }
    }),
  });

router.get('/newfood', (req, res, next) => {
    res.redirect('datas');
});

router.get('/k', (req, res, next)=> {
    const execSync = require('child_process').execSync;
    // import { execSync } from 'child_process';  // replace ^ if using ES modules
    //const output = execSync('nvidia-smi', { encoding: 'utf-8' });  // the default is 'buffer'
    const output = execSync('ls /usr/local/cuda-10.0/lib64', { encoding: 'utf-8' });  // the default is 'buffer'
    //const output = execSync('env', { encoding: 'utf-8' });  // the default is 'buffer'
    console.log('Output was:\n', output);
    
    res.status(201).json("okay");
        
});

router.get('/t', (req, res, next) => {
    console.log('/TTTTT');
    watchpuppy.ping(5);
    res.status(201).json('ddd');
});

router.post('/up', upload.single('image'), (req, res) => {
  console.log(req.file); 
  res.status(200);
});


router.get('/jspaint', (req, res, next) => {    
    res.render(path.join(__dirname, '../', 'views/jspaint/index.html'));
});

// router.get("/test/socketio", (req, res) => {
//     res.send({ response: "I am alive" }).status(200);
// });

/* GET home page. */
router.get('/api/getUsername', util.isLoggedin, (req, res, next) => {    
    res.send({ username: os.userInfo().username });
});

router.post('/list/remove', util.isLoggedin, (req, res, next) => {
    let id = req.body.id;
    let status = req.body.status;

    console.log(id, status);
    db.retrieveFood(id, (food) => {
        let path = path_util.getDataPathWithKeyword(food.attribute.keyword);
        let keyword = food.attribute.keyword;
        console.log(path);
        
        db.deleteFood(id, (food) => {
            path_util.removeFolderAsync(path);
            res.status(200).json({keyword});
        });
    });
    

    
});

router.post('/api/removefile', util.isLoggedin, (req, res, next) => {
    let id = req.body.id;
    let path = req.body.path;

    console.log(id, path);
    db.retrieveFood(id, (food) => {
        let mPath = path_util.getDataPathWithKeyword(food.attribute.keyword) + path;
        let keyword = food.attribute.keyword;
        console.log(mPath, path);
        
        db.deleteFoodImage(id, path, (food) => {
            console.log(food);
        });

        // db.deleteFood(id, (food) => {
        //     path_util.removeFile(path);
        //     res.status(200).json({food});
        // });
    });
    

    
});


router.get('/api/test', (req, res, next) => {    
    //const jobManager = require('../../src/utils/ProcessStatusManager.js');

    //const job = jobManager.JobEnum;

    let s = job.CLASSIFYING_DONE().code;
    console.log(s);
});

router.get('/api/getfoodsall', util.isLoggedin, (req, res, next) => {
//    db.connect();   
    db.retrieveFoodSAll();
});

router.get('/api/getfood', util.isLoggedin, (req, res, next) => {
    console.log('!!!!', req.body.id);
    db.retrieveFood(req.body.id);
});


function cancelFood(id, callback){
    
    console.log(id);
    
    db.setState(id, job.CRAWLING_DONE().code, (food) => {
        console.log('cancelFood ::: ', food);
        let dest = path_util.getDataPathWithKeyword(food.attribute.keyword);            
        dest = path_util.getSrcTrainingPath(dest);
        let src = path_util.getTrainingPath() + food.attribute.keyword;
        let keyword = food.attribute.keyword;
        console.log(src, dest, keyword);
        callback(food);
        return food;
    });
    
    
    // return new Promise( (resolve, reject) => {                        
    //     console.log(id);
        
    //     db.setState(id, job.CRAWLING_DONE().code, (food) => {
    //         console.log('cancelFood ::: ', food);
    //         let dest = path_util.getDataPathWithKeyword(food.attribute.keyword);            
    //         dest = path_util.getSrcTrainingPath(dest);
    //         let src = path_util.getTrainingPath() + food.attribute.keyword;
    //         let keyword = food.attribute.keyword;
    //         console.log(src, dest, keyword);
    //         resolve([src, dest, keyword]);
    //     });
        
    // });
}

function getFoodUpdated(paths){
    return new Promise((resolve, reject) => {
        
        console.log("getFoodUpdated::: ", paths[0], paths[1], paths[2]);
        resolve();
    });
}

router.post('/cancel', util.isLoggedin, async (req, res, next) => {
    let id = req.body.id;
    console.log('cancel ::: ', req.body.id);
    
    let food = await cancelFood(id, function(food){
        console.log(food);
        res.status(200).json(food);
    });
    
    // cancelFood(id)
    // .then(getFoodUpdated)
    // .then(() => {
    //     res.status(200).json("ok");
    // });

    // .get('/v1/foodlist/', async (req, res, next) => {
    //     try {                    
    //         let data = await req.db.Food.find({"attribute.is_activated": true});
    //         res.status(200).json(data);      
    //     } catch(error) {
    //         next(error)
    //     }
    // })
    

});


router.get('/addnewfoodsource', util.isLoggedin, function(req, res) {
    var fileName = "worldfoodlist";
    db.getFoodSourceByFileName(fileName, function(result){
        if(result != undefined && result.length == 0){
            db.addNewFoodSource("WORLD", "WIKI", "worldfoodlist", function(){
                db.getFoodSources(function(result){
                    console.log('after getfoodsources',result);
                    res.writeHead(200, { "Content-Type": "application/json" });
                    res.end("Updated Successfully");
                });
            });            
        } else{
            res.writeHead(401, { "Content-Type": "application/json" });
            res.end("Updated Failed!");
        }        
    });
    
    
    
});

router.get('/foodscheck_detail', util.isLoggedin, function(req, res) {
    console.log('foodscheck_detail', req.query, req.params);
    if(req.query.id != null) {
                
        db.getDataByIndex(req.query.id, function(data) {
                        
            if(data.length <= 0) {
                res.render('errors');
            } else {
                console.log('getDataByIndex callback, ', data);
                
                var keyword = data[0].keyword;
             
                if(req.query.crawlId != null){
                    console.log(' if(req.query.crawlId != null){', req.query.crawlId);

                    let foodname;
                    let keyword;
                    db.getDataByIndex(req.query.crawlId, function(crawlData) {
                        foodname = crawlData[0].foodname;
                        keyword = crawlData[0].keyword;
        
                        if(crawlData[0].is_activated == 1){
                            db.addNewKeyword(foodname, keyword, function(id){
                                console.log('before addCrawling', id);
                                routerUtil.addCrawling(id, keyword);
                                
                                db.getImagelist(req.query.id, function(results) {
                                    results.forEach( row => {
                                        row.filename = data[0].keyword+'/' + row.filename
                                    });
                                    res.render('foodcheck_detail', {itemid: req.query.id, foodname: data[0].foodname, activation: data[0].is_activated, images: results});
                                })
                                //res.render('data', {itemid: req.params.id, foodname: data[0].foodname, keyword: keyword, images: results});
                            });
                        } else{
                            console.log('The item is deactivated!!!!', req.query.crawlId, crawlData.foodname);
                            db.getImagelist(req.query.id, function(results) {
                                results.forEach( row => {
                                    row.filename = data[0].keyword+'/' + row.filename
                                });
                                res.render('foodcheck_detail', {itemid: req.query.id, foodname: data[0].foodname, activation: data[0].is_activated, images: results});
                            })
                        }
                        
                    });
                                 
                } else {
                    db.getImagelist(req.query.id, function(results) {
                        results.forEach( row => {
                            row.filename = keyword+'/' + row.filename
                        });
                        res.render('foodcheck_detail', {itemid: req.query.id, foodname: data[0].foodname, activation: data[0].is_activated, images: results});
                    })
                    //res.render('data', {itemid: req.params.id, foodname: data[0].foodname, keyword: keyword, images: results});
                }
        

                
            }
        })
        
    } else{        
        db.getDatas(function(datas){
            res.render('foodscheck_detail', {datas: datas});
        });
    }
});

router.get('/cropimages', util.isLoggedin, function(req, res){
    
    if(req.query.id != null){
        console.log('!!!!!', req.query);
        db.getDataByIndex(req.query.id, function(data) {
            if(data.length <= 0) {
                res.render('errors');
            }

            var keyword = data[0].keyword;            
            let goodImages = [];
            let badImages = [];
            
            db.getImagelist(req.query.id, function(results) {
                results.forEach( row => {                    
                    row.filename = keyword+'/' + row.filename;
                    let tFileName = row.filename;
                    
                    if(row.s1class == 1){ // Good                        
                        //row.filename = row.filename.replace('/', '/classified/good/');
                        let newRow = JSON.parse(JSON.stringify(row));
                        goodImages.push(newRow);
                        row.filename = tFileName;
                    } else if(row.s1class == -1){ // Bad                        
                        //row.filename = row.filename.replace('/', '/classified/bad/');
                        let newRow = JSON.parse(JSON.stringify(row));
                        badImages.push(newRow);
                        row.filename = tFileName;
                    } 
                });
                // console.log(goodImages);
                // console.log(badImages);

                goodImages.forEach(item => {
                    item.filename = item.filename.replace('/', '/classified/good/');
                });

                badImages.forEach(item => {
                    item.filename = item.filename.replace('/', '/classified/bad/');
                });
                 
                // console.log('GOODIMAGES ', goodImages);
                // console.log('BADIMAGES', badImages);
                // console.log('RESULTS', results)

                const croppedPath = '../ait_data/' + keyword + '/classified_cropped/good';
                const fs = require('fs');
                let cropped_images = [];

                if (fs.existsSync(croppedPath)) {
                    fs.readdirSync(croppedPath).forEach(file => {                    
                        cropped_images.push({filename: keyword + '/classified_cropped/good/' + file});
                    });
                }
                

                console.log(cropped_images);
                res.render('data', {itemid: req.params.id, foodname: data[0].foodname, keyword: keyword, images: results, activation:data[0].is_activated, goodImages:goodImages, badImages:badImages, cropped_images:cropped_images});
            })
            
        })
        
        
    }
});

function getData(callback) {
    
    return new Promise(function(resolve, reject){
        db.getDatasWithActivation(function(datas){ 
           resolve(datas);     
        });
    });
    
}

function getExistFiles(path){
    
    //return new Promise( (resolve, reject)=> {
        let existFoods = [];    
        if (fs.existsSync(path)) {
            fs.readdirSync(path).forEach(file => {                    
                existFoods.push({filename: file});
            });
        }    
        return existFoods;
        //resolve(existFoods);
    //});
    


    
}

function setFood(foodPath, callback){
    //let foodPath = existPath + foodName.filename;
    console.log('.!!!', foodPath);
    let images = [];
    if (fs.existsSync(foodPath)) {
        fs.readdirSync(foodPath).forEach(file => {
            console.log("-----",file);
            if(file !== "Thumbs.db"){                           
                images.push(
                    {
                        filename: file,
                        s1class: 1,
                        s2class: 0
                    }
                );                
            }
        });
    }
    
    callback(images);
    //console.log(images);

    // db.addExistKeyword(
    //     foodName.filename, 
    //     foodName.filename, 
    //     "exist_append", 
    //     "KR", 
    //     images,
    //     function(f){
    //     console.log(f);
    // });
}

function insertExistFood(existFoods){
    
    return new Promise((resolve, reject) => {        
        existFoods.forEach(function(foodName){
            console.log('.....', foodName);
            db.retrieveFoodName(foodName.filename, function(food){
                let images = [];
                if(food === undefined){
                    setFood(foodName);
                } else{                        
                    console.log(">>> ", food._id);
                    db.retrieveFood(food._id, function(f){
                        console.log(f);
                        data.push(f);
                    });   
                }                
            });
        });
    });
}

router.get(['/exist/refresh', '/api/datas/exist/refresh/:type'], util.isLoggedin, async function(req, res) {
    let type = req.query.type;
    console.log('exist!!!! append!!!', type);

    if(type === "append"){
    
        const existPath = path_util.getExistAppendPath();

        existFoods = getExistFiles(existPath);

        console.log('exist_append', existFoods);

        existFoods.forEach(function(foodName){
            db.retrieveFoodName(foodName.filename, function(food){
                
                if(food === null || food === undefined){
                    let foodPath = existPath + foodName.filename;
                    setFood(foodPath, (images) => {
                        db.addExistKeyword(
                            foodName.filename, 
                            foodName.filename, 
                            "exist_append", 
                            "KR", 
                            images,
                            req.decoded.username,
                            function(f){
                            console.log(f);
                        });
                    });
                } else{                        
                    console.log(">>> ", food._id);
                    // db.retrieveFood(food._id, function(f){
                    //     //console.log(f);
                    //     data.push(f);
                    // });   
                }                
            });
        });

        res.status(200).json(existFoods);     
    };

    
});

router.get(['/exist', '/exist/:type'], util.isLoggedin, async function(req, res) {
    let type = req.query.type;
    console.log(type);

    if(type === "append"){
    
        const existPath = path_util.getExistAppendPath();

        console.log(existPath);
        existFoods = getExistFiles(existPath);

        db.retrieveFoodsBySource("exist_append", function(data){

            res.json(data);
        })
        
    
    };

    
});

router.get('/exist/replace', function(req, res) {
        
    let existImages = [];
    const existPath = path_util.getExistAppendPath();

    if (fs.existsSync(existPath)) {
        fs.readdirSync(existPath).forEach(file => {                    
            existImages.push({filename: keyword + '/classified_cropped/good/' + file});
        });
    }

    console.log(existImages);
    console.log('/api/datas/exist');

    res.status(200).json("ok");
    
});


router.post(['/api/getfoodlist'], function(req, res) {    
    let status = req.body.status;

    db.getDatasWithStatus(status, function(datas){            
        res.status(200).json(datas);
    });    
});

router.get(['/datas', '/datas/:id'], util.isLoggedin, function(req, res) {
//router.get(['/datas', '/datas/:id'], function(req, res) {
    console.log("/datas!!!!", req.params.id, req.headers['x-access-token'])

    if(req.params.id != null) {
        db.getDataByIndex(req.params.id, function(data) {
            if(data.length <= 0) {
                res.render('errors');
            }

            var keyword = data[0].keyword;
            var images = [];
            let goodImages = [];
            let badImages = [];
            let cropImages = [];

            db.getImagelist(req.params.id, function(results) {
                results.forEach( row => {                    
                    row.filename = keyword+'/' + row.filename;
                    let tFileName = row.filename;
                    
                    if(row.s1class == 1){ // Good                        
                        //row.filename = row.filename.replace('/', '/classified/good/');
                        let newRow = JSON.parse(JSON.stringify(row));
                        goodImages.push(newRow);
                        row.filename = tFileName;
                    } else if(row.s1class == -1){ // Bad                        
                        //row.filename = row.filename.replace('/', '/classified/bad/');
                        let newRow = JSON.parse(JSON.stringify(row));
                        badImages.push(newRow);
                        row.filename = tFileName;
                    } 
                });
                // console.log(goodImages);
                // console.log(badImages);

                goodImages.forEach(item => {
                    item.filename = item.filename.replace('/', '/classified/good/');
                });

                badImages.forEach(item => {
                    item.filename = item.filename.replace('/', '/classified/bad/');
                });

                const croppedPath = '../ait_data/' + keyword + '/classified_cropped/good';
                const fs = require('fs');
                let cropped_images = [];

                if (fs.existsSync(croppedPath)) {
                    fs.readdirSync(croppedPath).forEach(file => {                    
                        cropped_images.push({filename: keyword + '/classified_cropped/good/' + file});
                    });
                }
                console.log(cropped_images);
                res.render('data', {itemid: req.params.id, foodname: data[0].foodname, keyword: keyword, images: results, activation:data[0].is_activated, goodImages:goodImages, badImages:badImages, cropped_images:cropped_images});
            })
            
        })
    }else{
        db.getDatasWithActivation(req.decoded.username, function(datas){            
            
                res.status(200).json(datas);            
            
        });
    }
});

router.post(['/api/getfoodlist'], function(req, res) {    
    let status = req.body.status;

    db.getDatasWithStatus(status, function(datas){            
        res.status(200).json(datas);
    });    
});

function confirmFood(id, status){
    return new Promise( (resolve, reject) => {                        
        console.log(id, status);
        let s = parseInt(status);

        db.setState(id, job.READY_FOR_TRAINING().code, (food) => {            
            let src, dest;
            if(s === job.CROPPING_DONE().code){
                src = path_util.getDataPathWithKeyword(food.attribute.keyword);            
                console.log("!!!! >>>> ", food.attribute.keyword, src, dest);
                src = path_util.getSrcTrainingPath(src);
                console.log("!!!! >>>> ", food.attribute.keyword, src, dest);
                dest = path_util.getTrainingPath() + food.attribute.keyword;
                console.log("!!!! >>>> ", food.attribute.keyword, src, dest);
            } else if(s === job.CRAWLING_DONE().code){
                src = path_util.getDataPathWithKeyword(food.attribute.keyword);            
                console.log("!!!! 2 >>>> ", food.attribute.keyword, src, dest);
                src = path_util.getDataPathWhenComplete(src);
                console.log("!!!! 2 >>>> ", food.attribute.keyword, src, dest);
                dest = path_util.getTrainingPath() + food.attribute.keyword;
                console.log("!!!! 2 >>>> ", food.attribute.keyword, src, dest);
            } else if(s === job.CLASSIFYING_DONE().code){
                src = path_util.getClassifiedPathWithKeyword(food.attribute.keyword);            
                console.log("!!!! 3 >>>> ", food.attribute.keyword, src, dest);
                
                dest = path_util.getTrainingPath() + food.attribute.keyword;
                console.log("!!!! 3 >>>> ", food.attribute.keyword, src, dest);
            }
            
            let keyword = food.attribute.keyword;
            
            resolve([src, dest, keyword]);
        });
        
    });
}

function cancelConfirmedFood(id){
    return new Promise( (resolve, reject) => {                        
        console.log(id);
        
        db.setState(id, job.READY_FOR_CONFIRM().code, (food) => {
            console.log('cancelConfirmedFood ::: ', food);
            let dest = path_util.getDataPathWithKeyword(food.attribute.keyword);            
            dest = path_util.getSrcTrainingPath(dest);
            let src = path_util.getTrainingPath() + food.attribute.keyword;
            let keyword = food.attribute.keyword;
            resolve([src, dest, keyword]);
        });
        
    });
}

function moveFood(paths){
    return new Promise((resolve, reject) => {
        // console.log("moveFood::", food._id);
        // console.log(food);
        
        console.log("moveFood::: ", paths[0], paths[1], paths[2]);
                
        path_util.moveFolderAsync(paths[0], paths[1], () => {
            console.log("TEMP REMOVE 1")
            //path_util.removeFolderAsync(paths[0]);
        });
        
        resolve([paths[0], paths[2]]);
    });
}

router.post(['/api/confirm'], util.isLoggedin, function(req, res) {    
    let idList = req.body.idList;
    let status = req.body.status;
    console.log(req);

    console.log('>>>>:ssss:: ', idList, status, req.decoded);
  
    // idList.forEach((id) => {
    //     confirmFood(id, status)
    //     .then(moveFood).catch(err => {
    //         console.error(err) // we will see what to do with it later            
    //     })        
    //     .then((param)=> {
    //         console.log(param[1]);
    //         let path = path_util.getDataPathWithKeyword(param[1]);
    //         console.log("TEMP REMOVE 2")
    //         //path_util.removeFolderAsync(path);            

    //         return 1;
    //     })
    //     .then((ret)=> { 
    //         console.log("confirm ", ret, id);           
    //         db.setDeactive(id, () => {
    //             console.log("SET DEACTIVED ", id);
    //         })
    //     });
    // });   

});

router.post(['/api/cancelconfirmed'], function(req, res) {    
    let id = req.body.id;
    let status = req.body.status;

    console.log('>>>>::: ', id, status);
      
    cancelConfirmedFood(id)
        .then(moveFood).catch(err => {
            console.error(err) // we will see what to do with it later
            //return Promise.resolve(3)
            //resolve(moveFood);
            
        });
    

});

router.get(['/api/v1/datas', '/api/v1/datas/:id'], function(req, res) {
    var mState = Number(req.query.state);
    var mId = req.params.id;
    console.log('>>>>>>>>>>>>>>>>> State:', mState, 'ID: ', mId);


    if(mId != null) {
        
        db.getDataByIndex(mId, function(data) {
            if(data.length <= 0) {
                res.send('errors');
            }
        
            var keyword = data.attribute.keyword;            
            let goodImages = [];
            let badImages = [];            
            var dimensions = null;

            db.getImagelist(mId, function(results) {
        
                results.forEach( row => {
                    let rawFilename = row.filename;                
                    row.filename = keyword +'/' + row.filename;
                    console.log(row.filename, keyword, row.filename)
                    //let tFileName = row.filename;
                    let tFilePath = path_util.getDataPathWithKeyword(keyword) + '/' + rawFilename;
                    //let tFilePath = path_util.getExistAppendPath() + keyword + '/' + rawFilename;
                    console.log(tFilePath);
                    try{
                        dimensions = sizeOf(tFilePath);
                        //console.log(tFilePath, dimensions.width, dimensions.height);
                        if(dimensions.height < 300 && dimensions.width < 300){
                            console.log('Need to remove:: ', tFilePath, dimensions.width, dimensions.height);
                            // file remove

                            // db update
                        }
                    } catch(e){
                        console.error('catch error::', e.stack);
                    } finally {

                        
                    }
                    

                    if(row.s1class == 1){ // Good                        
                        //row.filename = row.filename.replace('/', '/classified/good/');
                        let newRow = JSON.parse(JSON.stringify(row));
                        goodImages.push(newRow);
                        //row.filename = tFileName;
                    } else if(row.s1class == -1){ // Bad                        
                        //row.filename = row.filename.replace('/', '/classified/bad/');
                        let newRow = JSON.parse(JSON.stringify(row));
                        badImages.push(newRow);
                        //row.filename = tFileName;
                    } 
                });
                                
                //console.log(goodImages);
                
                //console.log(badImages);
                        
                console.log(mState);
                //if(mState === "Classifying done"){                    
                if(mState === job.CLASSIFYING_DONE().code){
                    
                    goodImages.forEach(item => {                        
                        item.filename = item.filename.replace('/', '/classified/good/');
                    });
    
                    badImages.forEach(item => {
                        item.filename = item.filename.replace('/', '/classified/bad/');
                    });
                     
                    console.log(badImages);

                    res.status(200).json({itemid: req.params.id, foodname: data.attribute.foodname, keyword: keyword, images: "", activation:data.attribute.is_activated, goodImages:goodImages, badImages:badImages});
                } else if((mState === job.CROPPING_DONE().code) ||
                        (mState === job.READY_FOR_CONFIRM().code) ||
                        (mState === job.READY_FOR_TRAINING().code)) {
                    console.log('11111huf9h3r89herg');
                    //const croppedPath = '../ait_data/' + keyword + '/classified_cropped/good';
                    const croppedPath = path_util.getDataPathWithKeywordCropped(keyword);
                    const fs = require('fs');
                    let cropped_images = [];
    
                    if (fs.existsSync(croppedPath)) {
                        fs.readdirSync(croppedPath).forEach(file => {                    
                            cropped_images.push({filename: keyword + '/classified_cropped/good/' + file});
                        });
                    }

                    console.log(cropped_images);

                    res.status(200).json({itemid: req.params.id, foodname: data.attribute.foodname, keyword: keyword, images: results, activation:data.attribute.is_activated, goodImages:goodImages, badImages:badImages, cropped_images:cropped_images});
                } else{                    
                    //res.status(200).send({itemid: req.params.id, foodname: data[0].foodname, keyword: keyword, images: results, activation:data[0].is_activated, goodImages:goodImages, badImages:badImages, cropped_images:cropped_images});
                    res.status(200).json({itemid: req.params.id, foodname: data.attribute.foodname, keyword: keyword, images: results, activation:data.attribute.is_activated, goodImages:goodImages, badImages:badImages});
                }
                
                
            })
            
        })
    }else{
        let index = 3; // Manual DB Source
        db.getDatasWithIndexAndActivation(index, function(data){            
            res.status(200).json({data: data});            
            //res.send('datas', {datas: datas});
        });
    }
});

router.get(['/convertfoodname', '/convertfoodname/:id'], function(req, res) {
    if(req.params.id != null) {

        var path = data_dir + '/'
        var fileName = 'worldfoodlist'
        
        console.log(req.params)
        var startIndex = req.params.id
        var endIndex = startIndex + 10
        
        //files.loadFoodNames(path + fileName, startIndex, endIndex, function(result){
        files.loadFoodNamesFull(path + fileName, function(result){
            result.forEach(function(element){
                db.addNewKeyword(element.foodname, element.foodname, function(id){
                    console.log('added ', id, element.foodname);
                });    
            });
        });
        //console.log(foodName)
                
        
        res.writeHead(200, { "Content-Type": "application/json" });
        res.end("Updated Successfully");
      
    }else{
        var path = data_dir + '/'
        var fileName = 'worldfoodlist'

        db.getFoodSourceByFileName(fileName, function(source){
            
            files.loadFoodNamesFull(path + fileName, function(result){
                result.forEach(function(element){
                    console.log('!!!!!!!!!!!!!!!!!!', source, source.index);    
                    db.addNewKeywordWithFoodSource(element.foodname, element.foodname, source.index, function(id){
                        console.log('added ', id, element.foodname, source.index, source.file_name);
                    });    
                });
            });
            //console.log(foodName)
                    
            
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end("Updated Successfully");
        });
        
        
        
    }
});

router.get(['/foodcheck', '/foodcheck/:data'], function(req, res) {
    if(req.params.startId != null) {

        var path = data_dir + '/';
        var fileName = 'worldfoodlist';
        var data = req.params.data;
        var startIndex = data.startId;
        var endIndex = data.endId;
        
        //db.getNeedCheckFoods(function(results){
        db.getStates(startIndex, endIndex, function(results){
            var ret = [];
            ret = ret.push(results[0]);
            res.render('foodcheck', {datas: ret});
        });
     
    }else{

        // var path = data_dir + '/';
        // var fileName = 'generic';
        // var data = req.params.data;
        // var startIndex = 1;
        // var endIndex = 109;
        
        // //db.getNeedCheckFoods(function(results){
        // db.getStates(startIndex, endIndex, function(results){
        //     var ret = [];
        //     ret = ret.push(results[0]);
        //     res.render('foodscheck', {datas: ret});
        // });
                
        var promise = new Promise(function(resolve, reject) {
            var ret = [];
            db.getFoodSources(function(result){
                //console.log(result);                
                result.forEach(value => {
                    console.log(value);
                    db.getCount(value.index, function(obj){                    
                        value.count = obj;                    
                        ret.push(value);                                                           
                        
                        if(ret.length == result.length){
                            resolve(ret);
                            console.log('1', ret);
                        }
                        
                    })
                });                
            });
            
            
        });

        promise.then(function(ret){
            console.log(ret);
            res.render('foodscheck', {datas: ret});
        });
      
    }
});

router.post('/foodcheck/activation', function(req, res) {
    var id = req.body.id;

    db.setActivationStateFood(1, id, function(result){
        console.log(result);
    });

    res.writeHead(200, { "Content-Type": "application/json" });
    res.end("Updated Successfully");
});

router.post('/foodcheck/deactivation', function(req, res) {
    var id = req.body.id;

    db.setActivationStateFood(0, id, function(result){
        console.log(result);
    });

    res.writeHead(200, { "Content-Type": "application/json" });
    res.end("Updated Successfully");
});

router.post('/data/all', function(req, res){
    let id = req.body.id;
    let s1class = req.body.s1class;
    let status = req.body.status;
    
    db.getKeywordbyid(id, function(keyword) {
    
        var pathRoot = rootpath + keyword +'/'        
        var pathTrain = pathRoot + 'train'
        var pathClassify = null;

        if(status === job.CRAWLING_DONE().code){
            pathRoot = rootpath + keyword;
            pathClassify = pathRoot;
        } else if(status === job.CLASSIFYING_DONE().code){
            pathClassify = pathRoot +'classified';
        } else{
            console.log("/data/all pathClassify NULL");
            return ;
        }
        

        console.log("/data/all ::: ", s1class, status);
        routerUtil.updateImageStatusAll(id, pathClassify, s1class, status);
        if(s1class == "good"){
            console.log("GOOD");
        } else if(s1class == "bad"){
    
            console.log("BAD");
        } else {
            // none
            console.log("NONE");
        }        
    });

});

router.post('/data/good', function(req, res) {
    var id = req.body.id;
    var fn = req.body.file;
    let status = req.body.status;
    var filename = fn.split('/').pop();
    var mKeyword = "";
    let s1class = req.body.prevStatus;

    db.getImageIdByFilename(id, filename, function(imageid) {        
        db.updateImageS1Class(imageid, filename, 1, function(result) {
            console.log('update done (G)', imageid, filename, result.length, status);
        }); 
    });

    db.getKeywordbyid(id, function(keyword) {
        var source = rootpath + fn;
        var dest;

        if(source.indexOf("bad") != -1){
            dest = source.replace("bad", "good");
        } else if(source.indexOf("none") != -1){
            dest = source.replace("none", "good");
        } else if(source.indexOf("good") != -1){
            console.log("same file", source, dest);
            return;
        } else{
            dest = rootpath + keyword + '/train/good/' + filename;
            
            // return;
        }

        console.log('::::: ', source, dest, s1class)
        mKeyword = keyword;
        console.log('Copy: ', source, dest)

        //fs.createReadStream(source).pipe(fs.createWriteStream(dest));

        // var fs = require('fs-extra');
        // fs.copySync(source, dest);

        try {
            routerUtil.copyFile(source, dest, (err) => {
                if (err) throw err;
                console.log(source, ' was copied to ', dest);
            });
            // fs.copyFile(source, dest, (err) => {
            //     if (err) throw err;
            //     console.log(source, ' was copied to destination.txt');
            // });
    
            //var bad = rootpath + keyword + '/train/bad/' + filename;
            var bad = source;
            if(bad.indexOf('train') != -1 || bad.indexOf('classified') != -1) {
                if(fs.existsSync(bad)) {
                    console.log('del:'+bad);
                    fs.unlink(bad, function() {});
                }
            }    
        } catch(err){
            console.error("ENOENT:::", err);
        }
        
        

    });
    
    let goodImages = [];
    let badImages = [];
    
    db.getImagelist(id, function(results) {
        
        results.forEach( row => {                    
            row.filename = mKeyword+'/' + row.filename;
            let tFileName = row.filename;
            
            if(row.s1class == 1){ // Good                        
                //row.filename = row.filename.replace('/', '/classified/good/');
                let newRow = JSON.parse(JSON.stringify(row));
                goodImages.push(newRow);
                row.filename = tFileName;
            } else if(row.s1class == -1){ // Bad                        
                //row.filename = row.filename.replace('/', '/classified/bad/');
                let newRow = JSON.parse(JSON.stringify(row));
                badImages.push(newRow);
                row.filename = tFileName;
            } 
        });

        //console.log(results, goodImages, badImages)
        let tFilename = fn.replace('bad', 'good');
        res.status(200).json({images: results, goodImages:goodImages, badImages:badImages, filename:tFilename});
    });
    // res.writeHead(200, { "Content-Type": "application/json" });
    // res.end("Updated Successfully");
});

router.post('/data/none', function(req, res) {
    var id = req.body.id;
    var fn = req.body.file;
    var filename = fn.split('/').pop();
    let status = req.body.status;
    let mKeyword = "";
    let s1class = req.body.prevStatus;

    console.log(fn);
    db.getImageIdByFilename(id, filename, function(imageid) {
        db.updateImageS1Class(imageid, filename, 0, function(result) {            
            console.log('update done (N)', imageid, filename, status);
        }); 
    });

    // db.getKeywordbyid(id, function(keyword) {
    //     var bad = rootpath + keyword + '/train/bad/' + filename;
    //     if(fs.existsSync(bad)) {
    //         console.log('del:'+bad);
    //         fs.unlink(bad, function() {});
    //     }

    //     mKeyword = keyword;
    //     var good = rootpath + keyword + '/train/good/' + filename;
    //     if(fs.existsSync(good)) {
    //         console.log('del:'+good);
    //         fs.unlink(good, function() {});
    //     }
    // });

    db.getKeywordbyid(id, function(keyword) {
        var source = rootpath + fn;
        var dest;

        console.log('::::: ', source, dest, s1class, filename)
        
        if(job.CRAWLING_DONE().code === status){
            if(s1class == -1){
                dest = source.replace(keyword + "/train/bad", keyword);
            } else if(s1class == 1){
                dest = source.replace(keyword + "/train/good", keyword);
            } else{
                //dest = rootpath + keyword + '/train/good/' + filename;                
                return;
            }
        }
        
        
        mKeyword = keyword;
        console.log('Copy: ', source, dest)

        //fs.createReadStream(source).pipe(fs.createWriteStream(dest));

        // var fs = require('fs-extra');
        // fs.copySync(source, dest);
    
        try {
            routerUtil.copyFile(source, dest, (err) => {
                if (err) throw err;
                console.log(source, ' was copied to ', dest);
            });
            // fs.copyFile(source, dest, (err) => {
            //     if (err) throw err;
            //     console.log(source, ' was copied to destination.txt');
            // });

            //var bad = rootpath + keyword + '/train/bad/' + filename;
            var bad = source;
            if(bad.indexOf('train') != -1 || bad.indexOf('classified') != -1) {
                if(fs.existsSync(bad)) {
                    console.log('del:'+bad);
                    fs.unlink(bad, function() {});
                }
            }
        } catch(err){
            console.log(err);
        }

    });

    //console.log(id, fn, filename)

    // res.writeHead(200, { "Content-Type": "application/json" });
    // res.end("Updated Successfully");
    // let goodImages = [];
    // let badImages = [];
    
    // db.getImagelist(id, function(results) {
        
    //     results.forEach( row => {                    
    //         row.filename = mKeyword+'/' + row.filename;
    //         let tFileName = row.filename;
            
    //         if(row.s1class == 1){ // Good                        
    //             //row.filename = row.filename.replace('/', '/classified/good/');
    //             let newRow = JSON.parse(JSON.stringify(row));
    //             goodImages.push(newRow);
    //             row.filename = tFileName;
    //         } else if(row.s1class == -1){ // Bad                        
    //             //row.filename = row.filename.replace('/', '/classified/bad/');
    //             let newRow = JSON.parse(JSON.stringify(row));
    //             badImages.push(newRow);
    //             row.filename = tFileName;
    //         } 
    //     });

    //     //console.log(results, goodImages, badImages)
    //     let tFilename = null;
    //     if(fn.indexOf('bad') !== -1){
    //         tFilename = fn.replace('bad', 'none')
    //     } else if(fn.indexOf('good') !== -1){
    //         tFilename = fn.replace('good', 'none')
    //     }
    //     res.status(200).json({images: results, goodImages:goodImages, badImages:badImages, filename:tFilename});
    // });
    let goodImages = [];
    let badImages = [];
    
    db.getImagelist(id, function(results) {
        
        results.forEach( row => {                    
            row.filename = mKeyword+'/' + row.filename;
            let tFileName = row.filename;
            
            if(row.s1class == 1){ // Good                        
                //row.filename = row.filename.replace('/', '/classified/good/');
                let newRow = JSON.parse(JSON.stringify(row));
                goodImages.push(newRow);
                row.filename = tFileName;
            } else if(row.s1class == -1){ // Bad                        
                //row.filename = row.filename.replace('/', '/classified/bad/');
                let newRow = JSON.parse(JSON.stringify(row));
                badImages.push(newRow);
                row.filename = tFileName;
            } 
        });

        //console.log(results, goodImages, badImages)
        //let tFilename = fn.replace('bad', 'good');
        let tFileName = fn;
        console.log(tFileName);
        res.status(200).json({images: results, goodImages:goodImages, badImages:badImages, filename:tFileName});
    });
});

router.post('/data/bad', function(req, res) {
    var id = req.body.id;
    var fn = req.body.file;
    var filename = fn.split('/').pop();
    let mKeyword = "";
    let status = req.body.status;
    let s1class = req.body.prevStatus;

    db.getImageIdByFilename(id, filename, function(imageid) {
        db.updateImageS1Class(imageid, filename, -1, function(result) {
            console.log('update done (B)', filename, status);
        }); 
    });

    db.getKeywordbyid(id, function(keyword) {
        var source = rootpath + fn;
        var dest;
        if(source.indexOf("good") != -1){
            dest = source.replace("good", "bad");
        } else if(source.indexOf("none") != -1){
            dest = source.replace("none", "bad");
        } else if(source.indexOf("bad") != -1){
            console.log("same file", source, dest);
            return;
        } else {
            dest = rootpath + keyword + '/train/bad/' + filename;
        }
        
        mKeyword = keyword;
        console.log('Copy: ', source, dest, s1class)
        //fs.copyFile(source, dest, function() {});
        try{
            routerUtil.copyFile(source, dest, (err) => {
                // if (err) {
                //     throw err;
                // }
                console.log(source, ' was copied to ', dest);
            });
    
            //var good = rootpath + keyword + '/train/good/' + filename;
            var good = source;
            if(good.indexOf('train') != -1 || good.indexOf('classified') != -1) {
                if(fs.existsSync(good)) {
                    console.log('del:'+good);
                    fs.unlink(good, function() {});
                }
            }
        } catch(e){
            console.error(e);
        }
        
    });
    console.log(">>>>> ", id, fn, filename)

    // res.writeHead(200, { "Content-Type": "application/json" });
    // res.end("Updated Successfully");
    let goodImages = [];
    let badImages = [];
    
    db.getImagelist(id, function(results) {
        
        results.forEach( row => {                    
            row.filename = mKeyword+'/' + row.filename;
            let tFileName = row.filename;
            
            if(row.s1class == 1){ // Good                        
                //row.filename = row.filename.replace('/', '/classified/good/');
                let newRow = JSON.parse(JSON.stringify(row));
                goodImages.push(newRow);
                row.filename = tFileName;
            } else if(row.s1class == -1){ // Bad                        
                //row.filename = row.filename.replace('/', '/classified/bad/');
                let newRow = JSON.parse(JSON.stringify(row));
                badImages.push(newRow);
                row.filename = tFileName;
            } 
        });

        //console.log(results, goodImages, badImages)
        let tFilename = fn.replace('good', 'bad');
        res.status(200).json({images: results, goodImages:goodImages, badImages:badImages, filename:tFilename});
    });
});

router.post('/list/askconfirm', util.isLoggedin, function(req, res) {
    var id = req.body.id;
    var done = req.body.done;
    let status;
    
    if(done){        
        status = job.READY_FOR_CONFIRM().code;
    } else{
        status = job.CROPPING_DONE().code;
    }
    db.setDone(id, done, status, (food) => {
        res.status(200).json(food);
    });
    
    
    // res.writeHead(200, { "Content-Type": "application/json" });
    // res.end("Updated Successfully");
});

router.post('/list/confirmed', util.isLoggedin, function(req, res) {
    var id = req.body.id;
     
    db.setState(id, job.READY_FOR_TRAINING().code);        
 
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end("Updated Successfully");
});

router.post('/crawlimages', util.isLoggedin, function(req, res){
    var path = data_dir + '/'
    var fileName = 'worldfoodlist'
        
    console.log(JSON.stringify(req.body.foods));
    var obj = JSON.parse(req.body.foods);
    console.log(obj);
    files.crawlImages(obj);

    res.redirect('foodcheck/' + {startId:1, endId:109});
});

router.post('/startcropping', util.isLoggedin, function(req, res) {
    var id = req.body.trainid;
    
    //baseDir = "/home/doinglab1/datahdd/data/Latest_Images/1221_new/20181220/"
    //savePath = "/home/doinglab1/datahdd/data/Latest_Images/1221_new/20181220_processed/"
    console.log('Start Cropping...', id);
    db.getKeywordbyid(id, function(keyword) {
        //var pathRoot = base + 'ait_data/'+ keyword +'/'
        var pathRoot = rootpath + keyword +'/'        
        var pathTrain = pathRoot + 'train'
        var savePath = pathRoot +'classified_cropped/'
        var pathBase = pathRoot +'classified/'

        console.log('pathclassify ', savePath);
        routerUtil.addCropper(id, pathBase, savePath, pathTrain)
        //db.setState(id, 'wait for training')
        res.redirect('datas');
    })
});

router.post('/starttrainig', util.isLoggedin, function(req, res) {
    var id = req.body.trainid;
    
    db.getKeywordbyid(id, function(keyword) {
        //var pathRoot = base + 'ait_data/'+ keyword +'/'
        var pathRoot = rootpath + keyword +'/'        
        var pathTrain = pathRoot + 'train'
        var pathClassify = pathRoot +'classified'
        //routerUtil.addTraining(id, pathRoot, pathTrain, pathClassify, "predict")
        routerUtil.addTrainingPredict(id, pathRoot, pathTrain, pathClassify)
        //db.setState(id, 'wait for training')
        db.setState(id, job.WAITING_LEARNING().code);
        res.redirect('datas');
    })
});

router.post('/registeritem', util.isLoggedin, function(req, res) {
    
    var keyword = req.body.keyword;
    let data = [];
            
    console.log(keyword.length, keyword, req.decoded.username);
    
    if(!Array.isArray(keyword)){
        data.push(keyword);
    } else {
        data = keyword;
    }
        
    data.forEach(function(k){
        console.log("Keyword: ", k);
        if(k != ''){
            db.addNewKeyword(k, k, req.decoded.username, function(id){
                db.setState(id, job.CRAWLING().code);
                routerUtil.addCrawling(id, k);
            });    
        }        
    });

    res.redirect('datas');
});

router.post('/deleteitem', util.isLoggedin, function(req, res) {
    var id = req.body.itemid;

    db.getKeywordbyid(id, function(keyword) {
        var path = data_dir + '/'+ keyword
        files.deleteFolderRecursive(path)
        db.deleteImagelist(id, function() {
            db.deleteitembyid(id, function() {
                res.redirect('datas');
            })
        })
    })
});

var uploadCroppedImage = function (req, res) {
    var Q = require('q');
    var deferred = Q.defer();
    var storage = multer.diskStorage({
      // 서버에 저장할 폴더
      destination: function (req, file, cb) {
        cb(null, 'public')
      },
  
      // 서버에 저장할 파일 명
      filename: function (req, file, cb) {        
        console.log(req.body.originalname);
        cb(null, Date.now() + '-' + req.body.originalname )
      }
    });
  
    var upload = multer({ storage: storage }).single('file');
    upload(req, res, function (err) {
      if (err) deferred.reject();
      else {
        //console.log("!!!!::::::::::::", req.body.file);

        var data = req.body.file.replace('/^data:image\/\w+;base64,/', "");
        var buf = new Buffer(data, 'base64');
        fs.writeFile('./image.png', buf);
        deferred.resolve(req.body.originalname)        
      };
    });
    return deferred.promise;
  };


const upload3 = multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {                        
            cb(null, 'public');                        
        },
        filename: function (req, file, cb) {
        //console.log(file, file.originalname, file.mimetype);
            
            console.log(path_util.getDataPath());
            //let extension = file.mimetype.split('/')[1];

            cb(null, "test!!.jpg");
            //cb(null, new Date().valueOf() + '.' +extension);
        //   let extension = path.extname(file.mimetype);
        //   let basename = path.basename(file.originalname, extension);
        //   console.log(extension," ::: " , basename);
        //   cb(null, basename + "-" + Date.now() + extension);
        }
    }),
});

const pngToJpeg = require('png-to-jpeg');

// gets the extension of a base64 encoded image
function getExt(data_url){
  return data_url.split("data:image/")[1].split(";")[0];
}

// gets the base64 encoded image from the data url
function getBa64Img(data_url){
  return data_url.split(";base64,").pop();
}

function writeImage(imageId, file_path, data_url, callback){
  console.log('...')
  if (arguments.length < 3){
    throw new Error("writeImage() requires three arguments. You have only included " + arguments.length);
  }
  //fs.writeFile(file_path + "." + getExt(data_url), getBa64Img(data_url), {encoding: "base64"}, callback);
  //fs.writeFile(file_path, getBa64Img(data_url), {encoding: "base64"}, callback);
  const buffer = new Buffer(data_url.split(/,\s*/)[1],'base64');
  pngToJpeg({quality: 100})(buffer).then(output => fs.writeFileSync(file_path, output));
  callback(imageId);
}

router.post('/saveimage', util.isLoggedin, upload3.single('file'), function(req, res) {    
    
    let originalfilename = req.body.originalfilename.split('/');
    let imageId = req.body.imageId;
    let status = parseInt(req.body.status);
    let type = req.body.type;

    console.log('saveimage:::', originalfilename, imageId, status, type);
    
    let dest;
    
    data_url = req.body.file;

    //console.log("data_url:::",data_url);

    // if(status === job.CLASSIFYING_DONE().code){
    //     if(originalfilename.length === 4){         
    //         dest = path_util.getModifiedImagePathClassified((originalfilename[0]), originalfilename[3]);
    //     }
    // } else if( status === job.CRAWLING_DONE().code){        
    //     dest = path_util.getModifiedImagePath(originalfilename[0], originalfilename[1]);
    // } else {        
    //     dest = path_util.getModifiedImagePath(originalfilename[0], originalfilename[1]);
    // }

    if(type === 'append'){
        //dest = path_util.getExistAppendPath() + originalfilename[0] + originalfilename[1];
        let tFilename = req.body.originalfilename.replace('.', 'm.');
        dest = tFilename.replace('/foodlist/exist/', path_util.getExistAppendPath());
    } else{
        dest = path_util.getModifiedImagePath(originalfilename[0], originalfilename[1]);
    }
    

    console.log("::: ", dest);
    // Save the image synchronously.
//    ba64.writeImageSync(originalfilename, data_url); // Saves myimage.jpeg.
    
    writeImage(imageId, dest, data_url, function(imageId, err){        
        if (err) throw err;
        let t = path_util.getFileName(dest);
        let removeFilePath = dest.replace("m.", ".");
        console.log("Image saved successfully", imageId, dest, t);

        if(fs.existsSync(removeFilePath)) {
            console.log('del:'+removeFilePath);
            fs.unlink(removeFilePath, function() {});
        }

        db.updateFoodImage(imageId, t, function(ret){
            console.log(imageId);
        });
    
        if(type === 'append'){            
            res.status(200).json({"dest":dest.replace(path_util.getExistAppendPath(), '/foodlist/exist/')});
        } else{            
            res.status(200).json({"dest":dest.replace(path_util.getDataPath() + '/', '')});
        }
        
    });
    // Or save the image asynchronously.
    // ba64.writeImage(imageId, dest, data_url, function(imageId, err){
    //     console.log('111');
    //     // if (err) throw err;
    //     // let t = path_util.getFileName(dest);
    //     // let removeFilePath = dest.replace("m.", ".");
    //     // console.log("Image saved successfully", dest, t);

    //     // if(fs.existsSync(removeFilePath)) {
    //     //     console.log('del:'+removeFilePath);
    //     //     fs.unlink(removeFilePath, function() {});
    //     // }

    //     // db.updateFoodImage(imageId, t);
    
    //     // if(type === 'append'){
    //     //     res.status(200).json({"dest":dest.replace(path_util.getExistAppendPath(), '/foodlist/exist/')});
    //     // } else{
    //     //     res.status(200).json({"dest":dest.replace(path_util.getDataPath() + '/', '')});
    //     // }
        
    // });

    
});

router.get('/checkfood', util.isLoggedin, function(req, res){
    var path = data_dir + '/'
    var fileName = 'worldfoodlist'
    console.log('check Food')
    files.loadFoodNames(path + fileName, 0, 10)
});

router.get('/testdb', function(req, res) {
    res.send('Done.') 
});

router.get(['/checkstatus', '/checkstatus/*'], util.isLoggedin, function(req, res) {
    console.log(req);
    var json = '{"status":"done"}';
    var obj = JSON.parse(json);
    //res.send(obj)
    res.json({"status":"done"});
});

router.get('/checkcrawlingstate', util.isLoggedin, function(req, res) {
    var json = '{"status":"done"}';
    var state = JSON.parse(json);
    
    res.json({"state":state});
});

router.get('/api/crawlingstart', util.isLoggedin, function(req, res) {
    let filename = req.query.filename;
    console.log(filename)
    console.log(req.query)
    db.getCrawlingListByFileName(filename, function(list){
        console.log(list);
        list.forEach(food => {
            console.log(food);
            db.setState(food._id, job.CRAWLING().code);
            routerUtil.addCrawling(food._id, food.attribute.keyword);
            console.log(food._id);
        });
        
    });
    
    let ret = {"filename":filename};
    console.log("1020 !!!!!! ", ret);
    res.status(200).json(ret);
    // res.json({"state":state});
    // 크롤링 스타트
    // DB 에 저장된 데이터 다 읽어와서
});

function uploadCrawlingList(req, res){
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
        cb(null, 'public')
      },
      filename: function (req, file, cb) {
        cb(null, Date.now() + '-' +file.originalname )
      }
    })
    
    var upload = multer({ storage: storage }).single('file')
    

    upload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            return res.status(500).json(err)
        } else if (err) {
            return res.status(500).json(err)
        }
        //return res.status(200).send(req.file)
        
    });
}

var uploadCrawlList = function (req, res) {
    var Q = require('q');
    var deferred = Q.defer();
    var storage = multer.diskStorage({
      // 서버에 저장할 폴더
      destination: function (req, file, cb) {
        cb(null, 'public')
      },
  
      // 서버에 저장할 파일 명
      filename: function (req, file, cb) {
        file.uploadedFile = {
          name: req.params.filename,
          ext: file.mimetype.split('/')[1]
        };
        cb(null, Date.now() + '-' +file.originalname )
      }
    });
  
    var upload = multer({ storage: storage }).single('file');
    upload(req, res, function (err) {
      if (err) deferred.reject();
      else {
        console.log(">>>>>>>>>>>>>>>>>", req.file);
        deferred.resolve(req.file.originalname)
        addCrawlingKeywords(req.file.path, req.file.filename)
        res.status(200).json({"filename":req.file});
      };
    });
    return deferred.promise;
  };

//router.post('/api/addcrawlingkeywords', upload2.single('file'), function(req, res) {    
router.post('/api/addcrawlingkeywords', util.isLoggedin, uploadCrawlList, function(req, res) {    
    
    console.log(req.file);
    // res.writeHead(200, { "Content-Type": "application/json" });
    // res.end("Updated Successfully");   
    res.status(200).json({"filename":req.file});
});


function addCrawlingKeywords(mFood, mFileName){
    console.log(mFood);
    var lineReader = require('readline').createInterface({
        input: require('fs').createReadStream(mFood)
    });
      
    lineReader.on('line', function (line) {
        //console.log('Line from file:', line);

        db.addNewFoodtoCrawlingList(line, mFileName, function(id){
            // db.setState(id, job.CRAWLING().code);
            // routerUtil.addCrawling(id, keyword);
            console.log(id);
        });
    });

    
    
    
}


module.exports = router;