const fs = require('fs');
const fs_extra = require('fs-extra');
var path = require('path');

var PORT;
var data_dir_name = 'ait_data';
var dest_training_dir = '/FoodJobsResult/new_add/pending/';
var exist_append_dir = '/FoodJobsResult/exist_append/pending/';
var exist_append_complete_dir = '/FoodJobsResult/exist_append/complete/';
var exist_replace_dir = '/FoodJobsResult/exist_replace/pending/';
const userdata_dir = '/FoodJobs/User_Data/';


if (process.env.NODE_ENV === "production") {    
  console.log("Production Mode ::: in path_util.js");

  if(process.env.NODE_ENV_PRODUCTION == "production1"){
    data_dir_name = 'ait_data_us';
    dest_training_dir = "/media/doinglab_nas/FoodJobsResult_US/new_add/pending/";
    exist_append_dir = "/media/doinglab_nas/FoodJobsResult_US/exist_append/pending/";
    exist_append_complete_dir = "/media/doinglab_nas/FoodJobsResult_US/exist_append/complete/";
    exist_replace_dir = "/media/doinglab_nas/FoodJobsResult_US/exist_replace/pending/";
  } else if(process.env.NODE_ENV_PRODUCTION == "production2") {
    data_dir_name = 'ait_data2'; // actual user
    dest_training_dir = "/media/doinglab_nas/FoodJobsResult/new_add/pending/";
    exist_append_dir = "/media/doinglab_nas/FoodJobsResult/exist_append/pending/";
    exist_append_complete_dir = "/media/doinglab_nas/FoodJobsResult/exist_append/complete/";
    exist_replace_dir = "/media/doinglab_nas/FoodJobsResult/exist_replace/pending/";
  } else if(process.env.NODE_ENV_PRODUCTION == "production3") {
    data_dir_name = 'ait_data3'; // actual user
    dest_training_dir = "/media/doinglab_nas/FoodJobsResult3/new_add/pending/";
    exist_append_dir = "/media/doinglab_nas/FoodJobsResult3/exist_append/pending/";
    exist_append_complete_dir = "/media/doinglab_nas/FoodJobsResult3/exist_append/complete/";
    exist_replace_dir = "/media/doinglab_nas/FoodJobsResult3/exist_replace/pending/";
  }
  
} else if (process.env.NODE_ENV == 'development') {
  data_dir_name = 'ait_data';
  dest_training_dir = "/media/doinglab_nas/FoodJobsResult_Temp/new_add/pending/";
  exist_append_dir = "/media/doinglab_nas/FoodJobsResult_Temp/exist_append/pending/";
  exist_append_complete_dir = "/media/doinglab_nas/FoodJobsResult_Temp/exist_append/complete/";
  exist_replace_dir = "/media/doinglab_nas/FoodJobsResult_Temp/exist_replace/pending/";
}

const base = path.resolve(__dirname, '../../../');

const data_dir = base + data_dir_name;
console.log(data_dir)
//const data_dir = "/media/doinglab_nas/AIT_Temp/" + data_dir_name;
//const data_dir = data_dir_name;
const keyword_dir = "/media/doinglab_nas/AIT_Temp/food_keywords";
// const dest_training_dir = "/media/doinglab_nas/FoodJobsResult/new_add/pending/"
const src_training_dir_tail = "/classified_cropped/good/"
const src_training_dir = "/train/good/"
const src_classified_dir = "/classified/good/";

const rimraf = require("rimraf");

module.exports = {
    getBasePath(){
        return base;
    },
    
    getDataPath(){      
        return data_dir;        
    },

    getFoodTrainPath(keyword){
        return this.getDataPath() + '/' + keyword + '/train'
    },

    getTrainingPath(){
        return dest_training_dir;
    },

    getDataPathWithKeyword(keyword){        
        return data_dir + '/' + keyword;
    },

    getModifiedImagePath(keyword, filename){
        return data_dir + '/' + keyword + '/' + filename.replace('.', 'm.');
    },

    getModifiedImagePathClassified(keyword, filename){                
        return this.getDataPathWithKeyword(keyword) + src_classified_dir + filename.replace('.', 'm.');
    },

    getFileName(dest){
        console.log(dest);
        let t = dest.split('/');
        let idx = t.length - 1;
        return t[idx];
    },

    getExistAppendPath(){
        return exist_append_dir;
    },

    getExistAppendCompletePath(){
        return exist_append_complete_dir;
    },

    getDataPathWithKeywordCropped(keyword){
        return this.getDataPathWithKeyword(keyword) + src_training_dir_tail;
    },

    getClassifiedPathWithKeyword(keyword){
        return this.getDataPathWithKeyword(keyword) + src_classified_dir;
    },

    getCompleteBasePath(keyword){
        return dest_training_dir + keyword + '/';
    },

    getExistAppendCompleteBasePath(keyword){
        return exist_append_complete_dir + keyword + '/';
    },

    getDataPathWhenComplete(path){
        return path + src_training_dir;
    },

    getKeywordPath(){
        return keyword_dir;
    },
    
    getSrcTrainingPath(path){
        return path + src_training_dir_tail;
    },

    getDataDirName(){
        return data_dir_name;
    },

    checkDataPath(){
        if (!fs.existsSync(this.getDataPath())){
            console.log('Created Root Path(' + this.getBasePath() + ')')
            fs.mkdirSync(this.getDataPath());
        }

        return this.getDataPath();
    },

    getPyScriptPath(){        
        var currentPath = process.cwd();
        return currentPath + '/py/';
    },

    removeFolderAsync(path){
        console.log("removeFolderAsync:::", path);
        if(fs.existsSync(path)){
            rimraf(path, function () { console.log(path + " REMOVED!!!"); });
        }        
    },

    removeFile(path){
        fs.unlink(path, (err) => {
            if (err) {
              console.error(err)
              return
            }
          
            //file removed
          })
    },

    moveFolderAsync(src, dest, callback){
        console.log("moveFolderAsync::: ", src, dest);
        if(fs.existsSync(src)){
            fs_extra.move(src, dest, err => {
                if(err) return console.error(err);
                console.log(src, ' moved to ', dest, ' success!');

                callback();
            });
        } else{
            console.log("Not Exist!!!!", src);
        }
    },

    getUserDataClassifiedPath(){
        console.log(path.join(userdata_dir, 'classified/'))
        return path.join(userdata_dir, 'classified/');
    }

    
}
