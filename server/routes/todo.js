var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');

var os = require('os');
var exec = require('child_process').exec;
var Todo = require('../models/todo');

/* GET home page. */
router.get('/', function(req, res, next) {
  return Todo.find({}).sort({regdate: -1}).then((todos) => res.send(todos));
});

router.post('/', function(req, res, next) {
  const todo = Todo();
  const title = req.body.title;
  const content = req.body.content;

  todo.title = title;
  todo.content = content;

  return todo.save().then((todo) => res.send(todo)).catch((error) => res.status(500).send({error}));
});

router.get('/call', function(req, res, next){
    
  let t = path.resolve(__dirname, '/');
  console.log(__basedir);
  let testFolder = '/ait_data';
  let filelist;
  fs.readdir(testFolder, function(error, filelist){
    console.log(filelist);
    res.status(200).json(filelist);
  })


  
});

module.exports = router;
