#!/usr/bin/env python
# coding: utf-8

# In[2]:


from keras.models import load_model
import numpy as np
import keras
from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
import tensorflow as tf
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D, ZeroPadding2D, GlobalAveragePooling2D, AveragePooling2D
from keras.layers.normalization import BatchNormalization
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ModelCheckpoint, CSVLogger, LearningRateScheduler, ReduceLROnPlateau
from keras.optimizers import SGD
from keras.regularizers import l2
import keras.backend as K
import math
from keras import regularizers
from keras.applications.inception_v3 import InceptionV3
from keras.applications.inception_v3 import preprocess_input, decode_predictions
from keras.preprocessing import image
from keras.layers import Input
import os
import time
import random
import glob
import sys
import shutil
from shutil import copyfile

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"

def deleteFile(path):
    dir_name = path
    test = os.listdir(dir_name)

    for item in test:
        if item.endswith(".hdf5"):
            os.remove(os.path.join(dir_name, item))

def predict(pathRoot, pathTrain, pathClassify):
    rootDir = pathRoot
    model_path = pathTrain+"/food_model.h5"
    weight_path = pathTrain
    classname_path = pathTrain+"/food_classes.txt"

    image_size = 299

    with open(classname_path) as f:
        classes_name = f.read().splitlines()

    print(len(classes_name))

    list_of_files = glob.glob(pathTrain+'/'+'*.hdf5') # * means all if need specific format then *.csv
    weight_path = max(list_of_files, key=os.path.getctime)

    model = load_model(model_path, custom_objects={"tf":tf})
    model.load_weights(weight_path)
    opt = SGD(lr=.01, momentum=.9)
    model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])
    print("weight loaded")

    imageFiles = sorted(os.listdir(rootDir))
    print(imageFiles)
    for imageFile in imageFiles:
        imagePath = rootDir + imageFile

        if os.path.isdir(imagePath):
            continue

        top_5 = False
        print(imagePath)
        img_org = image.load_img(imagePath, target_size=(image_size, image_size))
        x = image.img_to_array(img_org)
        x = np.expand_dims(x, axis=0)
        #img = np.vstack([x, x, x])
        img = np.vstack([x])

        classes = model.predict(img)

        idx = np.argmax(classes, 1)

        top_n_preds= np.argpartition(classes, -2)[:,-2:] 
        top_n_preds= top_n_preds[0]
        final_result = top_n_preds[np.argsort(-classes[:, top_n_preds])]

        deleteFile(pathClassify+'/good')
        deleteFile(pathClassify+'/bad')

        if classes_name[final_result[0, 0]] == 'good':
            copyfile(imagePath, pathClassify+'/good/'+imageFile)
        else:
            copyfile(imagePath, pathClassify+'/bad/'+imageFile)
        
        # print("Predict  -  %s:%f, %s:%f, %s:%f, %s:%f" % (classes_name[final_result[0, 0]], classes[0,final_result[0, 0]],
        #                         classes_name[final_result[0, 1]], classes[0,final_result[0, 1]],
        #                         classes_name[final_result[0, 2]], classes[0,final_result[0, 2]],
        #                         classes_name[final_result[0, 3]], classes[0,final_result[0, 3]]))
        # print("")

def main(argv):
    pathRoot = argv[1]
    pathTrain = argv[2]
    pathClassify= argv[3]
    print('Path:', pathRoot, pathTrain, pathClassify)
    predict(pathRoot, pathTrain, pathClassify)

if __name__ == "__main__":
    main(sys.argv)
