#!/usr/bin/env python
# coding: utf-8

# In[1]:


from __future__ import print_function
import keras
import os
#os.environ["THEANO_FLAGS"] = "mode=FAST_RUN,device=cuda0,floatX=float32,blas.ldflags="
#import theano
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D

from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D, ZeroPadding2D, GlobalAveragePooling2D, AveragePooling2D
from keras.layers.normalization import BatchNormalization
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ModelCheckpoint, CSVLogger, LearningRateScheduler, ReduceLROnPlateau
from keras.optimizers import SGD, Adam, RMSprop
from keras.regularizers import l2
import keras.backend as K
import math
from keras import regularizers
from keras.applications.inception_v3 import InceptionV3
#from keras.applications.inception_v3 import preprocess_input, decode_predictions
from keras.preprocessing import image
from keras.layers import Input
from keras.layers import concatenate
from keras.layers.core import Lambda
from keras.models import Model
from keras.applications.xception import Xception
import tensorflow as tf
import sys
from keras.callbacks import EarlyStopping
print(tf.__version__)
print(keras.__version__)


# In[2]:

import shutil
import os
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True


setattr(tf.contrib.rnn.GRUCell, '__deepcopy__', lambda self, _: self)
setattr(tf.contrib.rnn.BasicLSTMCell, '__deepcopy__', lambda self, _: self)
setattr(tf.contrib.rnn.MultiRNNCell, '__deepcopy__', lambda self, _: self)

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)

#rootPath = '/media/doinglab_nas/' + 'AIT_Temp/ait_data/'


#os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
#os.environ["CUDA_VISIBLE_DEVICES"]="2"
#os.environ["CUDA_VISIBLE_DEVICES"]=""

gpu_num = 1

def deleteFile(path):
    dir_name = path
    test = os.listdir(dir_name)

    for item in test:
        if item.endswith(".hdf5"):
            os.remove(os.path.join(dir_name, item))

def train(pathTrain):    
    trainBaseDir = pathTrain
    testBaseDir = pathTrain
    classes_name = sorted(os.listdir(trainBaseDir))

    print('Remove hdf5:'+trainBaseDir+'/*.hdf5')
    deleteFile(trainBaseDir)
    thefile = open(trainBaseDir+'/food_classes.txt', 'w')

    for item in classes_name:
        thefile.write("%s\n" % item)
    thefile.close()

    n_classes = len(classes_name)#101
    image_size = 299
    #batch_size = 32*gpu_num
    batch_size = 4*gpu_num

    print("Total classes %d" % n_classes)

    K.clear_session()

    base_model = Xception(weights='imagenet', include_top=False, input_tensor=Input(shape=(image_size, image_size, 3)))


    x = base_model.output
    x = AveragePooling2D(pool_size=(10, 10))(x)
    #x = MaxPooling2D(pool_size=(10, 10))(x)
    x = Dropout(.5)(x)
    x = Flatten()(x)


    predictions = Dense(n_classes, kernel_initializer='glorot_uniform', kernel_regularizer=regularizers.l2(.0005), activation='softmax')(x)


    model = Model(inputs=base_model.input, outputs=predictions)

    #opt = Adam(lr=1e-4, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    opt = SGD(lr=.01, momentum=.9)
    if gpu_num > 1:
        model = ModelMGPU(model , gpu_num)

    model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])
    model.save(trainBaseDir+"/food_model.h5")
    #model.summary()

    #for layer in model.layers:
    #    print(layer, layer.trainable)
        
    checkpointer = ModelCheckpoint(filepath=trainBaseDir+'/food_weight.{epoch:02d}-{val_loss:.2f}.hdf5', verbose=1, save_best_only=True)
    csv_logger = CSVLogger(trainBaseDir+'/food.log')

    lr_scheduler = LearningRateScheduler(schedule)
        
    train_datagen = ImageDataGenerator(
            #rescale=1./255,
            rotation_range=90,
            width_shift_range=0.12,
            height_shift_range=0.12,
            shear_range=0.18,
            zoom_range=0.15,
            channel_shift_range=0.005,
            horizontal_flip=False,
            vertical_flip=False)

    test_datagen = ImageDataGenerator()#rescale=1./255)

    train_generator = train_datagen.flow_from_directory(  
            trainBaseDir,
            target_size=(image_size, image_size),
            batch_size=batch_size,
            classes = classes_name,
            class_mode='categorical')

    validation_generator = test_datagen.flow_from_directory(
            testBaseDir,
            target_size=(image_size, image_size),
            batch_size=batch_size,
            classes = classes_name,
            class_mode='categorical')


    STEP_SIZE_TRAIN=train_generator.n //train_generator.batch_size
    STEP_SIZE_VALID=validation_generator.n //validation_generator.batch_size

    print(STEP_SIZE_TRAIN)
    print(STEP_SIZE_VALID)

    early_stopping = EarlyStopping(patience = 1)

    model.fit_generator(
        train_generator,
        steps_per_epoch=(STEP_SIZE_TRAIN),
        epochs=5,
        validation_data=validation_generator,
        validation_steps=(STEP_SIZE_VALID),
        workers=4,
        max_queue_size=32,
        callbacks=[lr_scheduler, csv_logger, checkpointer, early_stopping])
        #callbacks=[csv_logger, checkpointer])

    #Prediction



def multi_gpu_model(model, gpus):
    if isinstance(gpus, (list, tuple)):
        num_gpus = len(gpus)
        target_gpu_ids = gpus
    else:
        num_gpus = gpus
        target_gpu_ids = range(num_gpus)

    def get_slice(data, i, parts):
        shape = tf.shape(data)
        batch_size = shape[:1]
        input_shape = shape[1:]
        step = batch_size // parts
        if i == num_gpus - 1:
            size = batch_size - step * i
        else:
            size = step
            size = tf.concat([size, input_shape], axis=0)
            stride = tf.concat([step, input_shape * 0], axis=0)
            start = stride * i
        return tf.slice(data, start, size)

    all_outputs = []
    for i in range(len(model.outputs)):
        all_outputs.append([])

    # Place a copy of the model on each GPU,
    # each getting a slice of the inputs.
    for i, gpu_id in enumerate(target_gpu_ids):
        with tf.device('/gpu:%d' % gpu_id):
            with tf.name_scope('replica_%d' % gpu_id):
                inputs = []
                # Retrieve a slice of the input.
                for x in model.inputs:
                    input_shape = tuple(x.get_shape().as_list())[1:]
                    slice_i = Lambda(get_slice,
                                    output_shape=input_shape,
                                    arguments={'i': i,
                                                'parts': num_gpus})(x)
                    inputs.append(slice_i)

                # Apply model on slice
                # (creating a model replica on the target device).
                outputs = model(inputs)
                if not isinstance(outputs, list):
                    outputs = [outputs]

                # Save the outputs for merging back together later.
                for o in range(len(outputs)):
                    all_outputs[o].append(outputs[o])

    # Merge outputs on CPU.
    with tf.device('/cpu:0'):
        merged = []
        for name, outputs in zip(model.output_names, all_outputs):
            merged.append(concatenate(outputs,
                                    axis=0, name=name))
        return Model(model.inputs, merged)


# In[7]:


class ModelMGPU(Model):
    def __init__(self, ser_model, gpus):
        pmodel = multi_gpu_model(ser_model, gpus)
        self.__dict__.update(pmodel.__dict__)
        self._smodel = ser_model

    def __getattribute__(self, attrname):
        '''Override load and save methods to be used from the serial-model. The
        serial-model holds references to the weights in the multi-gpu model.
        '''
        # return Model.__getattribute__(self, attrname)
        if 'load' in attrname or 'save' in attrname:
            return getattr(self._smodel, attrname)

        return super(ModelMGPU, self).__getattribute__(attrname)


def schedule(epoch):
    if epoch < 2:
        return .01
    elif epoch < 3:
        return .007
    elif epoch < 4:
        return .004
    elif epoch < 5:
        return .001
    elif epoch < 6:
        return .0007
    elif epoch < 7:
        return .0004
    elif epoch < 8:
        return .0002
    elif epoch < 9:
        return .0001
    elif epoch < 10:
        return .00005
    else:
        return .00001

def main(argv):
    pathTrain = argv[1]
    
    print('pathTrain:', pathTrain)
    train(pathTrain)

if __name__ == "__main__":
    main(sys.argv)
