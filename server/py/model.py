from keras.models import load_model
from keras.preprocessing import image as image_p
from keras.applications.inception_v3 import preprocess_input, decode_predictions
from utils.utils import get_yolo_boxes, makedirs
from utils.bbox import draw_boxes
import json
import numpy as np
import tensorflow as tf
import time
import os
import cv2
try:
    from PIL import Image as pil_image
except ImportError:
    pil_image = None


os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"
#os.environ["CUDA_VISIBLE_DEVICES"]="2,3"

rootDir = './'
weight_path = rootDir+"food.h5"

os.chdir(os.path.dirname(__file__))
currentPath = os.getcwd()
print(os.getcwd())

input_size = 416
with open(currentPath + "/config.json") as config_buffer:    
    config = json.load(config_buffer)


#anchors = [35,36, 65,54, 68,88, 90,146, 122,90, 153,278, 153,155, 239,197, 328,333]
obj_thresh, nms_thresh = 0.85, 0.32
print ('initialize model...')


###############################
#   Load trained weights
###############################

print (weight_path)
model = load_model(weight_path)
print ('load weights...')

    
def predict(img):
    resultBox = []

    start_time = time.time()

    boxes = get_yolo_boxes(model, [img], input_size, input_size, config['model']['anchors'], obj_thresh, nms_thresh)[0]

    for box in boxes:
        tbox = {}
        tbox["xmin"] = box.xmin
        tbox["xmax"] = box.xmax
        tbox["ymin"] = box.ymin
        tbox["ymax"] = box.ymax
        tbox["score"] = round(box.get_score(),3)
        
        if box.get_score() > obj_thresh:
            resultBox.append(tbox)
            
    #print("--- %s seconds ---" % (time.time() - start_time))


    return {'boxes': resultBox}

