#-*- coding: utf-8 -*-

from icrawler.builtin import GoogleImageCrawler
import sys
import os
from pathlib import Path

#dir_path = os.path.dirname(os.path.realpath(__file__))
current_dir =  os.path.abspath(os.path.dirname(__file__))
parent_dir = os.path.abspath(current_dir + "/../../../")
print(current_dir, parent_dir)

def crawl(keyword, count=None):

    # productionType = ""

    # try:
    #     productionType = os.environ['NODE_ENV_PRODUCTION']
    #     print(productionType)

    #     if productionType == 'production1':
    #         rootPath = '/media/doinglab_nas/' + 'AIT_Temp/ait_data_us/'
    #     elif productionType == 'production2':
    #         rootPath = '/media/doinglab_nas/' + 'AIT_Temp/ait_data2/'
    #     elif productionType == 'production3':
    #         rootPath = '/media/doinglab_nas/' + 'AIT_Temp/ait_data3/'
    # except KeyError:
    #     print('DEV Mode')
    #     rootPath = '/media/doinglab_nas/' + 'AIT_Temp/ait_data/'   
    
    #rootPath = '/media/doinglab_nas/' + 'AIT_Temp/ait_data/'
    rootPath = '/ait_data/'

    google_crawler = GoogleImageCrawler(parser_threads=10, downloader_threads=10,
                                        storage={'root_dir': rootPath + str(keyword).strip()})

    if count == 0 or count is None:
        count = 200

    google_crawler.crawl(keyword=keyword, max_num=count)

if __name__ == '__main__':
    print('Crawl Start')

    print(current_dir)
    print(parent_dir)
    print(len(sys.argv), sys.argv[0], sys.argv[1])
    
   
    if len(sys.argv) - 1 == 1:
        print('!!!!!!!!!!.>>>>>>' +  sys.argv[1])
        crawl(sys.argv[1])
    elif len(sys.argv) - 1 == 2:
        print("Crawling Max Num: 30")
        print(sys.argv[1], sys.argv[2])
        crawl(sys.argv[1], int(sys.argv[2]))
   
    print('Crawl End')
