#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
import numpy as np
import os
import model as multimodel
#from flask import Flask, request, redirect, url_for, jsonify
import time
import json
from keras.preprocessing import image as kImage
import io
import cv2

from keras.models import load_model
import numpy as np
import keras
from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
import tensorflow as tf
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D, ZeroPadding2D, GlobalAveragePooling2D, AveragePooling2D
from keras.layers.normalization import BatchNormalization
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ModelCheckpoint, CSVLogger, LearningRateScheduler, ReduceLROnPlateau
from keras.optimizers import SGD
from keras.regularizers import l2
import keras.backend as K
import math
import matplotlib.pyplot as plt
from keras import regularizers
from keras.applications.inception_v3 import InceptionV3
from keras.applications.inception_v3 import preprocess_input, decode_predictions
from keras.preprocessing import image
from keras.layers import Input
import os
import time
import random


class Cropper:
    def __init__(self, baseP, saveP, imageSz):

        try:
            from PIL import Image as pil_image
        except ImportError:
            pil_image = None
   
        #self.baseDir = "/home/dykim/Workspace/ait/ait_data/감자만두/classified/"
        #self.savePath = "/home/dykim/Workspace/ait/ait_data/감자만두/classified_cropped/"
        self.baseDir = baseP
        self.savePath = saveP

        #self.image_size = 448
        self.image_size = imageSz

    def get_multpredict_positions(self, img_path):
        #start = time.time()
        prediction = multimodel.predict(img_path)
        #end = time.time()
        #print("Multi Time: " + str(end-start))    
        return prediction
        
        
    def getPredictImageClasses(self, tgtImg):
        #start = time.time()
        clist = model.predict(tgtImg)            
        #end = time.time()
        #print("Predict Time: " + str(end-start))    

        return clist


    def getTop5List(self, classList):
        classes = classList   
        idx = np.argmax(classes, 1)
    #     if idx != classes_name[idx[0]]:
    #          print("%s:%f - %s" % (classes_name[idx[0]], classes[0,idx[0]], testImg))

        top_n_preds= np.argpartition(classes, -5)[:,-5:] 
        top_n_preds= top_n_preds[0]
        final_result = top_n_preds[np.argsort(-classes[:, top_n_preds])]
        
        top_n_preds = final_result[0]
        
        top5list = []
        for lc in range(5):
            top5list.append(classes_name[top_n_preds[lc]])        
        
        return top5list


    def crop(self):
        file_names = sorted(os.listdir(self.baseDir))

        for tgtClass in file_names:
            newPath = self.savePath + tgtClass
                        
            try:
                print('::::::', newPath)
                if not os.path.exists(newPath):
                    os.makedirs(newPath)                
                    print("Make Dirs completed!")

            except OSError:
                if not os.path.isdir(newPath):
                    raise
                    
            
            imglist = sorted(os.listdir(self.baseDir+tgtClass))
            proc = 0
        
            for testImg in imglist:
                
                fname = testImg.split('.')[1].lower()
                if fname != 'jpg':
                    continue
                
                imgPath = self.baseDir  + tgtClass + "/" + testImg        
                print('IMG PATH: ', imgPath)
                try:
                    imgArr = cv2.imread(imgPath, cv2.IMREAD_COLOR)
                    height, width, _ = imgArr.shape
                except:
                    print("Src Imread Error")
                    continue
      
                multiPos = self.get_multpredict_positions(imgArr) 
                
            
                listMulti = multiPos['boxes']
                print(len(listMulti))
                i = 0        
                for pos in listMulti:
                    xmin = pos['xmin']
                    ymin = pos['ymin']
                    xmax = pos['xmax']
                    ymax = pos['ymax']     

                    if xmin < 0 :
                        xmin = 0
                    if ymin < 0:
                        ymin = 0
                    if ymax > height:
                        ymax = height
                    if xmax > width:
                        xmax = width

                    cropRatio = 0.05

                    xmin = (int)(xmin - (xmin*cropRatio))
                    if xmin <= 0:
                        xmin = 0

                    xmax = (int)(xmax + (xmax*cropRatio))
                    if xmax > width:
                        xmax = width

                    ymin = (int)(ymin - (ymin*cropRatio))
                    if ymin <= 0:
                        ymin = 0

                    ymax = (int)(ymax + (ymax*cropRatio))
                    if ymax > height:
                        ymax = height



                    w = xmax - xmin
                    h = ymax - ymin

                    crop_img = imgArr[ymin:ymax, xmin:xmax]

                    #crop_w, crop_h, _= crop_img.shape            
                    crop_h,crop_w, _= crop_img.shape            

                    square = crop_w * crop_h

                    if(crop_w <= 0 or crop_h <= 0):
                        continue

                    if square <= 40000:
                        continue

                    #print ("w:%d, h:%d, cw:%d, ch:%d, ym:%d, yx:%d, xm:%d, xx:%d" % (width, height,crop_w,crop_h, ymin, ymax, xmin, xmax))
                    if (crop_h <= (height*0.15)) and (ymin <= 10 or ymax>=(height-10)):
                        continue

                    if (crop_w <= (width*0.15)) and (xmin <= 10 or xmax>=(width-10)):
                        continue

                    if (crop_w != crop_h):   
                        img_size = (crop_w) if crop_w > crop_h else (crop_h)

                        if img_size < 416:
                            img_size = 416

                        xpt = int((img_size / 2) - (crop_w / 2))
                        ypt = int((img_size / 2) - (crop_h / 2))
                        patchImg = np.zeros([img_size, img_size, 3], dtype=np.uint8)
                        patchImg.fill(255)

                        #patchImg[xpt:xpt+crop_w, ypt:ypt+crop_h] = crop_img                
                        patchImg[ypt:ypt+crop_h, xpt:xpt+crop_w] = crop_img                
                        crop_img = patchImg         


                    saveImg = crop_img

                    mPath = newPath+"/"+str(i)+testImg                    
                    i = i + 1

                    try:                
                        cv2.imwrite(mPath, crop_img, [int(cv2.IMWRITE_JPEG_QUALITY), 100])
                        proc +=1
                    except:
                        print("imwrite error")
                        continue
            
            print (tgtClass + " proc cnt: " + str(proc) + "/" +str(len(imglist)))
                
        print("Crop Job is finished!")

def main(argv):
    
    basePath = argv[1]
    savePath = argv[2]
    
    print('Path:', basePath, savePath)

    cropper = Cropper(basePath, savePath, 416)

    cropper.crop()

if __name__ == "__main__":
    main(sys.argv)