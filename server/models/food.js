var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const foodSchema = new Schema({    
    attribute: { 
        name: {type: String, required: true},
        keyword: {type: String, required: true},
        state: {type: Number, default: 0},
        is_activated: {type: Boolean, default: 1},
        done: {type: Boolean, default: 0},
        username: {type: String, default: ""}
    },
    source: {
        national_code: {type: String, required: true},
        name: String,
        filename: String
    },
    image: [
            {
                filename: {type: String },
                s1class: {type: Number, default: 0},
                s2class: {type: Number, default: 0}
            }
    ]    
},
{
  timestamps: true
});

module.exports = mongoose.model('food', foodSchema);