var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const accountSchema = new Schema({    
    username: {type: String, required: true},
    password: {type: String, required: true},
    lastLogin: {type: Date, required: true},
    token: {type: String, required: true}    
},
{
  timestamps: true
});

module.exports = mongoose.model('food', accountSchema);