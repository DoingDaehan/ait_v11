var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const UserDataSchema = new Schema({    
    attribute: { 
        selectedDate: {type: String, required: true},
        status: {type: Number, default: 0},
        listFileName: {type: String},
        listCount: {type: Number, default: 0},
        faceCount: {type: Number, default: 0},
        foodCount: {type: Number, default: 0},
        noneFoodCount: {type: Number, default: 0},
        finishedDate: {type: Date}
    },
    sensitiveImages: [
        {
            filename: {type: String },
            eatHistoryId: {type: Number }
        }
    ],
    images: [
        {
            filename: {type: String},
            type: {type: String},
            eatHistoryId: {type: Number},
            isSensitive: {type: Boolean, defalut: false},
            isDownload: {type: Boolean, default: false}
        }
    ]    
},
{
  timestamps: true
});

module.exports = mongoose.model('UserData', UserDataSchema);