// const express = require('express')
// const bodyParser = require('body-parser')
// const expressWinston = require('express-winston')
// const cors = require('cors');
// const path_util = require('./routes/path_util')
// const engines = require('consolidate');
// const router = require('./routes/createRouter.js')()
// console.log(router);
// module.exports = ({ database, logger }) => express()
// .use(expressWinston.logger({
//     winstonInstance: logger,
//     msg: '{{res.statusCode}} {{req.method}} {{req.url}} {{res.responseTime}}ms',
//     meta: false,
// }))
// .use(bodyParser.urlencoded({ extended: true }))
// .use(bodyParser.json())
// .use((req, res, next) => {
//     req.base = `${req.protocol}://${req.get('host')}`
//     req.logger = logger
//     req.db = database
//     return next()
// })
// .use(express.static('./public'))
// .use('/foodlist', express.static(path_util.getDataPath()))
// .use('/', cors({origin: "http://*",optionSuccessStatus: 200}), router)
// .use('/api', router)
// .use('/jspaint', express.static(__dirname + "/views/jspaint"))
// .engine('html', engines.mustache)
// .set('view engine', 'html')
// .use((error, req, res, next) => {
//     logger.error(error, error)
//     res.status(error.status || 500).json({ error })
// })

const express = require('express');
const bodyParser = require('body-parser');
const expressWinston = require('express-winston');
const engines = require('consolidate');
const cors = require('cors');
const path_util = require('./routes/path_util');
const router = require('./routes/createRouter.js')();
const path = require('path');


module.exports = ({ database, logger }) => express()
.use(expressWinston.logger({
    winstonInstance: logger,
    msg: '{{res.statusCode}} {{req.method}} {{req.url}} {{res.responseTime}}ms',
    meta: false,
}))
.use(bodyParser.urlencoded({ extended: true }))
.use(bodyParser.json())
.use((req, res, next) => {
    req.base = `${req.protocol}://${req.get('host')}`
    req.logger = logger
    req.db = database
    return next()
})
.use(express.static('./public'))
.use('/foodlist', express.static(path_util.getDataPath()))
.use('/', cors({origin: "http://*",optionSuccessStatus: 200}), router)
.use('/api', router)
.use('/jspaint', express.static(__dirname + "/views/jspaint"))
.engine('html', engines.mustache)
.set('view engine', 'html')
// .use((req, res, next) => {
//     var clientPath = path.join(__dirname, "..", "build");
//     if (process.env.NODE_ENV === "production") {    
//         console.log("Production Mode ::: ", clientPath);
//         console.log(path.join(clientPath, "/static/css"));
//         express().use('/', express.static(clientPath));
//         //app.use('/a', express.static(path.join(clientPath, "/static/css")));
//         //app.use('/a', express.static(path.join(clientPath + "/static/js")));
//     } else if (process.env.NODE_ENV == 'development') {
//         console.log("Development Mode");
//     }
//     return next();
// })
.use((error, req, res, next) => {
    logger.error(error, error)
    res.status(error.status || 500).json({ error })
})