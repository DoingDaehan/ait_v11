var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var logger = require('morgan');
var cors = require('cors');
var mongoose = require('mongoose');

const path_util = require('routes/path_util')
require('dotenv').config();

var db = mongoose.connection;
db.on('error', console.error);
db.once('open', function(){    
    console.log("Connected to mongod server");
});

const dbURL = `mongodb://${process.env.DB_HOST || 'localhost'}:27017/${
  process.env.DB
}`

mongoose.connect(dbURL);
     
require('db/db').init((err) => {
  if (err) {        
    console.log(err);
      process.exit();
  } else {
    console.log('pool 생성 완료');
  }
});

console.log(dbURL);
//var indexRouter = require('./routes/index');
const foodsRouter = require('./routes/v2/foods');
const usersRouter = require('./routes/users');
const todoRouter = require('./routes/todo');
const router = require('./routes/router');
const auth = require('./routes/v2/auth/auth.ctrl');
const user = require('./routes/v2/auth/user');
const userdata = require('./routes/v2/userdata/userdata.ctrl');

var app = express();
global.__basedir = __dirname;

//Cors 설정
app.use(cors());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// Middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.header('Access-Control-Allow-Headers', 'content-type, x-access-token'); //1
  next();
});


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/foodlist', express.static(path_util.getDataPath()));
app.use('/foodlist/exist', express.static(path_util.getExistAppendPath()));
app.use('/foodlist/user', express.static(path_util.getUserDataClassifiedPath()));
app.use('/foodlist/user/foodimage', express.static(path_util.getUserDataClassifiedPath()));

console.log('::::>>>>>', path_util.getDataPath(), path_util.getExistAppendPath());
//app.use('/', indexRouter);
app.use('/', router);
app.use('/v2', foodsRouter);
app.use('/users', usersRouter);
app.use('/todos', todoRouter);
app.use('/v2/auth', auth);
app.use('/v2/user', user);
app.use('/v2/userdata', userdata);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
