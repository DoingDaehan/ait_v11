var db = require('./database_mongo');

function get_line(filename, line_no, callback) {
    var fs = require('fs');
    var data = fs.readFileSync(filename, 'utf8');
    var lines = data.split("\n");

    if(+line_no > lines.length){
        throw new Error('File end reached without finding line');
    }

    callback(null, lines[+line_no]);
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  
async function demo() {
    console.log('Taking a break...');
    await sleep(2000);
    console.log('Two seconds later');
}
  

  

module.exports = {
    deleteFolderRecursive: function(path) {
        var fs = require('fs')

        if( fs.existsSync(path) ) {
            fs.readdirSync(path).forEach(function(file,index){
                var curPath = path + "/" + file;
                if(fs.lstatSync(curPath).isDirectory()) { // recurse
                    module.exports.deleteFolderRecursive(curPath);
                } else { // delete file
                    fs.unlinkSync(curPath);
                }
            });
            fs.rmdirSync(path);
        }
    },
   
    crawlImages: function(foodNames){
        
        db.getFoodDatas(function(result){
            var dataCount = result.length;
            console.log(dataCount);

            result.forEach(value => {
                ch_crawl.add([value.id, value.foodname, 30]);
            });
         
    
        });       
    },

    loadFoodNames: function(fileName, startIndex, endIndex, callback) {
        
        
        //console.log(fileName)
        var lineReader = require('line-reader');
        //var lineReader = require('readline').createInterface({
          //  input: require('fs').createReadStream(fileName)
        //});

        var fs = require('fs');
        var result = [];
        
        for(var i = startIndex; i < endIndex; i++){        
            get_line(fileName, i, function(err, line){
                //console.log('The line: ' + line);
                result.push({id:i, foodname:line});
            })
        }
        
        callback(result);
        return result;

        
    },

    loadFoodNamesFull: function(fileName, callback){
        var fs = require('fs');
        var input = fs.createReadStream(fileName);
        var result = [];
        var i = 0;

        var promise = new Promise(function(resolve, reject) {
            var lineReader = require('readline').createInterface({
                input: input
            });
            input.on('end', () => {
                resolve("I reached the end!");
            });

            lineReader.on('line', (line) => {                
                result.push({id:i++, foodname:line});                
            });
        });

        promise.then((resolveResult) => {
            callback(result);
            return result;
        });
    }
}