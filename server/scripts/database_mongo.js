var dbconfig = require('../configuration/dbconfig.json')

const mongoose = require('mongoose');

const Enum = require('enum');
const classifiedTypeEnum = new Enum({'FACE': 'face', 'FOOD': 'food', 'NONE_FOOD': 'nonefood'});

const jobManager = require('../routes/ProcessStatusManager.js');
const job = jobManager();
const path = require('path');
const Food = require('../models/food');
const UserData = require('../models/userdata');

function getDBConnectionMongo(){
    console.log(process.env.MONGO_URI)
    // Node.js의 native Promise 사용
    mongoose.Promise = global.Promise;

    // CONNECT TO MONGODB SERVER
    mongoose.connect(process.env.MONGO_URI_DEV)
    .then(() => console.log('Successfully connected to mongodb'))
    .catch(e => console.error(e));
}

function descOrder(a, b){        
    return a._id > b._id ? -1 : a._id < b._id ? 1 : 0;            
}

function ascOrder(a, b){    
    return a._id < b._id ? -1 : a._id > b._id ? 1 : 0;    
}

module.exports = {

    
    connect: function(){
        getDBConnectionMongo();        
        
    },

    addNewUserDataList: function(selectedDate, listFileName, listCount, callback) {
        
        const userData = UserData();
        userData.attribute.listFileName = listFileName;
        userData.attribute.listCount = listCount;
        userData.attribute.selectedDate = selectedDate;
        userData.attribute.status = job.IDLE().code;
        // userData.attribute.finishDate = "None";
        
        userData.save(function(err, userData){                        
            if(err){
                console.log(err);
                throw err;
            } else {                
                console.log(userData);
                callback(userData._id);
            }
        });

        
    },

    addUserDataList: function(selectedDate, listCount, callback) {
        
        const userData = UserData();        
        userData.attribute.listCount = listCount;
        userData.attribute.selectedDate = selectedDate;
        userData.attribute.status = job.IDLE().code;
        // userData.attribute.finishDate = "None";
        
        userData.save(function(err, userData){                        
            if(err){
                console.log(err);
                throw err;
            } else {                
                //console.log(userData);
                callback(userData._id);
            }
        });        
    },

    addImageIntoUserDataList: function(id, filename, callback){        
        UserData.findOne({"_id":id}, function(err, userData){
            if(!userData) return -1;

            let img = {
                filename: filename,
                isDownload: false
            }
            userData.images.push(img);
    
            userData.save(function(err, userData){
                if(err){
                    console.log(err);
                    throw err;
                } else {
                    //console.log(userData);
                    callback(userData._id, filename);
                }
            })
        });
    },

    setDownloadTrueByFilename: function(id, filename, callback) {
        //console.log('setDownloadTrueByFilename', id);
        
        UserData.findOne({"_id":id}, function(err, userData){
            
            if(!userData) return -1;

            userData.images.forEach((o) => {
                if(o.filename == filename && o.isDownload === false){
                    o.isDownload = true;
                }
            });

            userData.save(function(err){
                if(err) return console.error(err);

                if(callback){
                    callback(userData);
                }
            });
        });
    },

    addSensitiveImageForUserDataList: function(id, filename, eatHistoryId, callback) {
                
        UserData.findOne({"_id":id}, function(err, userData){
            
            if(!userData) return -1;

            userData.sensitiveImages.push({"filename": filename, "eatHistoryId": eatHistoryId});

            userData.save(function(err){
                if(err) return console.error(err);
                //console.log(food);
                //io.emit("status", food);
                if(callback){
                    callback(userData);
                }
            });
        });
    },

    updateImageForUserDataList: function(id, filename, type, eatHistoryId, selectedDate, callback) {

        let mFilename = path.join(selectedDate, '/', type, '/', filename);
        UserData.updateOne({
            "_id":id, 
            "images": {
                $elemMatch: {
                    "filename":filename
                }
            }
        }, 
        {
            $set: {
                "images.$.filename": mFilename,
                "images.$.eatHistoryId": eatHistoryId,
                "images.$.type": type,
                "images.$.isSensitive": false
            }
        }, 
        function(err, res){       
            callback(res);
        });

        // UserData.findOne({"_id":id}, function(err, userData){
            
        //     if(!userData) return -1;

        //     let list = [];
        //     //console.log(filename, type);

        //     //userData.images.push({"filename": filename, "eatHistoryId": eatHistoryId, "type": type, "isSensitive": false});
            
        //     userData.images.forEach((obj, idx) => {                
        //         if(obj.filename == filename){                    
        //             let mFilename = path.join(userData.attribute.selectedDate, '/', type, '/', filename);
        //             console.log('!!!!!', obj.filename, mFilename, idx);
        //             let item = {
        //                 "filename": mFilename,
        //                 "eatHistoryId": eatHistoryId, 
        //                 "type": type,
        //                 "isSensitive": false
        //             }
                    
        //             userData.images.splice(idx, 1, item); 
        //         }
        //     });
            
        //     userData.save(function(err){
        //         if(err) return console.error(err);
        //         //console.log(food);
        //         //io.emit("status", food);
        //         if(callback){
        //             callback(userData);
        //         }
        //     });
        // });
    },

    setListCountForUserDataList: function(id, listCount, callback) {
        console.log('setListCountForUserDataList', id, listCount);
        
        UserData.findOne({"_id":id}, function(err, userData){
            
            if(!userData) return -1;

            userData.attribute.listCount = listCount;

            userData.save(function(err){
                if(err) return console.error(err);
                //console.log(food);
                //io.emit("status", food);
                if(callback){
                    callback(userData);
                }
            });
        });
    },

    getUserDataIdByDate: function(date, callback){
        console.log('getUserDataIdByDate', date);

        UserData.findOne({"attribute.selectedDate": date}, function(err, userData){
            

            if(callback){
                callback(userData);
            }
        })
    },

    getUserDataById: function(id, callback){
        console.log('getUserDataById', id);

        UserData.findOne({"_id":id}, function(err, userData){
            if(callback){
                callback(userData);
            }
        })
    },

    getSensitiveImages: function(id, callback){
        console.log('getSensitiveImages', id);

        UserData.findOne({"_id":id}, function(err, userData){
            if(callback){
                let list = userData.images.filter((o) => {

                    if(o.type == 'face' && o.isSensitive == true) return true;
                    else return false;
                    //return o.isSensitive == true;
                });
                
                console.log('sensitive images:::', list);
               
                callback(list);
            }
        });
    },

    getImagesByIdForPagination: function(id, imageId, type, order, callback){
        console.log('getUserDataByIdForPagination', id, order);
  
        UserData.findOne({"_id":id}, function(err, userData){
            if(callback){
                let list;
                
                if(imageId !== undefined && imageId !== null){
                    list = userData.images.sort(ascOrder).filter((o) => {                    
                        return o._id > imageId;
                    })
                    .filter((l) => {
                        return l.type === type;
                    })
                    .splice(0, 10);
                } else {
                    list = userData.images.sort(ascOrder)
                    .filter((l) => {
                        return l.type === type;
                    })
                    .splice(0, 10);
                }
                
               
                callback(list);
            }
        });
        
    },

    setSensitiveByImageId: function(id, imageId, value, callback) {
        console.log('setSensitiveByImageId', id, imageId, value);
        
        UserData.findOne({"_id":id}, function(err, userData){
            
            if(!userData) return -1;          

            let mImages = userData.images.map((o) => {                
                
                if(o._id == imageId){
                    console.log(o._id, imageId);
                    if(value == 1){
                        o.isSensitive = true;
                    } else{
                        o.isSensitive = false;
                    }                           
                    console.log('!!!', o);     
                }
                return o;
            });

            userData.images = mImages;

            userData.save(function(err){
                if(err) return console.error(err);
                //console.log(food);
                //io.emit("status", food);
                if(callback){
                    callback(userData);
                }
            });
        });
    },
  
    setStatusForUserDataList: function(id, status, callback) {
        //console.log('setStatusForUserDataList', id, status);
        
        UserData.findOne({"_id":id}, function(err, userData){
            
            if(!userData) return -1;

            if(status) userData.attribute.status = status;

            userData.save(function(err){
                if(err) return console.error(err);
                //console.log(food);
                //io.emit("status", food);
                if(callback){
                    callback(userData);
                }
            });
        });
    },

    setCountForUserDataList: function(id, type, count, callback) {
        console.log('setCountForUserDataList', id, type, count);
        
        UserData.findOne({"_id":id}, function(err, userData){
            
            if(!userData) return -1;
            
            if(type === classifiedTypeEnum.FACE.value){
                userData.attribute.faceCount = count;
            } else if(type === classifiedTypeEnum.FOOD.value){
                userData.attribute.foodCount = count;
            } else if(type === classifiedTypeEnum.NONE_FOOD.value){
                userData.attribute.noneFoodCount = count;
            }

            

            userData.save(function(err){
                if(err) return console.error(err);
                //console.log(food);
                //io.emit("status", food);
                if(callback){
                    callback(userData);
                }
            });
        });
    },
   
    setFinishedForUserDataList: function(id, callback) {
                
        UserData.findOne({"_id":id}, function(err, userData){
            
            userData.attribute.finishedDate = Date.now();

            userData.save(function(err){
                if(err) return console.error(err);
                //console.log(food);
                //io.emit("status", food);
                if(callback){
                    callback(userData);
                }
            });
        });
    },

    getUserDataListAll: function(callback) {     
        console.log('getUserDataListAll');
                
        UserData.find({"attribute.finishedDate": null}, function(err, userList){            
            console.log(err);
            if(err) return console.error(err);
        
            callback(userList)
        });
    },


    createFood: function(foodData) {

        let food = new FoodItem({            
            attribute: {                
                name: foodData.attribute.name,
                keyword: foodData.attribute.keyword,
                state: foodData.attribute.state,
                is_activate: foodData.attribute.is_activate
            },
            source: {
                national_code: foodData.source.national_code,
                name: foodData.source.name,
                filename: foodData.source.filename
            },
            image: {
                filename: foodData.image.filename,
                s1class: foodData.image.s1class,
                s2class: foodData.image.s2class
            }
        });
        
        food.save(function(err, food){
            if(err) return console.error(err);
            console.dir(food);
        });
    },

    retrieveFoodSAll: function(){
        console.log('retrievefoodall');
        Food.find(function(err, foods){
            if(err) console.error(err);
            console.log(foods);
        })
    },

    retrieveFood: function(id, callback){
        //console.log('retrieveFood');

        Food.findOne({_id:id}, function(err, food){
            if(err) return console.error(err);
            if(!food) return -1;
            //console.log(food);
            callback(food);
        });
    },

    retrieveFoodsBySource: function(filename, callback){
        console.log('retrieveFoodsBySource');

        //FoodItem.find({"source.filename":filename}, function(err, foods){
            Food.find
        (
            { $and: 
                [
                    {"attribute.is_activated": true},                     
                    {"source.filename":{$eq:filename}}
                ]
            }, 
            function(err, foods){
            
                if(err) return console.error(err);
                if(!foods) return -1;            
                callback(foods);
            }
        );
    },

    retrieveFoodName: function(name, callback){
        //console.log('retrieveFoodName', name);
        Food.findOne({"attribute.name": name}, function(err, food){            

            if(err) return console.error(err);
            //if(food !== null && food.length == 0) return -1;
            //console.log(">>> retrieveFoodName", food);
            callback(food);
        });
    },

    updateFood: function(id, uFood){
        console.log('updateFood');
        Food.findById(id, function(err, food){
            if(err) return console.error(err);
            if(!food) return -1;

            if(uFood.attribute.name) food.attribute.name = uFood.attribute.name;

            food.save(function(err){
                if(err) return console.error(err);
                console.log(food);
            });
        });
    },

    updateFoodImage: function(id, filename, callback){
        console.log("Update Food Image:::", id, filename);
        //FoodItem.updateOne({"image._id":id, image: {$elemMatch: {"filename":filename}}}, {$set: {"image.$.filename": filename}}, function(err, res){
        Food.updateOne({"image._id":id}, {$set: {"image.$.filename": filename}}, function(err, res){
            callback(res);
            // console.log(err);
            // console.log(res)
        });
       
    },

    deleteFood: function(id, callback){
        console.log('deleteFood');
        Food.remove({_id:id}, function(err, output){
            if(err) return console.error(err);

            console.log(output);
            callback(output);
        });
    },

    deleteFoodImage: function(id, image, callback){
        console.log('deleteFood', id, image);

        Food.updateOne(
        {
            "_id":id, 
            "image": 
            {
                $elemMatch: 
                {
                    "filename":image
                }
            }
        }, 
        {
            $pull: 
            {
                "image": image
            }
        }, 
            function(err, res)
            {
                console.error(err);
                callback(res);
            }
        );     
    },


    getCrawlingListByFileName: function(fileName, callback) {        
        Food.find({"source.filename":fileName}, function(err, food){        
            if(err) return console.error(err);
            if(!food) return -1;
            
            callback(food);
        });
    },

    addNewKeyword: function(foodname, keyword, username, callback) {
        
        const food = Food();
        food.attribute.name = foodname;
        food.attribute.keyword = keyword;
        food.source.national_code = "KR";
        food.source.name = "KR";
        food.source.filename = "Manual";
        food.attribute.username = username;
      
        food.save(function(err, food){                        
            if(err){
                console.log(err);
                throw err;
            } else {                
                callback(food._id);
            }
        });

        
    },

    addExistKeyword: function(foodname, keyword, source, nationalCode, images, username, callback) {
        const food = Food();

        food.attribute.name = foodname;
        food.attribute.keyword = keyword;
        food.source.national_code = nationalCode;
        food.source.name = nationalCode;
        food.source.filename = source;
        food.image = images;
        food.attribute.username = username;
      
        food.save(function(err, food){
            if(err) return console.error(err);
            //console.log(food);

            if(err){
                console.log(err);
                throw err;
            } else {                
                callback(food._id);
            }
        });

        
    },

    addNewFoodtoCrawlingList: function(mFood, fileName, callback) {

        let food = new FoodItem({            
            attribute: {                
                name: mFood,
                keyword: mFood,
                // state: foodData.attribute.state,
                // is_activate: foodData.attribute.is_activate
            },
            source: {
                national_code: "NA",
                name: "NA",
                filename: fileName
            },
            image: {
                // filename: foodData.image.filename,
                // s1class: foodData.image.s1class,
                // s2class: foodData.image.s2class
            }
        });
        
        food.save(function(err, food){
            if(err) return console.error(err);
            //console.log(food);

            if(err){
                console.log(err);
                throw err;
            } else {                
                callback(food._id);
            }
        });

        
    },

    getDataByIndex: function(id, callback) {        
        console.log('getDataByIndex');

        Food.findOne({_id:id}, function(err, food){            
            if(err) return console.error(err);
            if(!food) return -1;
            //console.log(food);

            callback(food);
        });
    },

    getDatasWithActivation: function(username, callback) {
     
        let optionsNormal = {
            "attribute.is_activated": true, 
            "attribute.state":{$ne:700}, 
            "source.filename":{$ne:"exist_append"},
            "attribute.username":{$eq:username}
        }

        let optionAdmin = {
            "attribute.is_activated": true, 
            "attribute.state":{$ne:700}, 
            "source.filename":{$ne:"exist_append"}            
        }

        let options;

        if(username === "admin"){
            options = optionAdmin;
        } else {
            options = optionsNormal;
        }

        console.log('getDatasWithActivation');
        //FoodItem.find({"attribute.is_activated": true}, function(err, foods){
        Food.find({ $and: 
            [
                // {"attribute.is_activated": true}, 
                // {"attribute.state":{$ne:700}}, 
                // {"source.filename":{$ne:"exist_append"}},
                // {"attribute.username":{$eq:username}}
                options
            ]
        }, function(err, foods){
            if(err) {
                console.error(err);
                //res.sendStatus(500);
                return;
            } else{
                callback(foods)
            }
            
        });
    },

    getDatasWithStatus: function(status, callback) {
     
        console.log('getDatasWithStatus', status);
        
        //FoodItem.find({"attribute.state": {$in:[600, 700]}}, function(err, foods){
        Food.find({"attribute.state": {$in:[700]}}, function(err, foods){
            console.log('!!!', foods);
            console.log(err);
            if(err) return console.error(err);
        
            callback(foods)
        });
    },

    setUser: function(id, username, callback) {
                
        Food.findById(id, function(err, food){
            if(err) return console.error(err);
            if(!food) return -1;

            food.attribute.username = username;

            food.save(function(err){
                
                if(err) return console.error(err);
                
                //io.emit("status", food);
                if(callback){
                    callback(food);
                }
            });
        });
    },

    setDeactive: function(id, callback) {
                
        Food.findById(id, function(err, food){
            if(err) return console.error(err);
            if(!food) return -1;

            food.attribute.is_activated = false;

            food.save(function(err){
                
                if(err) return console.error(err);
                
                //io.emit("status", food);
                if(callback){
                    callback(food);
                }
            });
        });
    },

    setActive: function(id, callback) {
        console.log('setActive', id);
        const FoodItem = Food();
        Food.findById(id, function(err, food){
            if(err) return console.error(err);
            if(!food) return -1;

            food.attribute.is_activated = true;

            food.save(function(err){
                if(err) return console.error(err);
                //console.log(food);
                io.emit("status", food);
                if(callback){
                    callback(food);
                }
            });
        });
    },

    setState: function(id, state, callback) {
        console.log('setState', id, state);
        
        Food.findOne({"_id":id}, function(err, food){
            
            if(!food) return -1;

            if(state) food.attribute.state = state;

            food.save(function(err){
                if(err) return console.error(err);
                //console.log(food);
                //io.emit("status", food);
                if(callback){
                    callback(food);
                }
            });
        });
    },

    setDone: function(id, done, status, callback) {
        console.log('setDone', id, done);
        
        Food.findById(id, function(err, food){
            if(err) return console.error(err);
            if(!food) return -1;

            food.attribute.done = done;
            food.attribute.state = status            

            food.save(function(err){
                if(err) return console.error(err);
                //console.log(food);     
                callback(food);
            });
        });
    },

    getKeywordbyid: function(id, callback) {
      
        console.log('getKeywordbyid');

        Food.findOne({_id:id}, function(err, food){
            if(err) return console.error(err);
            if(!food) return -1;
            //console.log(food);
            //console.log('!@#!@!!!   ', food.attribute.keyword);
            callback(food.attribute.keyword);
        });
    },

    pushImagelist: function(id, images, callback) {
        
        let criteria = {};
        if(id) {            
            if(mongoose.Types.ObjectId.isValid(id)) {
              criteria.id = id;
            }

          }
        
          //console.log(criteria.id);
        //return UserModule.find(criteria).exec(() => {
        return Food.findById(criteria.id, function(err, food){
            //FoodItem.findById(criteria, function(err, food){        
                if(err) return console.error(err);
                //if(!food) return -1;
                //console.log('!!!!! 1', food, images);
                let imageArr = [];
                images.forEach(function(item, index, array){
                    
                    let i = {
                        filename:item[0],
                        s1class:item[1],
                        s2class:item[2]                        
                    }
                    imageArr.push(i);
                });

                try{
                    if(images.length != 0) food.image = imageArr;
                    //console.log('!!!!! 2', food);
                    food.save(function(err){
                        if(err) return console.error(err);
                        console.log('!!!! 3 ', food);
                    });
                } catch(e){
                    console.log(e);
                }
                
            });
       
    },

    getImagelist: function(id, callback) {

        console.log('getImagelist');

        Food.findOne({_id:id}, function(err, food){
            console.log(food);
            if(err) return console.error(err);
            if(!food) return -1;
            //console.log(food.image);
            callback(food.image)
        });
    },

    getImageIdByFilename: function(id, filename, callback){
        Food.findOne({_id:id}, function(err, food){            
            if(err) return console.error(err);
            if(!food) return -1;
            //console.log(food.image);
            callback(food._id, filename);
        });
    },

    updateImageS1Class: function(id, filename, state, callback){
       
        Food.updateOne({"_id":id, image: {$elemMatch: {"filename":filename}}}, {$set: {"image.$.s1class": state}}, function(err, res){       
            callback(res);
        });
    },

}
