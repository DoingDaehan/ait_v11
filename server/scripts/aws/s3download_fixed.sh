#!/bin/bash
#set -e
#mkdir -p $3
#aws s3 ls s3://diet-diary-photo/ --recursive | grep $1 | awk -F' ' '{print $4}' >> $2
while read line
do
 aws s3 cp s3://diet-diary-photo/$line $2
done < $1