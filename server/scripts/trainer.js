const os = require('os');
const path = require('path');
const readline = require('readline');

module.exports = {
    train: function(pathTrain, pathClassify, callback) {
        const spawn = require('child_process').spawn;
        const mPath = path.join(path.dirname(__filename), '../py/train.py')
        var pythonProcess = spawn('python', [mPath, pathTrain, pathClassify])
        var retCode = 0;
        
        // pythonProcess.stdout.pipe(process.stdout);
        // pythonProcess.stderr.pipe(process.stderr);

        // pythonProcess.on('exit', (exitCode) => {
        //     callback();
        //     console.log(pathTrain + ': Predict Done');
        // });


        pythonProcess.stdout.on('data', function(data){
            console.log('stdout: ' + data);
        });

        pythonProcess.stderr.on('data', function(data){
            if(data.indexOf('OSError') !== -1){
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                retCode = -10;

            } else if(data.indexOf('out of memory') !== -1){
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                retCode = -11;
            } else if(data.indexOf('ValueError') !== -1){
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                retCode = -12;
            }
            console.error('stderr: ' + data);

            callback(retCode);
        });

        
        pythonProcess.on('exit', (exitCode) => {           
            let ret = 0;
            if(exitCode === 0){
                ret = 1;
            }
            callback(ret);            
            console.log(pathTrain + ': Process Done', exitCode, ret);
        });
    },

    predict: function(pathRoot, pathTrain, pathClassify, opt, callback) {
        const spawn = require('child_process').spawn;
        const mPath = path.join(path.dirname(__filename), '../py/predict.py')
        var pythonProcess;
        var retCode = 0;
        console.log("!!!!!!!OPT: ", opt)
        if(opt == undefined){
            pythonProcess = spawn('python', [mPath, pathRoot, pathTrain, pathClassify])
        } else {  
            pythonProcess = spawn('python', [mPath, pathRoot, pathTrain, pathClassify, opt])
        }

        pythonProcess.stdout.on('data', function(data){
            console.log('stdout: ' + data);
        });

        pythonProcess.stderr.on('data', function(data){
            if(data.indexOf('OSError') !== -1){
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                retCode = -10;
            } else if(data.indexOf('out of memory') !== -1){
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                retCode = -11;
            } else if(data.indexOf('ValueError') !== -1){
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                retCode = -12;
            }
            console.error('stderr: ' + data);
            //callback(retCode);
        });

        pythonProcess.on('exit', (exitCode) => {        
            callback(exitCode);            
            console.log(pathTrain + ': Process Done', exitCode);
        });
    },

    predict2: function(pathRoot, pathTrain, pathClassify, opt, callback) {
        const spawn = require('child_process').spawn;
        const mPath = path.join(path.dirname(__filename), '../py/predict_v2.py')
        var pythonProcess;
        var retCode = {};
        console.log("!!!!!!!OPT: ", opt)
        if(opt == undefined){
            pythonProcess = spawn('python', [mPath, pathRoot, pathTrain, pathClassify])
        } else {  
            pythonProcess = spawn('python', [mPath, pathRoot, pathTrain, pathClassify, opt])
        }

        readline.createInterface({
            input     : pythonProcess.stdout,
            terminal  : false
          }).on('line', function(line) {
//            console.log('((((((', line);
            let t = "";
            
            if(line.indexOf('GOOD') !== -1){
                t = line.split('/');
                console.log('GOOD filename:', t[1]);
            } else if(line.indexOf('BAD') !== -1){
                t = line.split('/');
                console.log('BAD filename:', t[1]);
            }

            retCode = {
                type: 1, // good or bad
                imageStatus:t[0], 
                filename:t[1]
            }

            callback(retCode);
          });

        // pythonProcess.stdout.on('data', function(data){
        //     console.log('stdout: ', data);
        // });

        pythonProcess.stderr.on('data', function(data){
            if(data.indexOf('OSError') !== -1){
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                retCode = {
                    type: -1, // error
                    errCode:-10,
                    desc:"OSERROR"
                }
            } else if(data.indexOf('out of memory') !== -1){
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                
                retCode = {
                    type: -1, // error
                    errCode:-11,
                    desc:"OUT OF MEMORY"
                }
            } else if(data.indexOf('ValueError') !== -1){
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                retCode = {
                    type: -1, // error
                    errCode:-12,
                    desc:"VALUE ERROR"
                }
            }

            
            console.error('stderr: ' + data);
            callback("retCode::", data);
        });

        pythonProcess.on('exit', (exitCode) => {        
            if(exitCode === 0){
                exitCode = {
                    type: 2,
                    exitCode: 0
                }
            }
            callback(exitCode);            
            console.log(pathTrain + ': Process Done', exitCode);
            
        });
    },
}