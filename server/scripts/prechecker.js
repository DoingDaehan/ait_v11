const os = require('os');
const path = require('path');
const readline = require('readline');
const fs = require('fs')

function ensureDirSync (dirpath) {    
    
    try {
        const mPath = path.join(path.dirname(__filename), dirpath);
        //console.log(mPath);
        fs.exists(dirpath, function(exist){
            //console.log('EXIST !!!!!!!!!!!', exist);
            if(exist !== true){
                //fs.mkdirSync(dirpath, { recursive: true }, function(err){
                fs.mkdirSync(dirpath, function(err){
                    if(err){
                        console.log(err);
                        //throw err;
                    } else{
                        console.log('Directory created');
                    }
                    // fs.writeFile(mPath + '/' + 'list.txt', '', function(err){
                    //     if(err) throw err;
                    //     console.log("File Created");
                    //   });
                });
            }
            
        });
    } catch (err) {
      if (err.code !== 'EEXIST') {
          console.err(err);
      }
    }
}

module.exports = {
    getlist: function(selectedDate, callback) {
        const spawn = require('child_process').spawn;
        const mPath = path.join(path.dirname(__filename), './aws/s3getlist_fixed.sh')
        const listPath = '/FoodJobs/User_Data/list/';
        
        try {
            ensureDirSync(listPath + selectedDate);
            console.log('Directory created')
        } catch (err) {
            console.error(err)
        }        
        
        var pythonProcess = spawn('bash', [mPath, selectedDate, 'list.txt'])
        var retCode = 0;
        
        pythonProcess.stdout.on('data', function(data){
            console.log('stdout: ' + data);
        });

        pythonProcess.stderr.on('data', function(data){            
            console.error('stderr: ' + data);
            callback(retCode);
        });
        
        pythonProcess.on('exit', (exitCode) => {           
            let ret = 0;
            
            if(exitCode === 0){
                ret = 1;                
            }
            callback(ret);            
            console.log(selectedDate + ': Process Done', exitCode, ret);
        });
    },

    getlistFromExactFile: function(filename, selectedDate, callback) {
        const spawn = require('child_process').spawn;
        const mPath = path.join(path.dirname(__filename), './aws/s3getlist_exact_file.sh')
        const listPath = '/FoodJobs/User_Data/list/';
        
        // try {
        //     ensureDirSync(listPath + selectedDate);
        //     console.log('Directory created')
        // } catch (err) {
        //     console.error(err)
        // }        
        
        // var pythonProcess = spawn('bash', [mPath, filename, selectedDate, 'list.txt']);
        var pythonProcess = spawn('bash', [mPath, filename]);
        var retCode = 0;
        
        pythonProcess.stdout.on('data', function(data){
            console.log('stdout: ' + data);
        });

        pythonProcess.stderr.on('data', function(data){            
            console.error('stderr: ' + data);
            callback(retCode);
        });
        
        pythonProcess.on('exit', (exitCode) => {           
            let ret = 0;
            
            if(exitCode === 0){
                ret = 1;                
            }
            callback(ret);            
            console.log(selectedDate + ': Process Done : ', filename, exitCode, ret);
        });
    },

    download: function(selectedDate, callback) {
        const spawn = require('child_process').spawn;
        const mPath = path.join(path.dirname(__filename), './aws/s3download_fixed.sh')
        const listPath = '/FoodJobs/User_Data/list/';
        const downPath = '/FoodJobs/User_Data/';
        
        const mFolder = listPath + selectedDate;
        let readFile;
        let readPath;
        console.log(mFolder)
        fs.readdirSync(mFolder).forEach(file => {
            console.log(file);
            readFile = file;
        });

        readPath = listPath + selectedDate + '/' + readFile;
                        
        const readInterface = readline.createInterface({
            input: fs.createReadStream(readPath),
            output: process.stdout,
            console: false
        });

        try {
            ensureDirSync(downPath + selectedDate);
            console.log('Directory created')
        } catch (err) {
            console.error(err)
        }        
        
        console.log(mPath);
        console.log(readPath);
        console.log(downPath + selectedDate)
        var pythonProcess = spawn('bash', [mPath, readPath, downPath + selectedDate])
        var retCode = 0;
        
        pythonProcess.stdout.on('data', function(data){
            console.log('stdout: ' + data);
        });

        pythonProcess.stderr.on('data', function(data){            
            console.error('stderr: ' + data);
            callback(retCode);
        });
        
        pythonProcess.on('exit', (exitCode) => {           
            let ret = 0;
            
            if(exitCode === 0){
                ret = 1;                
            }
            callback(ret);            
            console.log(selectedDate + ': Process Done', exitCode, ret);
        });      
    },

    downloadExactFile: function(filename, selectedDate, callback) {
        const spawn = require('child_process').spawn;
        const mPath = path.join(path.dirname(__filename), './aws/s3download_exact_file.sh')
        const listPath = '/FoodJobs/User_Data/list/';
        const downPath = '/FoodJobs/User_Data/';
        
        // const mFolder = listPath + selectedDate;
        // let readFile;
        // let readPath;
        // console.log(mFolder)
        // fs.readdirSync(mFolder).forEach(file => {
        //     console.log(file);
        //     readFile = file;
        // });

        // readPath = listPath + selectedDate + '/' + readFile;
                        
        // const readInterface = readline.createInterface({
        //     input: fs.createReadStream(readPath),
        //     output: process.stdout,
        //     console: false
        // });

        // try {
        //     ensureDirSync(downPath + selectedDate);
            
        // } catch (err) {
        //     console.error(err)
        // }        
        
        // console.log(mPath);
        // console.log(filename);
        // console.log(downPath + selectedDate)
        var pythonProcess = spawn('bash', [mPath, filename, downPath + selectedDate])
        var retCode = 0;
        
        pythonProcess.stdout.on('data', function(data){
            console.log('stdout: ' + data);
        });

        pythonProcess.stderr.on('data', function(data){            
            console.error('stderr: ' + data);
            callback(retCode);
        });
        
        pythonProcess.on('exit', (exitCode) => {           
            let ret = 0;
            
            if(exitCode === 0){
                ret = 1;                
            }
            callback(ret);            
            //console.log(selectedDate + ': Process Done', exitCode, ret);
        });      
    },

    train: function(callback) {
        const spawn = require('child_process').spawn;
        const mPath = path.join(path.dirname(__filename), '../py/userdata/train.py')                
        const pathTrainBase = '/FoodJobs/User_Data/';
        let pathTrain = path.join(pathTrainBase, 'trainset');
        var retCode = 0;
        
        var pythonProcess = spawn('python', [mPath, pathTrain])
        
        pythonProcess.stdout.on('data', function(data){
            console.log('stdout: ' + data);
        });

        pythonProcess.stderr.on('data', function(data){
            if(data.indexOf('OSError') !== -1){
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                retCode = -10;

            } else if(data.indexOf('out of memory') !== -1){
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                retCode = -11;
            } else if(data.indexOf('ValueError') !== -1){
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                retCode = -12;
            }
            console.error('stderr: ' + data);

            callback(retCode);
        });

        
        pythonProcess.on('exit', (exitCode) => {           
            let ret = 0;
            if(exitCode === 0){
                ret = 1;
            }
            callback(ret);            
            console.log(pathTrain + ': Process Done', exitCode, ret);
        });
    },

    predict: function(pathRoot, pathTrain, classifiedPath, callback) {
        const spawn = require('child_process').spawn;
        const mPath = path.join(path.dirname(__filename), '../py/userdata/predict.py')
        
        var pythonProcess;
        var retCode = 0;
        
        pythonProcess = spawn('python', [mPath, pathRoot, pathTrain, classifiedPath])
        
        pythonProcess.stdout.on('data', function(data){
            console.log('stdout: ' + data);
        });

        pythonProcess.stderr.on('data', function(data){
            if(data.indexOf('OSError') !== -1){
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                console.log("OSERROR !!!!!!!")
                retCode = -10;
            } else if(data.indexOf('out of memory') !== -1){
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                console.log("OUT OF MEMORY !!!!!!!!")
                retCode = -11;
            } else if(data.indexOf('ValueError') !== -1){
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                console.log("VALUE ERROR !!!!!!!!")
                retCode = -12;
            }
            console.error('stderr: ' + data);
            //callback(retCode);
        });

        pythonProcess.on('exit', (exitCode) => {        
            let ret = 0;
            if(exitCode === 0){
                ret = 1;
            }
            callback(ret);               
            console.log(pathTrain + ': Process Done', ret);
        });
    },
}