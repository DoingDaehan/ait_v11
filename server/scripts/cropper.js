const os = require('os');
const path = require('path');

module.exports = {
    crop: function(pathBase, savePath, callback) {
        const spawn = require('child_process').spawn;
        //const mPath = path.join(path.dirname(__filename), '../crop_script/Food_Background_Crop_Only.py')
        const mPath = path.join(path.dirname(__filename), '../py/food_bg_crop_only.py')
        var pythonProcess = spawn('python', [mPath, pathBase, savePath])
        
        pythonProcess.stdout.pipe(process.stdout);
        pythonProcess.stderr.pipe(process.stderr);
        
        pythonProcess.on('exit', (exitCode) => {
            callback();
            console.log(pathBase + ': Process Done');
        });
    },
}