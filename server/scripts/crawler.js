const path_util = require('../routes/path_util');

function run(pythonProcess, callback, keyword, watchpuppy){
    // Important (Jin)  
    // Child process stdout must be released. 
    // Child process will be stopped when you don't release like below.
    let retCode = 0;
    //watchpuppy = new Watchpuppy(options, callback);
    console.log('11111', watchpuppy);
    pythonProcess.stdout.on('data', function(data){
        console.log('stdout: ' + data);
    });

    pythonProcess.stderr.on('data', function(data){
        // if(data.indexOf('OSError') !== -1){
        //     console.log("OSERROR !!!!!!!")
        //     console.log("OSERROR !!!!!!!")
        //     console.log("OSERROR !!!!!!!")
        //     console.log("OSERROR !!!!!!!")
        //     console.log("OSERROR !!!!!!!")
        //     retCode = -10;

        // } else if(data.indexOf('out of memory') !== -1){
        //     console.log("OUT OF MEMORY !!!!!!!!")
        //     console.log("OUT OF MEMORY !!!!!!!!")
        //     console.log("OUT OF MEMORY !!!!!!!!")
        //     console.log("OUT OF MEMORY !!!!!!!!")
        //     console.log("OUT OF MEMORY !!!!!!!!")
        //     retCode = -11;
        // }
        console.error('stderr: ' + data);
        if(retCode === 0){
            console.log('alive...');            
            watchpuppy.ping();
        }
        callback(retCode);
    });

    
    pythonProcess.on('exit', (exitCode) => {           
        let ret = 0;
        if(exitCode === 0){
            ret = 1;
        }
        callback(ret);            
        console.log(keyword + ': Process Done', exitCode, ret);
    });

    // pythonProcess.stdout.on('data', function(data) {
    //     console.log(data + 'Processing');
    // });
}

module.exports = {

    crawl: function(keyword, maxNum, watchpuppy, callback) {
        const spawn = require('child_process').spawn;        

        var pythonProcess;
        console.log(__basedir);
        console.log( process.env.PATH );
        console.log(path_util.getPyScriptPath() + 'crawler.py');
        var currentPath = process.cwd();
        console.log(currentPath)
        
        // const testFolder = currentPath + '/py';
        // const fs = require('fs');

        // fs.readdirSync(testFolder).forEach(file => {
        //     console.log(file);
        // });

        if(maxNum != undefined){
            pythonProcess = spawn('python', [path_util.getPyScriptPath() + 'crawler.py', keyword, maxNum])
        } else{
            pythonProcess = spawn('python', [path_util.getPyScriptPath() + 'crawler.py', keyword])
        }
        
        //watchpuppy = new Watchpuppy(options, callback);
        //watchpuppy.ping();
        //console.log('11111', watchpuppy);

        callback(0);
        run(pythonProcess, callback, keyword, watchpuppy)
    },

}