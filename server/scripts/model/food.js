const mongoose = require('mongoose');

// Define Schemes
const foodSchema = new mongoose.Schema({    
    attribute: { 
        name: {type: String, required: true},
        keyword: {type: String, required: true},
        state: {type: Number, default: 0},
        is_activated: {type: Boolean, default: 1},
        done: {type: Boolean, default: 0}
    },
    source: {
        national_code: {type: String, required: true},
        name: String,
        filename: String
    },
    image: [
            {
                filename: {type: String },
                s1class: {type: Number, default: 0},
                s2class: {type: Number, default: 0}
            }
    ]    
},
{
  timestamps: true
});

// Create Model & Export
if (process.env.NODE_ENV === "production") {    
    console.log("Production Mode ::: food.js");

    if(process.env.NODE_ENV_PRODUCTION == "production1"){
        module.exports = mongoose.model('FoodTempUS', foodSchema);
    } else if(process.env.NODE_ENV_PRODUCTION == "production2"){
        module.exports = mongoose.model('FoodTemp2', foodSchema);
    } else if(process.env.NODE_ENV_PRODUCTION == "production3"){
        module.exports = mongoose.model('FoodTemp3', foodSchema);
    }
    //module.exports = mongoose.model('Food', foodSchema);
} else if (process.env.NODE_ENV == 'development') {
    module.exports = mongoose.model('Food_Dev', foodSchema);
}
