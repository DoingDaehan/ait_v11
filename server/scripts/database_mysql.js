var mysql = require('mysql');

const jobManager = require('../routes/ProcessStatusManager.js');
const job = jobManager();

const Food = require('../models/food');
const UserData = require('../models/userdata');

module.exports = {

    getEatHistoryAll: function(callback){
        var sql = 'SELECT * FROM EatHistory;';
        
        pool.query(sql, (err, results) => {
            if(err){
                console.log(err);
                throw err;
            } else {                
                callback(results);
            }
        })
    },

    getEatHistoryByFilename: function(fileName, callback){
        let query = 'SELECT * FROM EatHistory WHERE img_path=?';
        
        // var values = [
        //     [fileName]
        // ];
  
        pool.query(query, fileName, (err, results) => {
            if(err){
                console.log(err);
                throw err;
            } else {
                console.log('getEatHistoryByFilename::', results);
                callback(results);
            }
        });
    },

    getFoodImageByFilename: function(fileName, callback){
        let query = 'SELECT * FROM FoodImage WHERE full_image_file=?';
        
        // var values = [
        //     [fileName]
        // ];
  
        pool.query(query, fileName, (err, results) => {
            if(err){
                console.log(err);
                throw err;
            } else {
                console.log('getFoodImageByFilename::', results);
                callback(results);
            }
        });
    },

    getFilenameFromEatHistoryByDate: function(date, callback){
        let query = 'SELECT * FROM EatHistory WHERE DATE(date)=? AND img_path != ?';
        pool.query(query, [date, ''], (err, results) => {
            if(err){
                console.log(err);
                throw err;
            } else {
                //console.log('getFilenameFromEatHistoryByDate::', results);
                callback(results);
            }
        });
    },

    
    getFilenameFromFoodImageByDate: function(date, callback){
        let query = 'SELECT * FROM FoodImage WHERE DATE(predict_date)=? AND full_image_file != ?';
        pool.query(query, [date, ''], (err, results) => {
            if(err){
                console.log(err);
                throw err;
            } else {
                console.log('getFilenameFromFoodImageByDate::', results);
                callback(results);
            }
        });
    },

    updateEatHistoryWithDownflag: function(id, val, callback){
        var query = 'UPDATE EatHistory SET download_flag = ? WHERE id = ?;';
        
        pool.query(query, [val, id], (err, results) => {
            console.log(val, id);
            if(err){
                console.log(err);
                throw err;
            } else {                
                callback(results);
            }
        });
    },

    updateEatHistoryWithDownflagByImgPath: function(img_path, val, callback){
        var query = 'UPDATE EatHistory SET download_flag = ? WHERE img_path = ?;';
        
        pool.query(query, [val, img_path], (err, results) => {
            console.log(query, val, img_path);
            if(err){
                console.log(err);
                throw err;
            } else {                
                callback(results);
            }
        });
    },

    updateFoodImageWithDownflagByImgPath: function(img_path, val, callback){
        var query = 'UPDATE FoodImage SET is_private = ? WHERE full_image_file = ?;';
        
        pool.query(query, [val, img_path], (err, results) => {
            console.log(query, val, img_path);
            if(err){
                console.log(err);
                throw err;
            } else {                
                callback(results);
            }
        });
    },

}
