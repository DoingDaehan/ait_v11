// const logger = module.exports = require('winston')
// logger.add(logger.transports.File({
//     name: 'debug-file',
//     filename: 'log.log',
//     level: 'debug',
//     handleExceptions: true,
//     humanReadableUnhandledException: true,
//     exitOnError: true,
//     json: false,
//     maxsize: 104857600,
//     maxFiles: 5,
// }))
// logger.add(logger.transports.Console, {
//     name: 'error-console',
//     level: 'error',
//     handleExceptions: true,
//     humanReadableUnhandledException: true,
//     exitOnError: true,
// })

const winston = require('winston');
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf, colorize, splat } = format;
require('winston-daily-rotate-file');
require('date-utils');

const myFormat = printf((info) => {
    if (info.meta && info.meta instanceof Error) {
      return `${info.timestamp} ${info.level} ${info.message} : ${info.meta.stack}`;
    }
    return `${info.timestamp} ${info.level}: ${info.message}`;
});

const LOG_LEVEL = process.env.LOG_LEVEL || 'debug';
const logger = winston.createLogger({
    level: 'debug',
    transports: [
        new winston.transports.DailyRotateFile({
            filename: 'log/system.log',
            zippedArchive: true,
            format: winston.format.printf(
                info => `${new Date().toFormat('YYYY-MM-DD HH24:MI:SS')} [${info.level.toUpperCase()}] - ${info.message}`
            ),
            handleExceptions: true,
            //humanReadableUnhandledException: true,
        }),
        // new winston.transports.Console({
        //     format: winston.format.printf(
        //         info => `${new Date().toFormat('YYYY-MM-DD HH24:MI:SS')} [${info.level.toUpperCase()} - ${info.message}]`
        //     ),
        //     handleExceptions: true,
        //     //humanReadableUnhandledException: true,
        // })
        new (transports.Console)(
            {
              level: LOG_LEVEL,
              format: combine(
                colorize(),
                timestamp(),
                splat(),
                myFormat
              )
            }
          )
    ]
});

module.exports = logger;